# Dr Insights Backend

This is the backend project for our product named Dr Insights. Dr Insights was the first project that I worked on with Nixel.
It is basically a web application that helps business to track and manage their cashflow for their projects. This was mainly
intended for the SME contractors in the property development line.

The frontend project for Dr Insights can be found [here](https://gitlab.com/TheGreatGaben/dr-insights-frontend).
