<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;

class MakeCron extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:cron {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new cron class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Cron';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        if(strtolower($this->argument("name")) == "cronbase"){
            return app_path() . '/Console/Stubs/cronbase.stub';
        }
        return app_path() . '/Console/Stubs/cron.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Cron';
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
