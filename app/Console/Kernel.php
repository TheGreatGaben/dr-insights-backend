<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        if(!file_exists(app_path("Cron/CronBase.php")))
            \Artisan::call('make:cron', ["name" => "CronBase"]);
        foreach((new \DirectoryIterator(app_path("Cron"))) as $cron)
            if($cron->getExtension() == "php" && strtolower($cron = substr($cron, 0, -4)) != "basecron"){
                $cron = "\\App\\Cron\\" . $cron;
                new $cron($schedule);
            }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
