<?php
namespace App\Cron;

use Illuminate\Support\Str;
use App\Notifications\ErrorNotify;
use Illuminate\Support\Facades\Log;
use Illuminate\Console\Scheduling\Schedule;

class CronBase
{
    /**
     * if error happened during cron job, decide
     * to send email to developer or not
     *
     * @var bool
     */
    protected $send_err_email = true;

    /**
     * if error happened during cron job, decide
     * to send email to all emails here
     *
     * @var array
     */
    protected $emails = [
        "esp@nixel.tech",
        "yihou@nixel.tech",
        "nenglih@nixel.tech",
        "john@nixel.tech",
        "gabriel@nixel.tech",
    ];

    /**
     * the command to run in the cron
     * will auto run invoke() if no set
     *
     * @var string
     */
    protected $command;

    /**
     * auto load to run command if got value
     * else will auto run invoke and will
     * return schedule object to chain
     *
     * @param Schedule $schedule
     * @return \Illuminate\Console\Scheduling\CallbackEvent|\Illuminate\Console\Scheduling\Event
     */
    protected function run(Schedule $schedule){
        return (($this->command)?$schedule->command($this->command):$schedule->call($this));
    }

    /**
     * to check is current cron class is enable
     * in .env file or not automatically.
     * if does not exists in .env file, will
     * set default value depend on situation.
     *
     * @param bool $default
     * @return mixed
     */
    public function enable($default = true){
        return env('APP_CRON_' . strtoupper(Str::snake(class_basename($this))), $default);
    }

    /**
     * generate log and sending email to inform
     * developer if any error happened during cron process
     *
     * @param \Exception $exception
     * @throws \Exception
     */
    protected function error(\Exception $exception){
        $trace = $this->generateTraceMessage($exception);
        $error = $this->generateErrorResponse($exception);
        if(config("app.send_err_email") && $this->send_err_email){
            Notification::route("mail", $this->emails)
                ->notify(new ErrorNotify($error, $trace))
            ;
        }
        Log::channel('cron')->info($error["message"] . "\nError : \n" . $trace);
        throw $exception;
    }

    /**
     * Returns a human readable line by line backtrace from exception backtrace
     * including the file, function which calls, and the line #.
     *
     * @param  \Exception  $exception
     * @return string
     */
    private function generateTraceMessage(\Exception $exception) {
        $str = get_class($exception) . " Error " . $exception->getCode() . ": " . $exception->getMessage() .
            " in file " . $exception->getFile() . " line " . $exception->getLine() . "\n";
        foreach($exception->getTrace() as $trace)
        {
            $str .= ((isset($trace['file']))?$trace['file'] . ": ":"") .
                ((isset($trace['class']))?$trace['class'] . $trace['type']:"") .
                ((isset($trace['function']))?$trace['function'] . "(): ":"") .
                ((isset($trace['line']))?$trace['line'] . "\n":"\n");
        }
        return $str;
    }

    /**
     * Generate and return general error message depends on $exception
     * will return status code together with message by array
     * can return "send_email" as key and boolean value to decide force send an email or not
     *
     * @param Exception $exception
     * @return array
     */
    private function generateErrorResponse(\Exception $exception){
        $error = ["status_code" => 500, "message" => "Error happened in cron job. Please check : " . get_class($this)];
        return $error;
    }
}
