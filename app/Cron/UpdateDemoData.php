<?php
namespace App\Cron;

use Illuminate\Console\Scheduling\Schedule;

class UpdateDemoData extends CronBase
{
    /**
     * the command to run in the cron
     *
     * @var string
     */
    protected $command = "db:seed --class=UpdateAllDataDate";

    /**
     * DummyClass constructor. can set scheduler
     * frequency and other setting here
     *
     * @see https://laravel.com/docs/5.8/scheduling#schedule-frequency-options
     * @param Schedule $schedule
     */
    public function __construct(Schedule $schedule)
    {
        if($this->enable($default = false)){
            $this->run($schedule)
                ->timezone('Asia/Kuala_Lumpur')
                ->monthlyOn(1, '00:00')
            ;
        }
    }

    /**
     * set cron task here
     */
    public function __invoke()
    {
        try{

        }catch(\Exception $exception){
            $this->error($exception);
        }
    }
}
