<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class HasDuplicateDataEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $model;
    public $duplicateData;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($model, $duplicateData)
    {
        $this->model = $model;
        $this->duplicateData = $duplicateData;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
