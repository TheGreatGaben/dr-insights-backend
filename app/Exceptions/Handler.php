<?php

namespace App\Exceptions;

use Exception;
use Notification;
use Carbon\Carbon;
use App\Notifications\ErrorNotify;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     * there are some default exception which will not report.
     *
     * @see \Illuminate\Foundation\Exceptions\Handler::$internalDontReport
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    protected $emails = [
        "esp@nixel.tech",
        "yihou@nixel.tech",
        "nenglih@nixel.tech",
        "john@nixel.tech",
        "gabriel@nixel.tech",
    ];

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $error = $this->generateErrorResponse($exception);
        if(!isset($error["data"]))
            $error["data"] = [];
        if(config("app.debug"))
            $error["laravel_debug"] = parent::render($request, $exception);
        $error["data"]["get"] = $request->query->all();
        $error["data"]["post"] = (($request->method("post") != "GET")?$request->request->all():[]);
        if(config("app.send_err_email") && $this->shouldReport($exception) || (isset($error["send_email"]) && $error["send_email"]))
            Notification::route("mail", $this->emails)->notify(new ErrorNotify($error, $this->generateTraceMessage($exception)));
        return response()->json(array_merge([
            'exception_type' => get_class($exception),
            'exception_code' => $exception->getCode(),
            'exception_msg' => $exception->getMessage(),
        ], $error), $error["status_code"]);
    }

    /**
     * Returns a human readable line by line backtrace from exception backtrace
     * including the file, function which calls, and the line #.
     *
     * @param  \Exception  $exception
     * @return string
     */
    private function generateTraceMessage(Exception $exception) {
        $str = get_class($exception) . " Error " . $exception->getCode() . ": " . $exception->getMessage() .
            " in file " . $exception->getFile() . " line " . $exception->getLine() . "\n";
        foreach($exception->getTrace() as $trace)
        {
            $str .= ((isset($trace['file']))?$trace['file'] . ": ":"") .
                ((isset($trace['class']))?$trace['class'] . $trace['type']:"") .
                ((isset($trace['function']))?$trace['function'] . "(): ":"") .
                ((isset($trace['line']))?$trace['line'] . "\n":"\n");
        }
        return $str;
    }

    /**
     * Generate and return general error message depends on $exception
     * will return status code together with message by array
     * can return "send_email" as key and boolean value to decide force send an email or not
     *
     * @param Exception $exception
     * @return array
     */
    private function generateErrorResponse(Exception $exception)
    {
        $error = ["status_code" => 500, "message" => "Oh no! An error has occured on the server. Our developers have been notified on this. Please come back later when we fixed the problem."];
        if($exception instanceof \Illuminate\Auth\AuthenticationException)
            $error = ["status_code" => 418, "message" => 'Unauthenticated.'];
        elseif($exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
            $error = ["status_code" => 404, "message" => last(explode('\\', $exception->getModel())) . ' not found.'];
        elseif($exception instanceof \Illuminate\Auth\Access\AuthorizationException)
            $error = ["status_code" => 403, "message" => 'Not authorized.'];
        elseif($exception instanceof \Illuminate\Validation\ValidationException)
            $error = ["status_code" => 400, "message" => 'The given data was invalid.', "data" => ["errors" => $exception->validator->getMessageBag()]];
        elseif($exception instanceof DuplicateDataException)
            $error = ["status_code" => 422, "message" => 'Duplicate Data Found.'];
        return $error;
    }
}
