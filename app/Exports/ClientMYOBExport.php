<?php

namespace App\Exports;

use App\Models\Client;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ClientMYOBExport implements FromQuery, WithHeadings, WithMapping, WithCustomCsvSettings
{
    use Exportable;

    private $request = [];

    private $csv_structure = [
        'name' => 'Co./Last Name',
        'First Name',
        'Card ID',
        'Card Status',
        'Currency Code',
        'address1' => 'Addr 1 - Line 1',
        'address2' => '           - Line 2',
        '           - Line 3',
        '           - Line 4',
        'city' => '           - City',
        'state' => '           - State',
        'postcode' => '           - Postcode',
        'country' => '           - Country',
        'tel_no' => '           - Phone # 1',
        '           - Phone # 2',
        '           - Phone # 3',
        'fax_no' => '           - Fax #',
        '           - Email',
        '           - WWW',
        '           - Contact Name',
        '           - Salutation',
        'Addr 2 - Line 1',
        '           - Line 2',
        '           - Line 3',
        '           - Line 4',
        '           - City',
        '           - State',
        '           - Postcode',
        '           - Country',
        '           - Phone # 1',
        '           - Phone # 2',
        '           - Phone # 3',
        '           - Fax #',
        '           - Email',
        '           - WWW',
        '           - Contact Name',
        '           - Salutation',
        'Addr 3 - Line 1',
        '           - Line 2',
        '           - Line 3',
        '           - Line 4',
        '           - City',
        '           - State',
        '           - Postcode',
        '           - Country',
        '           - Phone # 1',
        '           - Phone # 2',
        '           - Phone # 3',
        '           - Fax #',
        '           - Email',
        '           - WWW',
        '           - Contact Name',
        '           - Salutation',
        'Addr 4 - Line 1',
        '           - Line 2',
        '           - Line 3',
        '           - Line 4',
        '           - City',
        '           - State',
        '           - Postcode',
        '           - Country',
        '           - Phone # 1',
        '           - Phone # 2',
        '           - Phone # 3',
        '           - Fax #',
        '           - Email',
        '           - WWW',
        '           - Contact Name',
        '           - Salutation',
        'Addr 5 - Line 1',
        '           - Line 2',
        '           - Line 3',
        '           - Line 4',
        '           - City',
        '           - State',
        '           - Postcode',
        '           - Country',
        '           - Phone # 1',
        '           - Phone # 2',
        '           - Phone # 3',
        '           - Fax #',
        '           - Email',
        '           - WWW',
        '           - Contact Name',
        '           - Salutation',
        'Picture',
        'Notes',
        'Identifiers',
        'Custom List 1',
        'Custom List 2',
        'Custom List 3',
        'Custom Field 1',
        'Custom Field 2',
        'Custom Field 3',
        'Billing Rate',
        'Terms - Payment is Due',
        '           - Discount Days',
        '           - Balance Due Days',
        '           - % Discount',
        '           - % Monthly Charge',
        'Tax Code',
        'Credit Limit',
        'Tax ID No.',
        'Volume Discount %',
        'Sales/Purchase Layout',
        'Price Level',
        'Payment Method',
        'Payment Notes',
        'Name on Card',
        'Card Number',
        'Expiry Date',
        'acc_code' => 'Account',
        'person_in_charge' => 'Salesperson',
        'Salesperson Card ID',
        'Comment',
        'Shipping Method',
        'Printed Form',
        'Freight Tax Code',
        'Use Customer\'s Tax Code',
        'Receipt Memo',
        'Invoice/Purchase Order Delivery',
        'Record ID',
        'Customer Business Reg No',
    ];

    private $csv_default_value = [
        'Card ID' => "*None",
        'Card Status' => "N",
        'Sales/Purchase Layout' => "S",
        'Use Customer\'s Tax Code' => "Y",
        'Invoice/Purchase Order Delivery' => "P",
    ];

    private $csv_integer_columns = [
        'Billing Rate' => 1,
        'Cost Per Hour' => 1,
        'Terms - Payment is Due' => 1,
        '           - Discount Days' => 1,
        '           - Balance Due Days' => 1,
        '           - % Discount' => 1,
        'Credit Limit' => 1,
        'Volume Discount %' => 1,
    ];

    /**
     * ExpenseMYOBExport constructor.
     * @param $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * @return Client|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    public function query()
    {
        return Client::query()->when(isset($this->request['uuid']), function ($query) {
            return $query->whereIn('uuid', $this->request['uuid']);
        });
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return array_values($this->csv_structure);
    }

    /**
     * @param mixed $client
     * @return array
     */
    public function map($client): array
    {
        $new_client = [];
        foreach($this->csv_structure as $key => $header)
            if(isset($client[$key]))
                $new_client[$key] = $this->wrapString($client[$key]);
            else
                $new_client[$key] = ((isset($this->csv_default_value[$header]))?$this->wrapString($this->csv_default_value[$header]):((isset($this->csv_integer_columns[$header]))?'0':''));
        return $new_client;
    }

    /**
     * @return array
     */
    public function getCsvSettings(): array
    {
        return [
            'enclosure' => false,
            'line_ending' => "\r\n",
        ];
    }

    /**
     * @param $value
     * @return string
     */
    private function wrapString($value) {
        $is_string = is_string($value);

        if($is_string){
            return '"' . $value . '"';
        }

        return $value;
    }
}
