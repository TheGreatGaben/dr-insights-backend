<?php

namespace App\Exports;

use App\Models\Expense;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ExpenseMYOBExport implements FromQuery, WithHeadings, WithMapping, WithCustomCsvSettings
{
    use Exportable;

    private $request = [];

    private function wrapString($value) {
        $is_string = is_string($value);

        if ($is_string) {
            return '"' . $value . '"';
        }

        return $value;
    }

    private $csv_structure = [
      'contractor_name' => 'Co./Last Name',
      'first_name' => 'First Name',
      'address_contractor_name' => 'Addr 1 - Line 1',
      'address_1' => '           - Line 2',
      'address_2' => '           - Line 3',
      'postcode_city_state' => '           - Line 4',
      'inclusive' => 'Inclusive',
      'ref_no' => 'Purchase #',
      'invoice_date' => 'Date',
      'cert_no' => 'Supplier Invoice #',
      'ship' => 'Ship Via',
      'delivery_status' => 'Delivery Status',
      'description' => 'Description',

      'account_no' => 'Account #',
      'payment_amt' => 'Amount',
      'payment_amt_with_tax' => 'Inc-Tax Amount',
      'job' => 'Job',
      'comment' => 'Comment',
      'journal_memo' => 'Journal Memo',
      'shipping_date' => 'Shipping Date',

      'tax_code' => 'Tax Code',
      'non_tax_amt' => 'Non-Tax Amount',
      'tax_amt' => 'Tax Amount',
      'import_duty_amt' => 'Import Duty Amount',
      'freight_amt' => 'Freight Amount',
      'freight_amt_with_tax' => 'Inc-Tax Freight Amount',
      'freight_tax_code' => 'Freight Tax Code',
      'freight_non_tax_amt' => 'Freight Non-Tax Amount',
      'freight_tax_amt' => 'Freight Tax Amount',
      'freight_import_duty_amt' => 'Freight Import Duty Amount',
      'purchase_status' => 'Purchase Status',
      'currency_code' => 'Currency Code',
      'exchange_rate' => 'Exchange Rate',
      'term' => 'Terms - Payment is Due',
      'discount_days' => '           - Discount Days',
      'balance_due_days' => '           - Balance Due Days',
      'discount' => '           - % Discount',
      'amount_paid' => 'Amount Paid',
      'category' => 'Category',
      'card_id' => 'Card ID',
      'record_id' => 'Record ID',
      'payment_details' => 'Payment Details',
    ];

    /**
     * custom value mapping from Expense modal
     * @param Expense $expense
     * @return array
     */
    private function formattedColumns(Expense $expense) {
        $expense_date = new Carbon($expense->invoice_date);
        $has_postcode_city_or_state = isset($expense->contractor->postcode, $expense->contractor->city, $expense->contractor->state);

        $payment_amt = round($expense->total_amount, 2);
        $payment_amt_with_tax = round($expense->total_amount + $expense->sst + $expense->gst, 2);
        $tax_amt = round($expense->sst + $expense->gst, 2);
        $amount_paid = round($expense->total_amount - $expense->balance, 2);


        return [
            'contractor_name' => $expense->contractor->name,
            'invoice_date' => $expense_date->format('j/n/Y'),
            'address_contractor_name' => $expense->contractor->name,
            'postcode_city_state' => $has_postcode_city_or_state ? $expense->contractor->postcode . ' ' . $expense->contractor->city . ', ' . $expense->contractor->state : NULL,
            'delivery_status' => 'P',
            'account_no' => (($expense->accCode)?$expense->accCode->code:""),
            'payment_amt' => "$payment_amt",
            'payment_amt_with_tax' => "$payment_amt_with_tax",
            'journal_memo' => 'Purchase; ' . $expense->contractor->name,
            'purchase_status' => 'B',
            'ref_no' => substr(trim(str_replace("Inv:", "", $expense->ref_no)), -8),

            'tax_code' => 'N-T',
            'non_tax_amt' => 0,
            'tax_amt' => $tax_amt,
            'amount_paid' => $amount_paid,
            'card_id' => '*None',
            'payment_details' => (($expense->payments->count())?$expense->payments->first()->payment_details:""),
        ];
    }

    /**
     * ExpenseMYOBExport constructor.
     * @param $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    public function query()
    {
        return Expense::with(['project', 'contractor', 'payments' => function($q){
            $q->select(["expenses_id", \DB::raw("group_concat(concat(payments.payment_date, \" : \", COALESCE(payments.cheque_no, \"\") , \" : RM \", FORMAT(payments.payment_amt, 2)) SEPARATOR \"\n\") as 'payment_details'")])->groupBy(["expenses_id"]);
        }])->whereIn('uuid', $this->request['uuid']);
    }

    public function headings(): array
    {
        return array_values($this->csv_structure);
    }

    public function map($expense): array
    {
        $formatted_columns = $this->formattedColumns($expense);
        $new_expense = [];

        foreach($this->csv_structure as $key => $header) {
            if (isset($formatted_columns[$key])) {
                $new_expense[$key] = $this->wrapString($formatted_columns[$key]);
            }
            else if (isset($expense[$key])) {
                $new_expense[$key] = $this->wrapString($expense[$key]);
            }
            else {
                $new_expense[$key] = '';
            }
        }

        return $new_expense;
    }

    public function getCsvSettings(): array
    {
        return [
            'enclosure' => false,
            'line_ending' => "\r\n\r\n",
        ];
    }
}
