<?php

namespace App\Exports;

use App\Models\Invoice;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class InvoiceMYOBExport implements FromQuery, WithHeadings, WithMapping, WithCustomCsvSettings, WithStrictNullComparison
{
    use Exportable;

    private $request = [];

    private function wrapString($value) {
        $is_string = is_string($value);

        if ($is_string) {
            return '"' . $value . '"';
        }

        return $value;
    }

    private $csv_structure = [
      'client_name' => 'Co./Last Name',
      'first_name' => 'First Name',
      'address_client_name' => 'Addr 1 - Line 1',
      'address_1' => '           - Line 2',
      'address_2' => '           - Line 3',
      'postcode_city_state' => '           - Line 4',
      'country' => 'Destination Country',
      'inclusive' => 'Inclusive',
      'invoice_no' => 'Invoice #',
      'invoice_date' => 'Date',
      'cert_no' => 'Customer PO',
      'ship' => 'Ship Via',
      'delivery_status' => 'Delivery Status',
      'description' => 'Description',

      'account_no' => 'Account #',
      'payment_amt' => 'Amount',
      'payment_amt_with_tax' => 'Inc-Tax Amount',
      'job' => 'Job',
      'comment' => 'Comment',
      'journal_memo' => 'Journal Memo',

      'person_in_charge' => 'Salesperson Last Name',
      'person_in_charge_first_name' => 'Salesperson First Name',
      'shipping_date' => 'Shipping Date',
      'referral_source' => 'Referral Source',

      'tax_code' => 'Tax Code',
      'non_tax_amt' => 'Non-Tax Amount',
      'tax_amt' => 'Tax Amount',
      'freight_amt' => 'Freight Amount',
      'freight_amt_with_tax' => 'Inc-Tax Freight Amount',
      'freight_tax_code' => 'Freight Tax Code',
      'freight_non_tax_amt' => 'Freight Non-Tax Amount',
      'freight_tax_amt' => 'Freight Tax Amount',
      'sale_status' => 'Sale Status',
      'currency_code' => 'Currency Code',
      'exchange_rate' => 'Exchange Rate',
      'term' => 'Terms - Payment is Due',
      'discount_days' => '           - Discount Days',
      'balance_due_days' => '           - Balance Due Days',
      'discount' => '           - % Discount',
      'monthly_charge' => '           - % Monthly Charge',
      'amount_paid' => 'Amount Paid',
      'payment_method' => 'Payment Method',
      'payment_notes' => 'Payment Notes',
      'name_on_card' => 'Name on Card',
      'card_number' => 'Card Number',
      'expiry_date' => 'Expiry Date',
      'authorisation_code' => 'Authorisation Code',
      'account_number' => 'Account Number',
      'account_name' => 'Account Name',
      'cheque_number' => 'Cheque Number',
      'category' => 'Category',
      'card_id' => 'Card ID',
      'record_id' => 'Record ID',
      'payment_details' => 'Payment Details',
    ];

    /**
     * custom value mapping from Invoice modal
     * @param Invoice $invoice
     * @return array
     */
    private function formattedColumns(Invoice $invoice) {
        $invoice_date = new Carbon($invoice->invoice_date);
        $has_postcode_city_or_state = isset($invoice->client->postcode, $invoice->client->city, $invoice->client->state);

        $payment_amt = round($invoice->total_amount, 2);
        $payment_amt_with_tax = round($invoice->total_amount + $invoice->sst + $invoice->gst, 2);
        $tax_amt = round($invoice->sst + $invoice->gst, 2);
        $amount_paid = round($invoice->total_amount - $invoice->balance, 2);

        return [
            'client_name' => $invoice->client->name,
            'invoice_date' => $invoice_date->format('j/n/Y'),
            'address_client_name' => $invoice->client->name,
            'postcode_city_state' => $has_postcode_city_or_state ? $invoice->client->postcode . ' ' . $invoice->client->city . ', ' . $invoice->client->state : NULL,
            'delivery_status' => 'A',
            'account_no' => (($invoice->accCode)?$invoice->accCode->code:""),
            'payment_amt' => "$payment_amt",
            'payment_amt_with_tax' => "$payment_amt_with_tax",
            'journal_memo' => 'Sale; ' . $invoice->client->name,
            'person_in_charge' => $invoice->client->person_in_charge,
            'invoice_no' => substr(trim(str_replace("Inv:", "", $invoice->invoice_no)), -8),
            'tax_code' => 'N-T',
            'non_tax_amt' => "0.00",
            'tax_amt' => $tax_amt,
            'amount_paid' => $amount_paid,
//            'sale_status' => 'I',
            'card_id' => '*None',
            'payment_details' => (($invoice->payments->count())?$invoice->payments->first()->payment_details:""),
        ];
    }

    /**
     * InvoiceMYOBExport constructor.
     * @param $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    public function query()
    {
        return Invoice::with(['accCode', 'project', 'client', 'payments' => function($q){
            $q->select(["invoice_id", \DB::raw("group_concat(concat(payments.payment_date, \" : \", COALESCE(payments.cheque_no, \"\") , \" : RM \", FORMAT(payments.payment_amt, 2)) SEPARATOR \"\n\") as 'payment_details'")])->groupBy(["invoice_id"]);
        }])->whereIn('uuid', $this->request['uuid']);
    }

    public function headings(): array
    {
        return array_values($this->csv_structure);
    }

    public function map($invoice): array
    {
        $formatted_columns = $this->formattedColumns($invoice);
        $new_invoice = [];

        foreach($this->csv_structure as $key => $header) {
            if (isset($formatted_columns[$key])) {
                $new_invoice[$key] = $this->wrapString($formatted_columns[$key]);
            }
            else if (isset($invoice[$key])) {
                $new_invoice[$key] = $this->wrapString($invoice[$key]);
            }
            else {
                $new_invoice[$key] = '';
            }
        }

        return $new_invoice;
    }

    public function getCsvSettings(): array
    {
        return [
            'enclosure' => false,
            'line_ending' => "\r\n\r\n",
        ];
    }
}
