<?php
namespace App\Http\Controllers\Auth;

use Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class PasswordChangeController extends Controller
{
    /**
     * Change password
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse [string] message
     */
    public function change(Request $request)
    {
        $request->validate([
            'password' => 'required|string|confirmed',
        ]);
        $user = $request->user();
        if (!$user) {
            return response()->json([
                'message' => 'User not found'
            ], 404);
        }
        // Check that the same old password is not being reused
        if (Hash::check($request->get('password'), $user->password)) {
            return response()->json([
                'message' => 'Current password and new password are the same'
            ], 400);
        }
        $user->password = $request->password;
        $user->save();
        return response()->json($user);
    }
}
