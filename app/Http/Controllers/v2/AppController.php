<?php

namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;

class AppController extends Controller
{
    public function runScript($file){
        if(file_exists($file = app_path("Sandbox/" . $file . ".php")))
            include($file);
    }
}
