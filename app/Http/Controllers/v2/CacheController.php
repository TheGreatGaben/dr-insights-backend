<?php

namespace App\Http\Controllers\v2;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;

class CacheController extends AppController
{
    /**
     * default cache driver for controller
     *
     * @var string
     */
    private $driver = "database"; // 1 day

    /**
     * all available cache driver
     *
     * @var array
     */
    private $avaiDrivers = ["apc", "array", "database", "file", "memcached", "redis"];

    /**
     * default cache expiry time in minutes
     *
     * @var int
     */
    private $expiryTime = 1440; // 1 day

    /**
     * default cache data that will not to store in database
     *
     * @var array
     */
    private $default_keys = ["cache_key", "cache_id", "cache_data", "cache_expiry_time", "cache_driver"];

    /**
     * to store request
     *
     * @var Request
     */
    private $request;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function index(Request $request){
        $cached = $this->cache($request)->get($this->getKey());
        if($this->request->has("cache_data") && !empty(Arr::get($cached, $this->request->get("cache_data"))))
            $cached = Arr::get($cached, $this->request->get("cache_data"));
        return $cached;
    }

    /**
     * Store a newly created/replaced old resource in storage.
     *
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function store(Request $request){
        $this->cache($request)->put($this->getKey(), $request->except($this->default_keys), $this->expiryTime());
        return $this->index($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function update(Request $request){
        $newCache = $request->except($this->default_keys);
        if(!empty($cached = $this->index($request)))
            if($this->request->has("cache_data") && !empty(Arr::get($cached, $this->request->get("cache_data"))))
                Arr::set($cached, $this->request->get("cache_data"), $newCache);
            else
                $cached = array_merge($cached, $newCache);
        else
            $cached = $newCache;
        $this->cache($request)->put($this->getKey(), $cached, $this->expiryTime());
        return $this->index($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return void
     * @throws \Exception
     */
    public function destroy(Request $request){
        $this->cache($request)->forget($this->getKey());
    }

    /**
     * load cache to correct driver and preparation
     *
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function cache(Request $request){
        $this->request = $request;
        $driver = $this->driver;
        if($request->has("cache_driver") && in_array($request->get("cache_driver"), $this->avaiDrivers))
            $driver = $request->get("cache_driver");
        return cache()->store($driver);
    }

    /**
     * generate key to get, store and update cache
     *
     * @return string
     */
    public function getKey(){
        if($this->request->has("cache_id"))
            $id = $this->request->get("cache_id");
        elseif($this->request->user())
            $id = $this->request->user()->id;
        return (($this->request->has("cache_key"))?"_" . $this->request->get("cache_key"):"") . (($id)?"_" . $id:"");
    }

    /**
     * return and set cache expiry time
     *
     * @return int
     */
    public function expiryTime(){
        $lifetime = $this->expiryTime;
        if($this->request->has("cache_expiry_time"))
            $lifetime = $this->request->get("cache_expiry_time");
        return $lifetime;
    }
}
