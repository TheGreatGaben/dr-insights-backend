<?php

namespace App\Http\Controllers\v2;

use Carbon\Carbon;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CashflowController extends Controller
{
    public function getProjection(Request $request, Project $project) {
        $days = $request->days;
        if (!$days) {
            return response()->json([
                'message' => 'Missing \'days\' parameter.'
            ], 400);
        }

        // Days can be 30, 60, 90 and above90
        $from = Carbon::now()->subMonthNoOverflow(1)->startOfMonth();
        if ($days == 30) {
            $to = $from->copy()->addMonthNoOverflow(1)->endOfMonth();
        }elseif($days == 60){
            $to = $from->copy()->addMonthNoOverflow(2)->endOfMonth();
        }elseif($days == 90){
            $to = $from->copy()->addMonthNoOverflow(3)->endOfMonth();
        }

        $project->load(
        ['invoices' => function ($query) use ($from, $to) {
            if ($to) {
                $query->whereBetween('due_date',[$from, $to])
                    ->where('balance', '>', 0);
            } else {
                $query->whereDate('due_date', '>=', $from)
                    ->where('balance', '>', 0);
            }
        }, 'expenses' => function ($query) use ($from, $to) {
            if ($to) {
                $query->whereBetween('due_date',[$from, $to])
                    ->where('balance', '>', 0);
            } else {
                $query->whereDate('due_date', '>=', $from)
                    ->where('balance', '>', 0);
            }
        }]);

        $cashflow_in = $project->invoices->sum('balance');
        $cashflow_out = $project->expenses->sum('balance');

        $payload = [];
        $payload['start_date'] = $from;
        $payload['end_date'] = $to;
        $payload['cashflow_in'] = $this->formatCurrencyValue($cashflow_in);
        $payload['cashflow_out'] = $this->formatCurrencyValue($cashflow_out);
        $payload['nett_cashflow'] = $this->formatCurrencyValue($cashflow_in - $cashflow_out);
        $payload['invoices'] = $project->invoices;
        $payload['expenses'] = $project->expenses;

        return ['data' => $payload];
    }

    private function formatCurrencyValue($value) {
        return number_format($value, 2, '.', '');
    }
}
