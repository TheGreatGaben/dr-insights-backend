<?php

namespace App\Http\Controllers\v2;

use App\Models\Client;
use App\Queries\v2 AS Query;
use Maatwebsite\Excel\Excel;
use App\Exports\ClientMYOBExport;
use App\Http\Requests\ClientRequest AS Request;

class ClientController extends AppController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request){
        return with(new Query\ClientQuery($request))->getAll();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request){
        return [];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request){
        $client = Client::create($request->validated());
        return $client;
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Client $client
     * @return mixed
     */
    public function show(Request $request, Client $client){
        return with(new Query\ClientQuery($request, $client))->getAll();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Client $client
     * @return mixed
     */
    public function edit(Request $request, Client $client){
        return with(new Query\ClientQuery($request, $client))->getAll();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Client $client
     * @return mixed
     */
    public function update(Request $request, Client $client){
        $client->fill($request->validated())->save();
        return $client;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Client $client
     * @return int
     * @throws \Exception
     */
    public function destroy(Request $request, Client $client){
        $client->delete();
        return 204;
    }

    /**
     * Display a report of the resource.
     *
     * @param Request $request
     * @param Client $client
     * @return int
     * @throws \Exception
     */
    public function report(Request $request, Client $client = null){
        return with(new Query\ClientQuery($request, $client))->getClientsReport();
    }

    /**
     * get all resource with invoices.
     *
     * @param Request $request
     * @param Client $client
     * @return mixed
     * @throws \Exception
     */
    public function clientsInvoice(Request $request, Client $client = null){
        return with(new Query\ClientQuery($request, $client))->getAllWithProjectInvoices();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportMYOB(Request $request){
        return (new ClientMYOBExport($request))
            ->download('clients.txt', Excel::CSV, [
                'Content-Type' => 'text/plain',
            ]);
    }
}
