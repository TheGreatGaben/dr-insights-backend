<?php

namespace App\Http\Controllers\v2;

use App\Models\Contractor;
use App\Models\Project;
use App\Queries\v2 AS Query;
use Maatwebsite\Excel\Excel;
use App\Exports\ContractorMYOBExport;
use App\Http\Requests\ContractorRequest AS Request;

class ContractorController extends AppController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request, Project $project = null){
        return with(new Query\ContractorQuery($request))->getAll($project);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request){
        return [];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request){
        $payload = $request->validated();
        unset($payload['project_id']);
        $contractor = Contractor::create($payload);
        return ["contractor" => $contractor];
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Contractor $contractor
     * @return mixed
     */
    public function show(Request $request, Contractor $contractor){
        return with(new Query\ContractorQuery($request, $contractor))->getAll();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Contractor $contractor
     * @return mixed
     */
    public function edit(Request $request, Contractor $contractor){
        return with(new Query\ContractorQuery($request, $contractor))->getAll();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Contractor $contractor
     * @return mixed
     */
    public function update(Request $request, Contractor $contractor){
        $payload = $request->validated();
        unset($payload['project_id']);
        $contractor->fill($payload)->save();
        return $contractor;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Contractor $contractor
     * @return mixed
     * @throws \Exception
     */
    public function destroy(Request $request, Contractor $contractor){
        $contractor->delete();
        return 204;
    }

    /**
     * Display a report of the resource.
     *
     * @param Request $request
     * @param Contractor $contractor
     * @return int
     * @throws \Exception
     */
    public function report(Request $request, Contractor $contractor = null){
        return with(new Query\ContractorQuery($request, $contractor))->getContractorsReport();
    }

    /**
     * get all resource with expenses.
     *
     * @param Request $request
     * @param Contractor $contractor
     * @return mixed
     * @throws \Exception
     */
    public function contractorsExpenses(Request $request, Contractor $contractor){
        return with(new Query\ContractorQuery($request, $contractor))->getAllContractorsWithExpenses();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportMYOB(Request $request){
        return (new ContractorMYOBExport($request))
            ->download('contractors.txt', Excel::CSV, [
                'Content-Type' => 'text/plain',
            ]);
    }
}
