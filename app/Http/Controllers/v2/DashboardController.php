<?php
namespace App\Http\Controllers\v2;

use App\Queries\v2 AS Query;
use Illuminate\Http\Request;

class DashboardController extends AppController
{
    /**
     * get dashboard overall summary data
     *
     * @param Request $request
     * @return array
     */
    public function getOverallSummary(Request $request){
        $results = [];
        $results["invoice"] = with(new Query\InvoiceQuery($request))->getSummary();
        $results["expense"] = with(new Query\ExpenseQuery($request))->getSummary();
        // $results["payment"] = with(new Query\PaymentQuery($request))->getSummary();
        $results["payment"]["cashflow_nett"] = round($results["invoice"]["total_received_in_period"] - $results["expense"]["total_paid_in_period"], 2);
        $results["payment"]["projected_cashflow_nett"] = round($results["invoice"]["total_invoiced"] - $results["expense"]["total_expenses"], 2);
        return $results;
    }

    /**
     * get dashboard projects summary data
     *
     * @param Request $request
     * @return mixed
     */
    public function getProjectSummary(Request $request){
        return with(new Query\ProjectQuery($request))->getAll();
    }

    /**
     * get dashboard overdue summary data
     *
     * @param Request $request
     * @return array
     */
    public function getOverdueSummary(Request $request){
        $results = [];
        $results["invoice"] = with(new Query\InvoiceQuery($request))->getOverdueSummary();
        $results["expense"] = with(new Query\ExpenseQuery($request))->getOverdueSummary();
        return $results;
    }
}
