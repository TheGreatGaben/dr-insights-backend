<?php
namespace App\Http\Controllers\v2;

use App\Exports\ExpenseMYOBExport;
use App\Models\Project;
use App\Models\Expense;
use App\Queries\v2 AS Query;
use App\Http\Requests\ExpenseRequest as Request;
use Maatwebsite\Excel\Excel;

class ExpenseController extends AppController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param Project|null $project
     * @return mixed
     */
    public function index(Request $request, Project $project = null){
        return with(new Query\ExpenseQuery($request))->getAll($project);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return array
     */
    public function create(Request $request){
        return [];
    }

    /**
     * Store a newly created resource.
     *
     * @param  Request  $request
     * @return mixed
     */
    public function store(Request $request){
        $expense = Expense::create($request->validated());
        return $expense;
    }

    /**
     * Display the resource details.
     *
     * @param Request $request
     * @param Expense $expense
     * @return mixed
     */
    public function show(Request $request, Expense $expense){
        return $expense;
    }

    /**
     * Show the form for editing the resource.
     *
     * @param Request $request
     * @param Expense $expense
     * @return mixed
     */
    public function edit(Request $request, Expense $expense){
        return $expense;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Expense $expense
     * @return mixed
     */
    public function update(Request $request, Expense $expense){
        $expense->fill($request->validated())->save();
        return $expense;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Expense $expense
     * @return mixed
     * @throws \Exception
     */
    public function destroy(Request $request, Expense $expense){
        $expense->delete();
        return 204;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportMYOB(Request $request){
        return (new ExpenseMYOBExport($request))
            ->download('expenses.txt', Excel::CSV, [
                'Content-Type' => 'text/plain',
            ]);
    }

    /**
     * Write Off Expenses
     * @param Request $request
     * @return array
     */
    public function writeOff(Request $request, Expense $expense) {
        $expense->write_off_reason = $request->get('write_off_reason');
        $expense->save();
        return $expense;
    }

    /**
     * Undo Write Off Expenses
     * @param Request $request
     * @return array
     */
    public function undoWriteOff(Request $request, Expense $expense) {
        $expense->write_off_date = null;
        $expense->write_off_reason = null;
        $expense->write_off_user = null;

        $expense->balance = $expense->write_off_amt;
        $expense->write_off_amt = 0;
        $expense->save();
        return 204;
    }
}
