<?php

namespace App\Http\Controllers\v2;

use App\Models\FinanceAccountCode;
use App\Queries\v2\FinanceAccountCodeQuery AS Query;
use App\Http\Requests\FinanceAccountCodeRequest AS Request;

class FinanceAccountCodeController extends AppController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param FinanceAccountCode|null $finance_account_code
     * @return mixed
     */
    public function index(Request $request, FinanceAccountCode $finance_account_code = null)
    {
        return (new Query($request, $finance_account_code))->getAll();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return array
     */
    public function create(Request $request)
    {
        return [];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return void
     */
    public function store(Request $request)
    {
        $financeAccountCode = FinanceAccountCode::create($request->validated());
        return $financeAccountCode;
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param FinanceAccountCode $finance_account_code
     * @return mixed
     */
    public function show(Request $request, FinanceAccountCode $finance_account_code)
    {
        return with(new Query($request, $finance_account_code))->getAll();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param FinanceAccountCode $finance_account_code
     * @return mixed
     */
    public function edit(Request $request, FinanceAccountCode $finance_account_code)
    {
        return with(new Query($request, $finance_account_code))->getAll();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param FinanceAccountCode $finance_account_code
     * @return mixed
     */
    public function update(Request $request, FinanceAccountCode $finance_account_code)
    {
        $finance_account_code->fill($request->validated())->save();
        return $finance_account_code;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param FinanceAccountCode $finance_account_code
     * @return int
     * @throws \Exception
     */
    public function destroy(Request $request, FinanceAccountCode $finance_account_code)
    {
        $finance_account_code->forceDelete();
        return 204;
    }

    /**
     * Query descendants and self of the $finance_account_code
     * @param Request $request
     * @param FinanceAccountCode $finance_account_code
     * @return mixed
     */
    private function getDescendantsAndSelf(Request $request, FinanceAccountCode $finance_account_code) {
        return (new Query($request))->query()->descendantsAndSelf($finance_account_code->id);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getChildByCode(Request $request)
    {
        $codes = hasValue("code");
        $list = collect([]);

        if (is_array($codes)) {
            foreach($codes as $code) {
                $finance_account_code = FinanceAccountCode::where("code", "=", $code)->select(["id"])->first();
                if($finance_account_code) {
                    $list = $list->merge($this->getDescendantsAndSelf($request, $finance_account_code));
                }
            }

            return $list;
        } else {
            $finance_account_code = FinanceAccountCode::where("code", "=", $codes)->select(["id"])->first();

            if($finance_account_code) {
                return $this->getDescendantsAndSelf($request, $finance_account_code);
            }
        }
    }
}
