<?php
namespace App\Http\Controllers\v2;

use App\Exports\InvoiceMYOBExport;
use App\Models\Project;
use App\Models\Invoice;
use App\Queries\v2 AS Query;
use App\Http\Requests\InvoiceRequest AS Request;
use Maatwebsite\Excel\Excel;

class InvoiceController extends AppController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param Project|null $project
     * @return mixed
     */
    public function index(Request $request, Project $project = null){
        return with(new Query\InvoiceQuery($request))->getAll($project);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request){
        return [];
    }

    /**
     * Store a newly created resource.
     *
     * @param  Request  $request
     * @return mixed
     */
    public function store(Request $request){
        $invoice = Invoice::create($request->validated());
        return $invoice;
    }

    /**
     * Display the resource details.
     *
     * @param Request $request
     * @param Invoice $invoice
     * @return mixed
     */
    public function show(Request $request, Invoice $invoice){
        return $invoice;
    }

    /**
     * Show the form for editing the resource.
     *
     * @param Request $request
     * @param Invoice $invoice
     * @return mixed
     */
    public function edit(Request $request, Invoice $invoice){
        return $invoice;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Invoice $invoice
     * @return mixed
     */
    public function update(Request $request, Invoice $invoice){
        $invoice->fill($request->validated())->save();
        return $invoice;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Invoice $invoice
     * @return array
     * @throws \Exception
     */
    public function destroy(Request $request, Invoice $invoice){
        $invoice->delete();
        return 204;
    }

    /**
     * Display resource summary data
     *
     * @param Request $request
     * @return mixed
     */
    public function getSummary(Request $request){
        return with(new Query\InvoiceQuery($request))->getSummary();
    }

    /**
     * Get project's prev certified
     * sum up all `total_certified` invoice values
     * @param Request $request
     * @return array
     */
    public function getPrevCertified(Request $request){
        $prev_certified = Invoice::where('project_id', $request->project_id)->sum('nett_certified');
        return [
            "prev_certified" => $prev_certified,
        ];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportMYOB(Request $request){
        return (new InvoiceMYOBExport($request))
            ->download('invoices.txt', Excel::CSV, [
                'Content-Type' => 'text/plain',
            ]);
    }

    /**
     * Write Off Invoices
     * sum up all `total_certified` invoice values
     * @param Request $request
     * @return array
     */
    public function writeOff(Request $request, Invoice $invoice) {
        $invoice->write_off_reason = $request->get('write_off_reason');
        $invoice->save();
        return $invoice;
    }

    /**
     * Undo Write Off Invoices
     * @param Request $request
     * @return array
     */
    public function undoWriteOff(Request $request, Invoice $invoice) {
        $invoice->write_off_date = null;
        $invoice->write_off_reason = null;
        $invoice->write_off_user = null;

        $invoice->balance = $invoice->write_off_amt;
        $invoice->write_off_amt = 0;
        $invoice->save();
        return 204;
    }
}
