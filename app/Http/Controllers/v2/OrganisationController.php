<?php

namespace App\Http\Controllers\v2;

use App\Models\Organisation;
use App\Queries\v2 AS Query;
use App\Http\Requests\OrganisationRequest AS Request;
use Illuminate\Http\Response;

class OrganisationController extends AppController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request){
        return with(new Query\OrganisationQuery($request))->getAllOrganisation();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function create(){
        return [
            "data" => [],
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Organisation|\Illuminate\Database\Eloquent\Model
     */
    public function store(Request $request){
        $organisation = Organisation::create($request->only("name"));
        return $organisation;
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Organisation $organisation
     * @return Organisation
     */
    public function show(Request $request, Organisation $organisation){
        return $organisation;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Organisation $organisation
     * @return array
     */
    public function edit(Request $request, Organisation $organisation){
        return [
            "data" => $organisation,
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Organisation $organisation
     * @return Organisation
     */
    public function update(Request $request, Organisation $organisation){
        $organisation->fill($request->only('name'))->save();
        return $organisation;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Organisation $organisation
     * @return int
     * @throws \Exception
     */
    public function destroy(Request $request, Organisation $organisation){
        $organisation->delete();
        return 204;
    }
}
