<?php
namespace App\Http\Controllers\v2;

use App\Models\Payment;
use App\Queries\v2 AS Query;
use App\Http\Requests\PaymentRequest AS Request;

class PaymentController extends AppController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request){
        return with(new Query\PaymentQuery($request))->getAll();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return array
     */
    public function create(Request $request){
        return [];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return array
     */
    public function store(Request $request){
        $request->extraRules();
        $payment = Payment::create($request->validated());
        return $payment;
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Payment $payment
     * @return array
     */
    public function show(Request $request, Payment $payment){
        return $payment;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Payment $payment
     * @return array
     */
    public function edit(Request $request, Payment $payment){
        return $payment;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Payment $payment
     * @return array
     */
    public function update(Request $request, Payment $payment){
        $request->extraRules();
        $payment->fill($request->validated())->touch();
        return $payment;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Payment $payment
     * @return int
     * @throws \Exception
     */
    public function destroy(Request $request, Payment $payment){
        $payment->delete();
        return 204;
    }
}
