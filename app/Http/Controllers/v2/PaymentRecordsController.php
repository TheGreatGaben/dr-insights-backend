<?php

namespace App\Http\Controllers\v2;

use App\Models\PaymentRecord;
use App\Queries\v2 AS Query;
use App\Http\Requests\PaymentRecordRequest as Request;

class PaymentRecordsController extends AppController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request){
        return with(new Query\PaymentRecordQuery($request))->getAll();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create(Request $request, PaymentRecord $paymentRecord){
        return [];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function store(Request $request){
        $request->extraRules();
        $paymentRecord = PaymentRecord::create($request->only(["bank", "remark"]));
        return with(new Query\PaymentRecordQuery($request, $paymentRecord))->getAll();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show(Request $request, PaymentRecord $paymentRecord){
        return with(new Query\PaymentRecordQuery($request, $paymentRecord))->getAll();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit(Request $request, PaymentRecord $paymentRecord){
        return [];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param PaymentRecord $paymentRecord
     * @return mixed
     */
    public function update(Request $request, PaymentRecord $paymentRecord){
        $paymentRecord->fill($request->validated())->save();
        return with(new Query\PaymentRecordQuery($request, $paymentRecord))->getAll();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param PaymentRecord $paymentRecord
     * @return void
     * @throws \Exception
     */
    public function destroy(Request $request, PaymentRecord $paymentRecord){
        $payments = $paymentRecord->payments;
        foreach($payments as $payment) {
            $payment->delete();
        }
        $paymentRecord->delete();
    }
}
