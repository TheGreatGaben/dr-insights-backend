<?php
namespace App\Http\Controllers\v2;

use App\Models\Role;
use App\Models\Permission;
use App\Queries\v2 AS Query;
use App\Http\Requests\PermissionRequest AS Request;

class PermissionController extends AppController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request){
        return with(new Query\PermissionQuery($request))->getAllPermission();
    }

    /**
     * Show the form for creating a new permission.
     *
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request){
        return [
            "data" => [],
            "roles" => Role::addSelect(["id", "name"])->get(),
        ];
    }

    /**
     * Store a newly created role in storage.
     *
     * @param  Request  $request
     * @return mixed
     */
    public function store(Request $request){
        $permission = Permission::create($request->only(["name"]));
        return $permission;
    }

    /**
     * Display the specified role with all permissions.
     *
     * @param Request $request
     * @param Permission $permission
     * @return mixed
     */
    public function show(Request $request, Permission $permission){
        $permission->allRoles = with(new Query\PermissionQuery(null, $permission))->getPermissionRole();
        return $permission;
    }

    /**
     * Show the form for editing the specified role with all permissions.
     *
     * @param Request $request
     * @param Permission $permission
     * @return mixed
     */
    public function edit(Request $request, Permission $permission){
        $permission->allRoles = with(new Query\PermissionQuery(null, $permission))->getPermissionRole();
        return [
            "data" => $permission,
            "role" => Role::addSelect(["id", "name"])->get()
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Permission  $permission
     * @return mixed
     */
    public function update(Request $request, Permission $permission){
        $permission->fill($request->only('name'))->touch();
        return $permission;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Permission $permission
     * @return mixed
     * @throws \Exception
     */
    public function destroy(Request $request, Permission $permission){
        $permission->delete();
        return 204;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getAllProjectPermissions(Request $request){
        return with(new Query\PermissionQuery($request))->getAllProjectPermissions();
    }
}
