<?php

namespace App\Http\Controllers\v2;

use App\Models\Project;
use App\Queries\v2 as Query;
use App\Http\Requests\ProjectRequest as Request;

class ProjectController extends AppController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request, Project $project = null){
        return with(new Query\ProjectQuery($request, $project))->getAll();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return array
     */
    public function create(Request $request){
        return [];
    }

    /**
     * Store a newly created resource.
     *
     * @param Request $request
     * @return array
     */
    public function store(Request $request){
        $project = Project::create($request->except("project_manager"));
        return $project;
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Project $project
     * @return array
     */
    public function show(Request $request, Project $project){
        return with(new Query\ProjectQuery($request, $project))->getAll();
    }

    /**
     * Show the form for editing the resource.
     *
     * @param Request $request
     * @param Project $project
     * @return array
     */
    public function edit(Request $request, Project $project){
        return (new Query\ProjectQuery($request, $project))->getAll();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Project $project
     * @return array
     */
    public function update(Request $request, Project $project){
        $project->fill($request->except("project_manager"))->save();
        return $project;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Project $project
     * @return array
     * @throws \Exception
     */
    public function destroy(Request $request, Project $project){
        $project->delete();
        return 204;
    }

    /**
     * @param Request $request
     * @param Project|null $project
     * @return mixed
     */
    public function getSummary(Request $request, Project $project = null){
        return with(new Query\ProjectQuery($request, $project))->getSummary();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getProjectsForBulkPayment(Request $request){
        return with(new Query\ProjectQuery($request))->getProjectsForBulkPayment()->get();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getEndCustomers(Request $request){
        return \DB::table('projects')->distinct()->whereNotNull('end_customer')->get(['end_customer']);
    }

    /**
     * @param Request $request
     * @param Project $project
     * @return mixed
     */
    public function getProjectionCashflow(Request $request, Project $project){
        return with(new Query\ProjectQuery($request, $project))->getProjectionCashflow();
    }
}
