<?php

namespace App\Http\Controllers\v2;

use App\Models\PurchaseOrder;
use App\Queries\v2 AS Query;
use App\Http\Requests\PurchaseOrderRequest AS Request;

class PurchaseOrderController extends AppController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request){
        return with(new Query\PurchaseOrderQuery($request))->getAll();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request){
        return [];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return mixed
     * @throws \App\Exceptions\DuplicateDataException
     */
    public function store(Request $request){
        $request->extraRules();
        $purchaseOrder = PurchaseOrder::create($request->validated());
        return $purchaseOrder;
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param PurchaseOrder $purchaseOrder
     * @return mixed
     */
    public function show(Request $request, PurchaseOrder $purchaseOrder){
        return $purchaseOrder;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param PurchaseOrder $purchaseOrder
     * @return mixed
     */
    public function edit(Request $request, PurchaseOrder $purchaseOrder){
        return $purchaseOrder;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param PurchaseOrder $purchaseOrder
     * @return mixed
     * @throws \App\Exceptions\DuplicateDataException
     */
    public function update(Request $request, PurchaseOrder $purchaseOrder){
        $request->extraRules();
        $purchaseOrder->fill($request->validated())->save();
        return $purchaseOrder;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param PurchaseOrder $purchaseOrder
     * @return mixed
     * @throws \Exception
     */
    public function destroy(Request $request, PurchaseOrder $purchaseOrder){
        $purchaseOrder->delete();
        return 204;
    }
}
