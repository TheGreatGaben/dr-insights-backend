<?php
namespace App\Http\Controllers\v2;

use App\Models\Role;
use App\Queries\v2 AS Query;
use App\Http\Requests\RoleRequest as Request;

class RoleController extends AppController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request){
        return with(new Query\RoleQuery($request))->getAllRole();
    }

    /**
     * Show the form for creating a new role.
     *
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request){
        return [
            "data" => [],
            "permission" => with(new Query\PermissionQuery())->getAllPermission(),
        ];
    }

    /**
     * Store a newly created role in storage.
     *
     * @param  Request  $request
     * @return mixed
     */
    public function store(Request $request){
        $role = Role::create($request->only("name"));
        $role->syncPermissions($request->only('permission'));
        unset($role->permissions);
        $role->allPermissions = with(new Query\RoleQuery(null, $role))->getRolePermission();
        return $role;
    }

    /**
     * Display the specified role with all permissions.
     *
     * @param Request $request
     * @param Role $role
     * @return mixed
     */
    public function show(Request $request, Role $role){
        $role->allPermissions = with(new Query\RoleQuery(null, $role))->getRolePermission();
        return $role;
    }

    /**
     * Show the form for editing the specified role with all permissions.
     *
     * @param Request $request
     * @param Role $role
     * @return mixed
     */
    public function edit(Request $request, Role $role){
        $role->allPermissions = with(new Query\RoleQuery(null, $role))->getRolePermission();
        return [
            "data" => $role,
            "permission" => with(new Query\PermissionQuery())->getAllPermission()
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Role $role
     * @return mixed
     */
    public function update(Request $request, Role $role){
        $role->fill($request->only('name'))->save();
        // sync permissions if provided
        if($request->has('permission')){
            $role->syncPermissions($request->only('permission'));
            unset($role->permissions);
        }
        $role->allPermissions = with(new Query\RoleQuery(null, $role))->getRolePermission();
        return $role;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Role $role
     * @return mixed
     * @throws \Exception
     */
    public function destroy(Request $request, Role $role){
        $role->delete();
        return 204;
    }
}
