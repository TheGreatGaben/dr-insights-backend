<?php
namespace App\Http\Controllers\v2;

use Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;
use App\Queries\v2 AS Query;
use App\Http\Requests\UserRequest AS Request;

class UserController extends AppController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request){
        return with(new Query\UserQuery($request))->getAllUser();
    }

    /**
     * Show the form for creating a resource.
     *
     * @return mixed
     */
    public function create(Request $request){
        return [
            "data" => [],
            "role" => Role::addSelect("id", "name")->get(),
            "permission" => with(new Query\PermissionQuery())->getAllPermission()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return mixed
     */
    public function store(Request $request){
        $user = User::create($request->except(["role"]));
        return with(new Query\UserQuery($request, $user))->getAllUser();
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param User $user
     * @return mixed
     */
    public function show(Request $request, User $user){
        return with(new Query\UserQuery($request, $user))->getAllUser();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param User $user
     * @return mixed
     */
    public function edit(Request $request, User $user){
        return [
            "data" => with(new Query\UserQuery($request, $user))->getAllUser(),
            "role" => Role::addSelect("id", "name")->get(),
            "permission" => with(new Query\PermissionQuery())->getAllPermission()
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param User $user
     * @return mixed
     */
    public function update(Request $request, User $user){
        $user->fill($request->except(["role"]))->touch();
        return with(new Query\UserQuery($request, $user))->getAllUser();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param User $user
     * @return mixed
     * @throws \Exception
     */
    public function destroy(Request $request, User $user){
        $user->delete();
        return 204;
    }

    /**
     * Update the roles of multiple users
     *
     * @param Request $request
     * @return mixed
     */
    public function updateUserRoles(Request $request){
        $updatedRoles = $request->input('data');
        foreach($updatedRoles as $updated){
            $user = User::find($updated['id']);
            if(is_array(array_get($updated, 'roles'))){
                $user->syncRoles($updated['roles']);
            }
            if(is_array(array_get($updated, 'permissions'))){
                $user->syncPermissions($updated['permissions']);
            }
        }
        return with(new Query\UserQuery($request))->getAllUser();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getUserDetails(Request $request){
        $user = with(new Query\UserQuery($request, Auth::user()))->getAllUser();
        if(Auth::user()->hasRole("KAMI")){
            $user->all_permissions = Permission::addSelect(["id", "name"])->get()->toArray();
        }
        return $user;
    }

    /**
     * @param Request $request
     * @param $role
     * @return mixed
     */
    public function getUserByRoleName(Request $request, $role){
        return with(new Query\UserQuery($request))->getUserByRoles($role);
    }

    /**
     * @param Request $request
     * @param Role $role
     * @return mixed
     */
    public function getUserByRoleId(Request $request, Role $role){
        return with(new Query\UserQuery($request))->getUserByRoles($role->name);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function impersonateUser(Request $request){
        $user = User::where('email', $request->get('email'))->get()->first();
        $tokenObj= $user->createToken('Temp Token');
        $tokenID = $tokenObj->token->id;
        (new Query\TokenQuery($request))->auditImpersonationSession(Auth::user(), $user, $tokenID);
        return ['access_token' => $tokenObj->accessToken, 'token_id' => $tokenID];
    }

    /**
     * @param Request $request
     */
    public function leaveImpersonatingUser(Request $request){
        (new Query\TokenQuery($request))->deleteTempToken($request->get('token_id'));
    }

    /**
     * @param Request $request
     * @return array|mixed
     */
    public function getProjectManagersSummary(Request $request){
        return (new \App\Queries\v2\UserQuery($request))->getProjectManagerSummary();
    }
}
