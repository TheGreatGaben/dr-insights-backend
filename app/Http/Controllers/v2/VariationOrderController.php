<?php

namespace App\Http\Controllers\v2;

use App\Models\Project;
use App\Queries\v2 AS Query;
use App\Models\VariationOrder;
use App\Http\Requests\VariationOrderRequest AS Request;

class VariationOrderController extends AppController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param Project|null $project
     * @return array
     */
    public function index(Request $request){
        return with(new Query\VariationOrderQuery($request))->getAll();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return array
     */
    public function create(Request $request){
        return [];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\DuplicateDataException
     */
    public function store(Request $request){
        $request->extraRules();
        $variationOrder = VariationOrder::create($request->all());
        return ['variation_order' => $variationOrder];
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param VariationOrder $variationOrder
     * @return array
     */
    public function show(Request $request, VariationOrder $variationOrder){
        return ["variation_order" => $variationOrder];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param VariationOrder $variationOrder
     * @return array
     */
    public function edit(Request $request, VariationOrder $variationOrder){
        return ["variation_order" => $variationOrder];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param VariationOrder $variationOrder
     * @return array
     * @throws \App\Exceptions\DuplicateDataException
     */
    public function update(Request $request, VariationOrder $variationOrder){
        $request->extraRules();
        $variationOrder->fill($request->all())->save();
        return ['variation_order' => $variationOrder];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param VariationOrder $variationOrder
     * @return int
     * @throws \Exception
     */
    public function destroy(Request $request, VariationOrder $variationOrder){
        $variationOrder->delete();
        return 204;
    }
}
