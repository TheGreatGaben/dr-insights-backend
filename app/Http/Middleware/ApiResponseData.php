<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ApiResponseData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        $response = $next($request);

        // debug all query run on current api
        if(config("app.debug") && $request->has("ddQuery"))
            dd(config("all_query"));
        // generate response json data
        if($response instanceof JsonResponse)
            $this->generateJsonResponse($response);
        else if (!$response instanceof BinaryFileResponse)
            $this->convertJsonResponse($response);

        return $response;
    }

    protected function convertJsonResponse(Response &$response){
        $response = response()->json(((ctype_digit($content = $response->content()))?
            ["data" => [], "status_code" => $content]:
            ["data" => $content, "status_code" => 200]), 200);
    }

    /**
     * @param JsonResponse $response
     */
    protected function generateJsonResponse(JsonResponse &$response){
        $responseData = $response->getData();
        if(is_object($responseData)){
            if(property_exists($responseData, "data")){
                $data = $responseData;
            }else{
                $data = new \stdClass();
                $data->data = $responseData;
            }
            if(!empty($currentSorting = config("currentSorting")))
                list($data->current_sorting["sort"], $data->current_sorting["order"]) = $currentSorting;
            $data->status_code = ((property_exists($data, "status_code"))?$data->status_code:200);
        }else{
            $data["data"] = $responseData;
            if(!empty($currentSorting = config("currentSorting")))
                list($data["current_sorting"]["sort"], $data["current_sorting"]["order"]) = $currentSorting;
            $data["status_code"] = ((isset($data["status_code"]) && $data["status_code"])?$data["status_code"]:200);
        }
        $response->setData($data);
    }

    /**
     * @return int
     */
    protected function getResponseStatusCode(){
        switch (request()->getMethod()) {
            case 'POST':
                return 201;
            case 'DELETE':
                return 204;
            default:
                return 200;
        }
    }

}
