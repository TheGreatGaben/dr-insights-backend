<?php
namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class BaseRequest extends FormRequest
{
    /**
     * store all rules mapping
     *
     * @var array
     */
    protected $allRulesMapping = [
        "get" => "view",
        "post" => "create",
        "put" => "update",
        "delete" => "delete",
    ];

    /**
     * store default rules mapping only
     *
     * @var array
     */
    protected $defaultRulesMapping;

    /**
     * new custom rules mapping
     *
     * @var array
     */
    protected $rulesMapping = [];

    /**
     * to store the rules which required to
     * validate data from route
     *
     * @var array
     */
    protected $rulesWithRouteParameters = [];

    /**
     * BaseRequest constructor.
     */
    public function __construct(){
        parent::__construct();

        $this->defaultRulesMapping = $this->allRulesMapping;
        $this->addNewRules();
    }

    /**
     * check user authorization by laravel policy/gate
     * if user not login, will return false as default
     * if no rules mapping set will return true as default
     *
     * @param $class
     * @param null $model
     * @return bool
     */
    public function policy($class, $model = null){
        if(isset($this->allRulesMapping[$policy_method = $this->getCurrentMethod($class)])){
            if($this->user())
                return $this->user()->can($this->allRulesMapping[$policy_method], [$class, $model]);
            return false;
        }
        return true;
    }

    /**
     * Auto call specific function to
     * return correct validation rules
     *
     * @return array
     */
    public function validation(){
        $functionName = "";
        if(isset($this->allRulesMapping[$methodName = $this->getCurrentMethod()]))
            $functionName = $this->allRulesMapping[$methodName] . "Rules";
        return (($functionName && method_exists($this, $functionName))?$this->$functionName():[]);
    }

    /**
     * add custom rules and mapping function
     */
    public function addNewRules(){
        foreach($this->rulesMapping as $rulesMapping => $ruleMethodName){
            $rulesKeys = strtolower(((is_numeric($rulesMapping))?$ruleMethodName:$rulesMapping));
            if(count($rulesArray = explode(";", $ruleMethodName)) > 1 && $rulesArray[1]){
                $ruleMethodName = $rulesArray[0];
                $rulesKeys = strtolower(((is_numeric($rulesMapping))?$ruleMethodName:$rulesMapping));
                $this->rulesWithRouteParameters[$rulesKeys] = strtolower($ruleMethodName);
            }
            $this->allRulesMapping[$rulesKeys] = strtolower($ruleMethodName);
        }
    }

    /**
     * get the current method name
     * or controller action name
     * to do the correct validation
     *
     * @param string|null $class
     * @return string
     */
    public function getCurrentMethod($class = null){
        $controllerAction = $this->getControllerActionName();
        if(
            ($class && !isset(array_flip($this->defaultRulesMapping)[$controllerAction]) && method_exists(policy($class), $controllerAction))
            || (!$class && isset($this->allRulesMapping[$controllerAction]))
        )
            return $controllerAction;
        return strtolower($this->method());
    }

    /**
     * get controller action name
     *
     * @return bool|string
     */
    public function getControllerActionName(){
        return strtolower(substr(\Route::currentRouteAction(), strpos(\Route::currentRouteAction(), "@") + 1));
    }

    /**
     * add route parameters into request object
     * to do the validation
     */
    public function validationData(){
        if(isset($this->rulesWithRouteParameters[$this->getCurrentMethod()]))
            $this->merge($this->route()->parameters());
        return $this->all();
    }

    /**
     * easy set the laravel validation 'in'
     *
     * @param $in
     * @return \Illuminate\Validation\Rules\In
     */
    public function in($in){
        return Rule::in(((is_array($in))?$in:[$in]));
    }

    /**
     * easy set the laravel validation 'unique'
     * allow combination of columns as unique
     * value instead of only 1 column
     *
     * @param $table
     * @param $unique
     * @param string $id
     * @param string $column
     * @return \Illuminate\Validation\Rules\Unique
     */
    public function unique($table, $unique, $id = "", $column = "id"){
        return Rule::unique($table)->where(function ($query) use ($unique, $id, $column){
            if($id && $column)
                $query = $query->where($column, "<>", $id);
            foreach($this->only($unique) as $key => $value)
                $query = $query->where($key, "=", $value);
        });
    }

    /**
     * default pagination rules
     *
     * @param string $prefix
     * @return array
     */
    public function paginate($prefix = ""){
        $prefix = (($prefix)?rtrim($prefix,"_") . "_":"");
        return [
            "{$prefix}page" => "sometimes|integer|min:1",
            "{$prefix}limit" => "sometimes|integer|min:1",
            "{$prefix}sort" => "sometimes|string|nullable",
            "{$prefix}order" => [
                "sometimes",
                "string",
                "nullable",
                $this->in(["asc", "ASC", "desc", "DESC"]),
            ],
        ];
    }
}
