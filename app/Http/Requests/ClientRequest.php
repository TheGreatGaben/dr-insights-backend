<?php
namespace App\Http\Requests;

use App\Models\Client;

class ClientRequest extends BaseRequest
{
    /**
     * ClientRequest constructor.
     */
    public function __construct(){
        parent::__construct();

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return $this->policy(Client::class, $this->route("client"));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return $this->validation();
    }

    /**
     * @return array
     */
    public function viewRules(){
        return array_merge($this->paginate(), $this->paginate("invoice"));
    }

    /**
     * @return array
     */
    public function createRules(){
        return [
            'name' => 'required|string|unique:clients,name',
            'address_1' => 'sometimes|nullable|string',
            'address_2' => 'sometimes|nullable|string',
            'postcode' => 'sometimes|nullable|string',
            'city' => 'sometimes|nullable|string',
            'state' => 'sometimes|nullable|string',
            'country' => 'sometimes|required|string|size:2',
            'tel_no' => 'sometimes|nullable|string',
            'fax_no' => 'sometimes|nullable|string',
            'person_in_charge' => 'sometimes|nullable|string',
            'end_customer' => 'sometimes|nullable|string',
        ];
    }

    /**
     * @return array
     */
    public function updateRules(){
        return [
            'name' => 'sometimes|required|string',
            'address_1' => 'sometimes|nullable|string',
            'address_2' => 'sometimes|nullable|string',
            'postcode' => 'sometimes|nullable|string',
            'city' => 'sometimes|nullable|string',
            'state' => 'sometimes|nullable|string',
            'country' => 'sometimes|required|string|size:2',
            'tel_no' => 'sometimes|nullable|string',
            'fax_no' => 'sometimes|nullable|string',
            'person_in_charge' => 'sometimes|nullable|string',
            'end_customer' => 'sometimes|nullable|string',
        ];
    }

    /**
     * @return array
     */
    public function deleteRules(){
        return [];
    }
}
