<?php
namespace App\Http\Requests;

use App\Models\Contractor;

class ContractorRequest extends BaseRequest
{
    /**
     * ContractorRequest constructor.
     */
    public function __construct(){
        parent::__construct();

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return $this->policy(Contractor::class, $this->route("contractor"));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return $this->validation();
    }

    /**
     * @return array
     */
    public function viewRules(){
        return array_merge($this->paginate(), $this->paginate("expenses"));
    }

    /**
     * @return array
     */
    public function createRules(){
        return [
            'name' => 'required|string|unique:contractors,name',
            'type' => 'sometimes|integer|min:1',
            'address_1' => 'sometimes|nullable|string',
            'address_2' => 'sometimes|nullable|string',
            'postcode' => 'sometimes|nullable|string',
            'city' => 'sometimes|nullable|string',
            'state' => 'sometimes|nullable|string',
            'country' => 'sometimes|required|string|size:2',
            'tel_no' => 'sometimes|nullable|string',
            'fax_no' => 'sometimes|nullable|string',
            'project_id' => 'sometimes|nullable|exists:projects,uuid'
        ];
    }

    /**
     * @return array
     */
    public function updateRules(){
        return [
            'name' => 'sometimes|required|string|unique:contractors,name,'.$this->route('contractor')->id,
            'type' => 'sometimes|integer|min:1',
            'address_1' => 'sometimes|nullable|string',
            'address_2' => 'sometimes|nullable|string',
            'postcode' => 'sometimes|nullable|string',
            'city' => 'sometimes|nullable|string',
            'state' => 'sometimes|nullable|string',
            'country' => 'sometimes|required|string|size:2',
            'tel_no' => 'sometimes|nullable|string',
            'fax_no' => 'sometimes|nullable|string',
            'project_id' => 'sometimes|nullable|exists:projects,uuid'
        ];
    }

    /**
     * @return array
     */
    public function deleteRules(){
        return [];
    }
}
