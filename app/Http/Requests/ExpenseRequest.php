<?php
namespace App\Http\Requests;

use App\Models\Expense;

class ExpenseRequest extends BaseRequest
{
    protected $rulesMapping = [
        "writeOff" => "writeOff",
    ];

    /**
     * ProjectRequest constructor.
     */
    public function __construct(){
        parent::__construct();

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return $this->policy(Expense::class, $this->route("expense"));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return $this->validation();
    }

    /**
     * @return array
     */
    public function viewRules(){
        $rules = $this->paginate();
        $rules['overdue'] = 'sometimes|array';

        return $rules;
    }

    /**
     * @return array
     */
    public function createRules(){
        return [
            'project_id' => 'required|exists:projects,uuid',
            'organisation_id' => 'sometimes|nullable|exists:organisations,uuid',
            'acc_code_id' => 'sometimes|nullable|exists:finance_account_codes,uuid',
            'contractor' => 'required',
            'contractor_type' => 'sometimes|integer|min:1',
            'ref_no' => 'required|string',
            'invoice_no' => 'nullable',
            'description' => 'nullable',
            'invoice_amt' => 'required|numeric',
            'dc_note' => 'required|numeric',
            'credit_note' => 'required|numeric',
            'exclude_tax' => 'boolean',
            'tax_amt' => 'required|numeric',
            'tax_type' => [
                'string',
                $this->in(["gst", "sst"]),
            ],
            'term' => 'required|integer',
            'invoice_date' => 'required|date|date_format:Y-m-d',
            'date_received' => 'required|date|date_format:Y-m-d',
            'due_date' => 'required|date|date_format:Y-m-d',
            'payable_id' => 'sometimes|nullable|integer',
            'payable_type' => 'sometimes|nullable|string'
        ];
    }

    /**
     * @return array
     */
    public function updateRules(){
        return [
            'project_id' => 'required|exists:projects,uuid',
            'organisation_id' => 'sometimes|nullable|exists:organisations,uuid',
            'acc_code_id' => 'sometimes|nullable|exists:finance_account_codes,uuid',
            'contractor' => 'required',
            'contractor_type' => 'sometimes|integer|min:1',
            'ref_no' => 'required|string',
            'invoice_no' => '',
            'description' => '',
            'invoice_amt' => 'required|numeric',
            'dc_note' => 'required|numeric',
            'credit_note' => 'required|numeric',
            'exclude_tax' => 'required|boolean',
            'tax_amt' => 'required|numeric',
            'tax_type' => [
                'string',
                $this->in(["gst", "sst"]),
            ],
            'term' => 'required|integer',
            'invoice_date' => 'required|date|date_format:Y-m-d',
            'date_received' => 'required|date|date_format:Y-m-d',
            'due_date' => 'required|date|date_format:Y-m-d',
            'payable_id' => 'sometimes|nullable|integer',
            'payable_type' => 'sometimes|nullable|string'
        ];
    }

    /**
     * @return array
     */
    public function deleteRules(){
        return [];
    }

    public function writeOffRules() {
        return [
            'write_off_reason' => 'required|string',
        ];
    }
}
