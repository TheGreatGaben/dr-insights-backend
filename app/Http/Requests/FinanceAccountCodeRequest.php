<?php
namespace App\Http\Requests;

use App\Models\FinanceAccountCode;

class FinanceAccountCodeRequest extends BaseRequest
{
    public $rulesMapping = [
        "getChildByCode"
    ];

    /**
     * ClientRequest constructor.
     */
    public function __construct(){
        parent::__construct();

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return $this->policy(FinanceAccountCode::class, $this->route("finance_account_code"));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return $this->validation();
    }

    /**
     * @return array
     */
    public function viewRules(){
        return $this->paginate();
    }

    /**
     * @return array
     */
    public function createRules(){
        return [
            'code' => 'required|string|unique:finance_account_codes,code',
            'name' => 'required|string|unique:finance_account_codes,name',
            'parent_id' => 'sometimes|string|nullable|exists:finance_account_codes,uuid',
        ];
    }

    /**
     * @return array
     */
    public function updateRules(){
        return [
            'code' => 'required|string|unique:finance_account_codes,code,' . $this->route('finance_account_code')->id,
            'name' => 'required|string|unique:finance_account_codes,name,' . $this->route('finance_account_code')->id,
            'parent_id' => 'sometimes|string|nullable|exists:finance_account_codes,uuid',
        ];
    }

    /**
     * @return array
     */
    public function deleteRules(){
        return [];
    }

    /**
     * @return array
     */
    public function getChildByCodeRules(){
        $validation_type = is_array($this->request->get("code")) ? "array" : "string";

        return [
            "code" => "required|$validation_type|exists:finance_account_codes,code"
        ];
    }
}
