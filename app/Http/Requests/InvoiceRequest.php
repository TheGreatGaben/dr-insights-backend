<?php
namespace App\Http\Requests;

use App\Models\Invoice;

class InvoiceRequest extends BaseRequest
{
    protected $rulesMapping = [
        "writeOff" => "writeOff",
    ];

    /**
     * ProjectRequest constructor.
     */
    public function __construct(){
        parent::__construct();
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return $this->policy(Invoice::class, $this->route("invoice"));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return $this->validation();
    }

    /**
     * @return array
     */
    public function viewRules(){
        $rules = $this->paginate();
        $rules['overdue'] = 'sometimes|array';

        return $rules;
    }

    /**
     * @return array
     */
    public function createRules(){
        return [
            'project_id' => 'required|exists:projects,uuid',
            'organisation_id' => 'sometimes|nullable|exists:organisations,uuid',
            'acc_code_id' => 'sometimes|nullable|exists:finance_account_codes,uuid',
            'invoice_no' => 'nullable',
            'invoice_date' => 'required|date|date_format:Y-m-d',
            'cert_no' => [
                'required',
                'string',
                $this->unique("invoices", ["project_id", "cert_no", "invoice_date"])
            ],
            'client_id' => 'sometimes|exists:clients,uuid',
            'description' => 'nullable',
            'term' => 'required|integer',
            'retention' => 'required|numeric',
            'retention_released' => 'required|numeric',
            'work_done_certified' => 'required|required|numeric',
            'prev_certified' => 'required|numeric',
            'nett_certified' => 'required|numeric',
            'debit_note' => 'required|numeric',
            'exclude_tax' => 'boolean',
            'tax_amt' => 'required|numeric',
            'tax_type' => [
                'required',
                'string',
                $this->in(["gst", "sst"]),
            ],
            'vo' => 'required|numeric',
        ];
    }

    /**
     * @return array
     */
    public function updateRules(){
        $invoice = $this->route("invoice");
        return [
            'project_id' => 'required|exists:projects,uuid',
            'organisation_id' => 'sometimes|nullable|exists:organisations,uuid',
            'acc_code_id' => 'sometimes|nullable|exists:finance_account_codes,uuid',
            'invoice_no' => 'nullable',
            'invoice_date' => 'required|date|date_format:Y-m-d',
            'cert_no' => [
                'required',
                'string',
                $this->unique("invoices", ["project_id", "cert_no", "invoice_date"], $invoice->id)
            ],
            'client_id' => 'sometimes|exists:clients,uuid',
            'description' => 'nullable',
            'term' => 'required|integer',
            'retention' => 'required|numeric',
            'retention_released' => 'required|numeric',
            'work_done_certified' => 'required|numeric',
            'prev_certified' => 'required|numeric',
            'nett_certified' => 'required|numeric',
            'debit_note' => 'required|numeric',
            'exclude_tax' => 'required|boolean',
            'tax_amt' => 'required|numeric',
            'tax_type' => [
                'required',
                'string',
                $this->in(["gst", "sst"]),
            ],
            'vo' => 'required|numeric',
            'write_off_reason' => 'sometimes|string',
        ];
    }

    /**
     * @return array
     */
    public function deleteRules(){
        return [];
    }

    public function writeOffRules() {
        return [
            'write_off_reason' => 'required|string',
        ];
    }
}
