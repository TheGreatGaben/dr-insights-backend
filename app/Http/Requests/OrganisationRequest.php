<?php
namespace App\Http\Requests;

use App\Models\Organisation;

class OrganisationRequest extends BaseRequest
{
    /**
     * RoleRequest constructor.
     */
    public function __construct(){
        parent::__construct();

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return $this->policy(Organisation::class, $this->route("organisation"));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return $this->validation();
    }

    /**
     * @return array
     */
    public function viewRules(){
        return $this->paginate();
    }

    /**
     * @return array
     */
    public function createRules(){
        return [
            'name' => 'required|unique:organisations,name',
        ];
    }

    /**
     * @return array
     */
    public function updateRules(){
        return [
            'name' => 'required|unique:organisations,name,' . $this->route('organisation')->id,
        ];
    }

    /**
     * @return array
     */
    public function deleteRules(){
        return [];
    }
}
