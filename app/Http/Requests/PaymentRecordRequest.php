<?php
namespace App\Http\Requests;

use App\Models\PaymentRecord;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PaymentRecordRequest extends BaseRequest
{
    /**
     * ClientRequest constructor.
     */
    public function __construct(){
        parent::__construct();

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        // return $this->policy(PaymentRecord::class, $this->route("paymentRecord"));
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return $this->validation();
    }

    /**
     * @return array
     */
    public function viewRules(){
        return $this->paginate();
    }

    /**
     * @return array
     */
    public function createRules(){
        return [
            'payer_name' => 'required|string',
            'bank' => 'required|string',
            'remark' => 'sometimes|nullable|string',
            'payments' => 'required|array',
            'payments.*.project_id' => 'required|exists:projects,uuid',
            'payments.*.expenses_id' => 'required|exists:expenses,uuid',
            'payments.*.payment_amt' => 'required|gt:0',
        ];
    }

    /**
     * @return array
     */
    public function updateRules(){
        return [
            'bank' => 'sometimes|required|string',
            'remark' => 'sometimes|nullable|string',
        ];
    }

    /**
     * @return array
     */
    public function deleteRules(){
        return [];
    }

    /**
     *
     */
    public function extraRules(){
        foreach($this->get("payments") as $index => $payment){
            $this->projectHasExpenses($payment["expenses_id"], $payment["project_id"]);
        }
    }

    /**
     * to check expenses
     * is belongs to correct project
     *
     * @param $expenses_id
     * @param $project_id
     * @return void
     */
    public function projectHasExpenses($expenses_id, $project_id){
        $query = \DB::table("expenses")
            ->where("expenses.uuid", "=", $expenses_id)
            ->where("expenses.project_id", "=", $project_id)
            ->whereNull("expenses.deleted_at");
        if(!$query->exists()){
            $classes = "[" . implode("] & [", [\App\Models\Project::class, \App\Models\Expense::class]) . "]";
            throw new ModelNotFoundException("No query results for combine of models $classes.");
        }
    }
}
