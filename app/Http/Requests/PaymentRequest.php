<?php
namespace App\Http\Requests;

use App\Models\Payment;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PaymentRequest extends BaseRequest
{
    /**
     * PaymentRequest constructor.
     */
    public function __construct(){
        parent::__construct();

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return $this->policy(Payment::class, $this->route("payment"));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return $this->validation();
    }

    /**
     * @return array
     */
    public function viewRules(){
        return $this->paginate();
    }

    /**
     * @return array
     */
    public function createRules(){
        return array_merge([
            "project_id" => "required|exists:projects,uuid",
            "cash_flow" => ["required", $this->in(["in", "IN", "out", "OUT"])],
            "cheque_no" => "",
            "payment_date" => "required|date|date_format:Y-m-d",
            "payment_amt" => "required|numeric",
        ], $this->getCashflowRule());
    }

    /**
     * @return array
     */
    public function updateRules(){
        return array_merge([
            "project_id" => "required|exists:projects,uuid",
            "cash_flow" => ["required", $this->in(["in", "IN", "out", "OUT"])],
            "cheque_no" => "",
            "payment_date" => "required|date|date_format:Y-m-d",
            "payment_amt" => "required|numeric",
        ], $this->getCashflowRule());
    }

    /**
     * @return array
     */
    public function getCashflowRule(){
        if(strtolower($this->get("cash_flow")) == "in")
            return ["invoice_id" => "required|exists:invoices,uuid"];
        else
            return ["expenses_id" => "required|exists:expenses,uuid"];
    }

    /**
     * @return array
     */
    public function deleteRules(){
        return [];
    }

    /**
     * extra validation if needed
     */
    public function extraRules(){
        $this->projectHas();
    }

    /**
     * to check invoice/expenses/payment
     * is belongs to correct project
     *
     * @return void
     */
    public function projectHas(){
        $function = "has" . (($this->route("payment"))?"Payment":((strtolower($this->get("cash_flow")) == "in")?"Invoice":"Expense"));
        $check = $this->$function();
        if(!$check["query"]->exists()){
            $classes = "[" . implode("] & [", $check["classes"]) . "]";
            throw new ModelNotFoundException("No query results for combine of models $classes.");
        }
    }

    /**
     * @return array
     */
    public function hasInvoice(){
        return [
            "query" => \DB::table("invoices")
                ->where("uuid", "=", $this->get("invoice_id"))
                ->where("project_id", "=", $this->get("project_id")),
            "classes" => [\App\Models\Project::class, \App\Models\Invoice::class],
        ];
    }

    /**
     * @return array
     */
    public function hasExpense(){
        return [
            "query" => \DB::table("expenses")
                ->where("uuid", "=", $this->get("expenses_id"))
                ->where("project_id", "=", $this->get("project_id")),
            "classes" => [\App\Models\Project::class, \App\Models\Expense::class],
        ];
    }

    /**
     * @return array
     */
    public function hasPayment(){
        $column = ((($cash_flow = strtolower($this->get("cash_flow"))) == "in")?"invoice_id":"expenses_id");
        return [
            "query" => \DB::table("payments")
                ->where("uuid", "=", $this->route("payment")->uuid)
                ->where("cash_flow", "=", $this->get("cash_flow"))
                ->where("project_id", "=", $this->get("project_id"))
                ->where($column, "=", $this->get($column)),
            "classes" => [
                \App\Models\Project::class,
                (($cash_flow == "in")?\App\Models\Invoice::class:\App\Models\Expense::class),
                \App\Models\Payment::class,
            ],
        ];
    }
}
