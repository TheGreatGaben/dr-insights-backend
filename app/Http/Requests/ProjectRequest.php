<?php
namespace App\Http\Requests;

use App\Models\Project;

class ProjectRequest extends BaseRequest
{
    public $rulesMapping = [
        "getProjectionCashflow" => "getProjectionCashflow",
    ];

    /**
     * ProjectRequest constructor.
     */
    public function __construct(){
        parent::__construct();
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return $this->policy(Project::class, $this->route("project"));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return $this->validation();
    }

    /**
     * @return array
     */
    public function viewRules(){
        return array_merge([
            "no_of_months" => "sometimes|integer|min:0",
        ], $this->paginate());
    }

    /**
     * @return array
     */
    public function createRules(){
        return [
            'name' => 'required|string',
            'client' => 'required|string',
            'end_customer' => 'nullable|string',
            'schedule' => 'nullable|string',
            'project_managers' => 'required|array|exists:users,id',
            'contract_sum' => 'required|numeric|min:0',
            'budget' => 'required|numeric|min:0',
            'retention' => 'required|numeric|min:0',
            'retention_percent' => 'required|numeric|min:0|max:100',
            'omission_value' => 'required|numeric|min:0',
            'contingency_amt' => 'required|numeric|min:0',
            'variation_order' => 'required|numeric|min:0',
            'status' => [
                'required',
                'string',
                $this->in(["Completed", "New", "On Going"]),
            ],
            'start_date' => 'required|date|date_format:Y-m-d',
            'end_date' => 'required|date|date_format:Y-m-d',
        ];
    }

    /**
     * @return array
     */
    public function updateRules(){
        return [
            'name' => 'required|string',
            'client' => 'required|string',
            'end_customer' => 'nullable|string',
            'schedule' => 'nullable|string',
            'project_managers' => 'required|array|exists:users,id',
            'contract_sum' => 'required|numeric|min:0',
            'budget' => 'required|numeric|min:0',
            'retention' => 'required|numeric|min:0',
            'retention_percent' => 'required|numeric|min:0|max:100',
            'omission_value' => 'required|numeric|min:0',
            'contingency_amt' => 'required|numeric|min:0',
            'variation_order' => 'required|numeric|min:0',
            'status' => [
                'required',
                'string',
                $this->in(["Completed", "New", "On Going"]),
            ],
            'start_date' => 'required|date|date_format:Y-m-d',
            'end_date' => 'required|date|date_format:Y-m-d',
        ];
    }

    /**
     * @return array
     */
    public function deleteRules(){
        return [];
    }

    /**
     * @return array
     */
    public function getProjectionCashflowRules(){
        return ["days" => ["required", "int", $this->in(["30", "60", "90"])]];
    }
}
