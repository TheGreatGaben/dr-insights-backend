<?php
namespace App\Http\Requests;

use App\Models\PurchaseOrder;

class PurchaseOrderRequest extends BaseRequest
{
    /**
     * PurchaseOrderRequest constructor.
     */
    public function __construct(){
        parent::__construct();

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return $this->policy(PurchaseOrder::class, $this->route("purchaseOrder"));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return $this->validation();
    }

    /**
     * @return array
     */
    public function viewRules(){
        return $this->paginate();
    }

    /**
     * @return array
     */
    public function createRules(){
        return [
            'project_id' => 'required|exists:projects,uuid',
            'contractor_id' => 'required|exists:contractors,uuid',
            'issued_date' => 'required|date|date_format:Y-m-d',
            'po_no' => 'required',
            'description' => '',
            'total_value' => 'required|numeric',
            'dc_note' => 'numeric',
            'exclude_tax' => 'boolean',
            'tax_amt' => 'numeric',
            'tax_type' => [
                'string',
                $this->in(["gst", "sst"]),
            ],
            'budget' => 'required|numeric',
        ];
    }

    /**
     * @return array
     */
    public function updateRules(){
        return [
            'project_id' => 'required|exists:projects,uuid',
            'contractor_id' => 'required|exists:contractors,uuid',
            'issued_date' => 'required|date|date_format:Y-m-d',
            'po_no' => 'required',
            'description' => '',
            'total_value' => 'required|numeric',
            'dc_note' => 'numeric',
            'exclude_tax' => 'boolean',
            'tax_amt' => 'numeric',
            'tax_type' => [
                'string',
                $this->in(["gst", "sst"]),
            ],
            'budget' => 'required|numeric',
        ];
    }

    /**
     * @return array
     */
    public function deleteRules(){
        return [];
    }

    /**
     * @throws \App\Exceptions\DuplicateDataException
     */
    public function extraRules(){
        $this->uniquePoNo();
    }

    /**
     * @throws \App\Exceptions\DuplicateDataException
     */
    public function uniquePoNo(){
        if(
            $this->po_no &&
            \DB::table('purchase_orders')
                ->where('project_id', $this->project_id)
                ->where('contractor_id', $this->contractor_id)
                ->where('po_no', $this->po_no)
                ->whereNull('deleted_at')
                ->when($this->route("purchaseOrder"), function($query){
                    return $query->where("uuid", "<>", $this->route("purchaseOrder")->uuid);
                })
                ->exists()
        ){
            throw new \App\Exceptions\DuplicateDataException("Duplicated Data Found", 422);
        }
    }
}
