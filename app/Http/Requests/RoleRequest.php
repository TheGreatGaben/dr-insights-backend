<?php
namespace App\Http\Requests;

use App\Models\Role;

class RoleRequest extends BaseRequest
{
    /**
     * RoleRequest constructor.
     */
    public function __construct(){
        parent::__construct();

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return $this->policy(Role::class, $this->route("role"));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return $this->validation();
    }

    /**
     * @return array
     */
    public function viewRules(){
        return $this->paginate();
    }

    /**
     * @return array
     */
    public function createRules(){
        return [
            'name' => 'required|unique:roles,name',
            'permission' => 'required|array|orExists:permissions,id,name',
        ];
    }

    /**
     * @return array
     */
    public function updateRules(){
        return [
            'name' => 'required|unique:roles,name,' . $this->route('role')->id,
            'permission' => 'sometimes|array|orExists:permissions,id,name',
        ];
    }

    /**
     * @return array
     */
    public function deleteRules(){
        return [];
    }
}
