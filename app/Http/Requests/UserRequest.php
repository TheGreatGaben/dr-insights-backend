<?php
namespace App\Http\Requests;

use App\Models\User;

class UserRequest extends BaseRequest
{
    /**
     * UserRequest constructor.
     */
    public function __construct(){
        parent::__construct();

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return $this->policy(User::class, $this->route("user"));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return $this->validation();
    }

    /**
     * @return array
     */
    public function viewRules(){
        return array_merge([
            "no_of_months" => "sometimes|integer|min:1",
        ], $this->paginate());
    }

    /**
     * @return array
     */
    public function createRules(){
        return [
            'name' => 'required|unique:users,name',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string|confirmed',
            'role' => 'array|orExists:roles,id,name',
        ];
    }

    /**
     * @return array
     */
    public function updateRules(){
        $is_batch_update = str_contains($this->route()->uri, 'batch-update/users/roles');

        if ($is_batch_update) {
            return $this->batchUpdateRules();
        }

        return [
            'name' => 'sometimes|required|unique:users,name,' . $this->route('user')->id,
            'email' => 'sometimes|required|email|unique:users,email,' . $this->route('user')->id,
            'password' => 'sometimes|required|string|confirmed',
            'role' => 'array|orExists:roles,id,name',
        ];
    }

    /**
     * @return array
     */
    public function batchUpdateRules(){
        return [
            'data' => 'required|array',
            'data.*.id' => 'required|exists:users,id',
            'data.*.roles' => 'sometimes|array|exists:roles,name',
            'data.*.permissions' => 'sometimes|array|exists:permissions,name',
        ];
    }

    /**
     * @return array
     */
    public function deleteRules(){
        return [];
    }
}
