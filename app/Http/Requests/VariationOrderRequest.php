<?php
namespace App\Http\Requests;

use App\Models\VariationOrder;

class VariationOrderRequest extends BaseRequest
{
    /**
     * VariationOrderRequest constructor.
     */
    public function __construct(){
        parent::__construct();

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return $this->policy(VariationOrder::class, $this->route("variationOrder"));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return $this->validation();
    }

    /**
     * @return array
     */
    public function viewRules(){
        return $this->paginate();
    }

    /**
     * @return array
     */
    public function createRules(){
        return [
            'project_id' => 'required|exists:projects,uuid',
            'contractor_id' => 'required|exists:contractors,uuid',
            'issued_date' => 'required|date|date_format:Y-m-d',
            'vo_no' => '',
            'description' => '',
            'total_value' => 'required|numeric',
            'dc_note' => 'numeric',
            'exclude_tax' => 'boolean',
            'tax_amt' => 'numeric',
            'tax_type' => [
                'string',
                $this->in(["gst", "sst"]),
            ],
        ];
    }

    /**
     * @return array
     */
    public function updateRules(){
        return [
            'project_id' => 'required|exists:projects,uuid',
            'contractor_id' => 'required|exists:contractors,uuid',
            'issued_date' => 'required|date|date_format:Y-m-d',
            'vo_no' => '',
            'description' => '',
            'total_value' => 'required|numeric',
            'dc_note' => 'numeric',
            'exclude_tax' => 'boolean',
            'tax_amt' => 'numeric',
            'tax_type' => [
                'string',
                $this->in(["gst", "sst"]),
            ],
        ];
    }

    /**
     * @return array
     */
    public function deleteRules(){
        return [];
    }

    /**
     * @throws \App\Exceptions\DuplicateDataException
     */
    public function extraRules(){
        $this->uniqueVoNo();
    }

    /**
     * @throws \App\Exceptions\DuplicateDataException
     */
    public function uniqueVoNo(){
        if(
            $this->vo_no &&
            \DB::table('variation_orders')
                ->where('project_id', $this->project_id)
                ->where('contractor_id', $this->contractor_id)
                ->where('vo_no', $this->vo_no)
                ->whereNull('deleted_at')
                ->when($this->route("variationOrder"), function($query){
                    return $query->where("uuid", "<>", $this->route("variationOrder")->uuid);
                })
                ->exists()
        ){
            throw new \App\Exceptions\DuplicateDataException("Duplicated Data Found", 422);
        }
    }
}
