<?php

namespace App\Listeners;

use Notification;
use App\Notifications\HasDuplicateDataNotify;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class HasDuplicateDataListener
{
    public $emails = [
        "esp@nixel.tech",
        "yihou@nixel.tech",
        "nenglih@nixel.tech",
        "john@nixel.tech",
        "gabriel@nixel.tech",
    ];

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        Notification::route("mail", $this->emails)
            ->notify(new HasDuplicateDataNotify($event->model, $event->duplicateData));
    }
}
