<?php
namespace App\Models;

use Auth;
use App\Traits\HasUuid;
use App\Traits\Searchable;
use App\Traits\Observable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Base extends Model{
    use hasUuid, Searchable, Observable, SoftDeletes;

    /**
     * @return mixed
     */
    public function authUser(){
        return \Auth::user();
    }

    /**
     * Define a one-to-one relationship.
     *
     * @see \Illuminate\Database\Eloquent\Concerns\HasRelationships;
     *
     * @param  string  $related
     * @param  string  $foreignKey
     * @param  string  $localKey
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function customHasOne($related, $foreignKey = null, $localKey = null)
    {
        $instance = $this->newRelatedInstance($related);

        $foreignKey = $foreignKey ?: $this->getForeignKey();

        $localKey = $localKey ?: $this->getKeyName();

        return $this->newHasOne($instance->newQuery(), $this, $foreignKey, $localKey);
    }
}
