<?php

namespace App\Models;

use App\Traits\uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class BulkPayer extends Model implements Auditable
{
    use SoftDeletes;
    use Uuids;
    use \OwenIt\Auditing\Auditable;

    protected $guarded = [
        'id', 'uuid', 'created_at', 'updated_at', 'deleted_at'
    ];

    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];

    public function paymentRecords()
    {
        return $this->hasMany(PaymentRecord::class, 'payer_id');
    }
}
