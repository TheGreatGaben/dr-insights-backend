<?php

namespace App\Models;

use OwenIt\Auditing\Contracts\Auditable;

class Client extends Base implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $guarded = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];

    protected $filters = [
        "clients" => [
            "filter" => ["clients.name", "clients.person_in_charge", "clients.end_customer"]
        ],
        "clients_invoices" => [
            "filter" => ["clients.end_customer", "projects.name", "invoices.cert_no", "invoices.invoice_no"]
        ],
    ];

    /**
     *******************************************
     * Set Relationship
     *******************************************
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projects(){
        return $this->hasMany(Project::class, "client_id", "uuid");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invoices(){
        return $this->hasMany(Invoice::class, "client_id", "uuid");
    }

    /**
     *******************************************
     * Set Scopes
     *******************************************
     */

    /**
     * @param $query
     * @return mixed
     */
    public function scopeLeftJoinProject($query){
        return $query->leftJoin("projects", "projects.client_id", "=", "clients.uuid")
            ->whereNull("projects.deleted_at")
        ;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeLeftJoinInvoiceSum($query){
        return $query->leftJoinSub(
                Invoice::joinPaymentSum("left")
                    ->addSelect([
                        "invoices.project_id",
                        "invoices.client_id",
                        \DB::raw("SUM(`invoices`.`nett_certified` + `invoices`.`gst` + `invoices`.`sst` - `invoices`.`debit_note`) AS `total_invoiced`"),
                        \DB::raw("SUM(`invoices`.`payment_amt`) AS `total_received`"),
                        \DB::raw("SUM(`invoices`.`balance`) AS `total_receivable`"),
                        \DB::raw("AVG(CASE WHEN `invoices`.`balance` > 0 THEN DATEDIFF(NOW(), `invoices`.`invoice_date`) ELSE DATEDIFF(`payments`.`last_payment_date`, `invoices`.`invoice_date`) END) AS `aging`"),
                    ])
                    ->groupBy(["invoices.project_id", "invoices.client_id"])
            , "invoices", function($query){
                return $query->on("invoices.project_id", "=", "projects.uuid")
                    ->on("invoices.client_id", "=", "clients.uuid");
            })
        ;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeJoinInvoice($query){
        return $query->joinSub(
            Invoice::joinPaymentSum("left")
                ->addSelect([
                    "invoices.id",
                    "invoices.project_id",
                    "invoices.cert_no",
                    "invoices.invoice_no",
                    \DB::raw("(`invoices`.`nett_certified` + `invoices`.`gst` + `invoices`.`sst` - `invoices`.`debit_note`) AS `invoiced_amt`"),
                    "invoices.payment_amt",
                    "invoices.balance",
                    "invoices.invoice_date",
                    "invoices.due_date",
                    "payments.last_payment_date",
                    \DB::raw("(CASE WHEN `invoices`.`balance` > 0 THEN DATEDIFF(NOW(), `invoices`.`invoice_date`) ELSE DATEDIFF(`payments`.`last_payment_date`, `invoices`.`invoice_date`) END) AS `aging`")
                ])
                ->groupBy(["invoices.id", "payments.last_payment_date"])
            , "invoices", function($query){
                return $query->on("invoices.project_id", "=", "projects.uuid");
            })
        ;
    }
}
