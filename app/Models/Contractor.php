<?php

namespace App\Models;

use OwenIt\Auditing\Contracts\Auditable;

class Contractor extends Base implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $guarded = [
        'id', 'uuid', 'created_at', 'updated_at', 'deleted_at'
    ];

    public static $observer = \App\Observers\ContractorObserver::class;

    protected $appends = ["type_string"];

    protected $hidden = ["pivot"];

    protected $filters = [
        "contractors" => [
            "filter" => ["contractors.name"]
        ],
    ];

    protected $codeToType = [
        "1" => "Supplier",
        "2" => "Subcon",
    ];

    public function typeToCode($type = "") {
        return array_flip($this->codeToType)[$type];
    }

    /**
     *******************************************
     * Set Relationship
     *******************************************
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projects(){
        return $this->belongsToMany(Project::class)->using('App\Pivots\ProjectContractors');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function expenses(){
        return $this->hasMany(Expense::class, 'contractor_id', 'uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pos(){
        return $this->hasMany(PurchaseOrder::class, 'contractor_id', 'uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vos(){
        return $this->hasMany(VariationOrder::class, 'contractor_id', 'uuid');
    }

    /**
     *******************************************
     * Set Scopes
     *******************************************
     */

    /**
     * @param $query
     * @return mixed
     */
    public function scopeJoinProject($query, Project $project = null){
        return $query->join("contractor_project", "contractor_project.contractor_id", "=", "contractors.id")
            ->whereNull("contractor_project.deleted_at")
            ->join("projects", "contractor_project.project_id", "=", "projects.id")
            ->whereNull("projects.deleted_at")
            ->when($project, function($query) use ($project){
                $query->where("projects.id", "=", $project->id);
            })
        ;
    }

    /**
     * return $mixed
     */
    public function scopeLeftJoinProjectTotal($query){
        return $query->leftJoinSub(
            Project::leftJoin("contractor_project", "contractor_project.project_id", "=", "projects.id")
                ->whereNull("contractor_project.deleted_at")
                ->addSelect([
                    "contractor_project.contractor_id",
                    \DB::raw("COUNT(*) AS total_projects"),
                ])
                ->groupBy(["contractor_project.contractor_id"])
            , "projects", function($query){
                return $query->on("projects.contractor_id", "=", "contractors.id");
            })
            ->groupBy(["projects.total_projects"])
        ;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeLeftJoinExpensesSum($query, Project $project = null){
        return $query->leftJoinSub(
                \DB::query()->fromSub(
                    Expense::search($this->searchRequest, "contractor")
                        ->joinProject($project)
                        ->leftjoin("payments", function($query){
                            return $query->on("payments.expenses_id", "=", "expenses.uuid")
                                ->where("payments.cash_flow", "=", "OUT")
                                ->whereNull("payments.deleted_at")
                            ;
                        })
                        ->addSelect([
                            "expenses.contractor_id",
                            \DB::raw("COALESCE(`expenses`.`invoice_amt`, 0) + COALESCE(`expenses`.`gst`, 0) + COALESCE(`expenses`.`sst`, 0) AS `total_expenses`"),
                            \DB::raw("COALESCE(`expenses`.`payment_amt`, 0) AS `payment_amt`"),
                            \DB::raw("COALESCE(`expenses`.`balance`, 0) AS `balance`"),
                            \DB::raw("COALESCE(CASE WHEN `expenses`.`balance` > 0 THEN DATEDIFF(NOW(), `expenses`.`date_received`) ELSE DATEDIFF(MAX(`payments`.`payment_date`), `expenses`.`date_received`) END, 0) AS `aging`"),
                        ])
                        ->groupBy(["expenses.id"])
                , "expenses")->addSelect([
                    "expenses.contractor_id",
                    \DB::raw("SUM(`expenses`.`total_expenses`) AS `total_expenses`"),
                    \DB::raw("SUM(`expenses`.`payment_amt`) AS `payment_amt`"),
                    \DB::raw("SUM(`expenses`.`balance`) AS `balance`"),
                    \DB::raw("AVG(`expenses`.`aging`) AS `aging`"),
                ])
                ->groupBy("expenses.contractor_id")
            , "expenses", function($query){
                return $query->on("expenses.contractor_id", "=", "contractors.uuid");
            })
        ;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeLeftJoinPurchaseOrderSum($query, Project $project = null){
        return $query->leftJoinSub(
                PurchaseOrder::search($this->searchRequest, "contractor")
                    ->joinProject($project)
                    ->addSelect([
                        "purchase_orders.contractor_id",
                        \DB::raw("SUM(`purchase_orders`.`total_value`) AS total_value"),
                    ])
                    ->groupBy(["purchase_orders.contractor_id"])
            , "purchase_orders", function($query){
                return $query->on("purchase_orders.contractor_id", "=", "contractors.uuid");
            })
        ;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeLeftJoinVariationOrderSum($query, Project $project = null){
        return $query->leftJoinSub(
                VariationOrder::search($this->searchRequest, "contractor")
                    ->joinProject($project)
                    ->addSelect([
                        "variation_orders.contractor_id",
                        \DB::raw("SUM(`variation_orders`.`total_value`) AS total_value"),
                    ])
                    ->groupBy(["variation_orders.contractor_id"])
            , "variation_orders", function($query){
                return $query->on("variation_orders.contractor_id", "=", "contractors.uuid");
            })
        ;
    }

    /**
     *******************************************
     * Set Model Attributes
     *******************************************
     */

    /**
     * @return string
     */
    public function getTypeStringAttribute() {
        if (isset($this->codeToType[$this->type])) {
            return $this->codeToType[$this->type];
        }
    }
}
