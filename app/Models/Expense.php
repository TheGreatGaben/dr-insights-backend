<?php

namespace App\Models;

use App\Traits\HasDuplicateData;
use OwenIt\Auditing\Contracts\Auditable;

class Expense extends Base implements Auditable
{
    use hasDuplicateData, \OwenIt\Auditing\Auditable;

 	protected $fillable = [
        'project_id', 'organisation_id', 'acc_code_id', 'invoice_date', 'date_received', 'contractor', 'contractor_type',
        'contractor_id', 'ref_no', 'payable_id', 'payable_type',
        'term', 'due_date', 'description', 'invoice_amt', 'gst', 'sst', 'exclude_tax',
        'dc_note', 'credit_note', 'remark', 'invoice_id', 'payment_amt', 'balance',
        'write_off_date', 'write_off_reason', 'write_off_user'
    ];

    protected $hidden = ["payments"];

    protected $appends = ["contractor_type_string", "total_amount", "total_payment"];

    protected $dates = ['invoice_date', 'date_received', 'due_date', 'write_off_date'];

    protected static $observer = \App\Observers\ExpenseObserver::class;

    /**
     * if combination of data from these column got duplicate
     * will send email to notify
     *
     * @var array
     */
    public static $duplicateData = ["project_id", "ref_no"];
    public static $wontNotifyIfAnyDataEmpty = true;

    protected $filters = [
        "expenses" => [
            "filter" => ["projects.name", "contractors.name", "expenses.ref_no"],
            "date" => [
                "expenses.invoice_date", "expenses.due_date", "expenses.date_received", "payments.payment_date",
                "expenses.write_off_date",
                "due_date" => ["expenses"],
                "invoice_date" => ["expenses"],
                "date_received" => ["expenses"],
                "payment_date" => ["payments"],
                "write_off_date" => ["expenses"],
            ],
            "string" => [
                "project_id" => ["expenses"],
                "contractor_id" => ["expenses"],
            ],
            "in" => [
                "organisation_id" => ["expenses"],
            ],
        ],
        "payments" => [
            "date" => [
                "expenses.invoice_date", "expenses.due_date", "expenses.date_received",
                "due_date" => ["expenses"],
                "invoice_date" => ["expenses"],
                "date_received" => ["expenses"],
            ]
        ],
    ];

    protected $codeToType = [
        "1" => "Supplier",
        "2" => "Subcon",
    ];

    public function typeToCode($type = "") {
        return array_flip($this->codeToType)[$type];
    }

    /**
     *******************************************
     * Set Relationship
     *******************************************
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project(){
        return $this->belongsTo(Project::class, 'project_id', 'uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organisation(){
        return $this->belongsTo(Organisation::class,'organisation_id', "uuid");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contractor(){
        return $this->belongsTo(Contractor::class, 'contractor_id', 'uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments(){
        return $this->hasMany(Payment::class, 'expenses_id', 'uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function accCode(){
        return $this->hasOne(FinanceAccountCode::class, 'uuid', 'acc_code_id');
    }

    /**
     *******************************************
     * Set Scopes
     *******************************************
     */

    /**
     * @param $query
     * @return mixed
     */
    public function scopePending($query, $writeOff = false){
        // Because we set written off expenses balance to 0 and move it to write_off_amt
        if ($writeOff) {
            return $query->whereNotNull('write_off_date');
        } else {
            return $query->where('balance', '>', '0');
        }
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeWriteOff($query, $writeOff = false){
        if ($writeOff) {
            return $query->whereNotNull('write_off_date');
        } else {
            return $query->whereNull('write_off_date');
        }
    }

    /**
     * @param $query
     * @param Project|null $project
     * @return mixed
     */
    public function scopeJoinProject($query, Project $project = null){
        return $query->join("projects", "projects.uuid", "=", "expenses.project_id")
            ->whereNull("projects.deleted_at")
            ->when($project, function($query) use ($project){
                return $query->where("projects.uuid", "LIKE", $project->uuid);
            })
        ;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeJoinOrganisation($query){
        return $query->join("organisations", "organisations.uuid", "=", "expenses.organisation_id")
            ->whereNull("organisations.deleted_at")
        ;
    }

    /**
     * to return user's projects that assigned to them
     *
     * @param $query
     * @param $projectManagers
     * @return mixed
     */
    public function scopeProjectManagers($query, $projectManagers = []){
        $byProjectManager = (!empty($projectManagers));
        $projectManagers = ((is_array($projectManagers))?$projectManagers:[$projectManagers]);
        $byAuthUser = ($this->authUser() && !$this->authUser()->hasAnyRole(["KAMI", "Admin", "Business Owner"]));
        return $query->when($byProjectManager || $byAuthUser, function ($query) use ($byProjectManager, $projectManagers){
            return $query->join("permissions AS p", \DB::raw("CONCAT('project.view.', `projects`.`id`)"), "=", "p.name")
                ->join("model_has_permissions AS mp", function($query) use ($byProjectManager, $projectManagers) {
                    return $query->on("mp.permission_id", "=", "p.id")
                        ->where("mp.model_type", "=", "App\Models\User")
                        ->whereIn("mp.model_id", (($byProjectManager)?$projectManagers:[$this->authUser()->id]))
                    ;
                })
            ;
        });
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeJoinContractor($query){
        return $query->join("contractors", "contractors.uuid", "=", "expenses.contractor_id")
            ->whereNull("contractors.deleted_at");
    }

    /**
     * @param $query
     * @param string $joinType
     * @return mixed
     */
    public function scopeJoinPurchaseOrder($query, $joinType = "") {
        $joinType .= "Join";
        return $query->$joinType("purchase_orders", function ($query) {
            return $query->on("purchase_orders.id", "=", "expenses.payable_id")
                ->where(function ($query) {
                    return $query->where("expenses.payable_type", "App\Models\PurchaseOrder")
                        ->orWhere("expenses.payable_type", "App\PurchaseOrder")
                    ;
                })
                ->whereNull("purchase_orders.deleted_at")
            ;
        });
    }

    /**
     * @param $query
     * @param string $joinType
     * @return mixed
     */
    public function scopeJoinVariationOrder($query, $joinType = "") {
        $joinType .= "Join";
        return $query->$joinType("variation_orders", function ($query) {
            return $query->on("variation_orders.id", "=", "expenses.payable_id")
                ->where(function ($query) {
                    return $query->where("expenses.payable_type", "App\Models\VariationOrder")
                        ->orWhere("expenses.payable_type", "App\VariationOrder")
                    ;
                })
                ->whereNull("variation_orders.deleted_at")
            ;
        });
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeLeftJoinPayment($query){
        return $query->leftJoin("payments", function($query){
            return $query->on("payments.expenses_id", "=", "expenses.uuid")
                ->whereNull("payments.deleted_at")
                ->where("payments.cash_flow", "=", "OUT")
            ;
        });
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeLeftJoinAccountCode($query){
        return $query->leftJoin("finance_account_codes", "finance_account_codes.uuid", "=", "expenses.acc_code_id");
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeOverdue($query){
        return $query->whereRaw("CURRENT_DATE() >= `expenses`.`due_date`")
            ->where("expenses.balance", ">", "0")
            ;
    }

    /**
     *******************************************
     * Set Model Attributes
     *******************************************
     */

    /**
     * @return float
     */
    public function getTotalPaymentAttribute(){
        return $this->payment_amt;
    }

    public function getAccurateTotalPaymentAttribute() {
        return round($this->payments()->sum('payment_amt'), 2);
    }

    /**
     * @return float
     */
    public function getTotalAmountAttribute(){
        return round($this->invoice_amt + $this->gst + $this->sst + $this->dc_note + $this->credit_note, 2);
    }

    /**
     * @return float
     */
    public function getAccurateBalanceAttribute(){
        return round($this->total_amount - $this->accurate_total_payment, 2);
    }

    /**
     * @return string
     */
    public function getContractorTypeStringAttribute() {
        if (isset($this->codeToType[$this->contractor_type])) {
            return $this->codeToType[$this->contractor_type];
        }
    }

    /**
     *******************************************
     * Other Settings
     *******************************************
     */

}
