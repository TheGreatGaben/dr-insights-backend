<?php

namespace App\Models;

use Kalnoy\Nestedset\NodeTrait;
use OwenIt\Auditing\Contracts\Auditable;

class FinanceAccountCode extends Base implements Auditable
{
    use NodeTrait, \OwenIt\Auditing\Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid', 'code', 'name', 'parent_id'
    ];

    protected $guarded = [
        'id', 'created_at', 'updated_at', 'deleted_at',
    ];

    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at',
    ];

    /**
     *******************************************
     * Set Relationship
     *******************************************
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invoices(){
        return $this->hasMany(Invoice::class, 'acc_code_id', 'uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function expenses(){
        return $this->hasMany(Expense::class, 'acc_code_id', 'uuid');
    }

    /**
     *******************************************
     * Mutator and Accessor
     *******************************************
     */

    /**
     * overwrite method in trait class to
     * get model by uuid instead of id
     *
     * @param $value
     */
    public function setParentIdAttribute($value)
    {
        if ($this->getParentId() == $value) return;
        if($value && ($parent = FinanceAccountCode::findByUuid($value)))
            $this->appendToNode($parent);
        else
            $this->makeRoot();
    }
}
