<?php

namespace App\Models;

use App\Traits\HasDuplicateData;
use OwenIt\Auditing\Contracts\Auditable;

class Invoice extends Base implements Auditable
{
    use hasDuplicateData, \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'project_id', 'organisation_id', 'acc_code_id', 'invoice_no', 'invoice_date', 'term', 'due_date', 'cert_no', 'description',
        'work_done_certified', 'vo', 'retention_released', 'retention', 'prev_certified', 'nett_certified',
        'gst', 'sst', 'exclude_tax', 'debit_note', 'payment_amt', 'balance',
        'client_id', 'write_off_date', 'write_off_reason', 'write_off_user'
    ];

    protected $hidden = ["payments"];

    protected $appends = ['total_amount', 'total_payment'];

    protected $dates = ['invoice_date', 'due_date', 'write_off_date'];

    public static $observer = \App\Observers\InvoiceObserver::class;

    /**
     * if combination of data from these column got duplicate
     * will send email to notify
     *
     * @var array
     */
    public static $duplicateData = ["project_id", "invoice_no"];
    public static $wontNotifyIfAnyDataEmpty = true;

    protected $filters = [
        "invoices" => [
            "filter" => ["projects.name", "clients.name", "invoices.cert_no", "invoices.invoice_no"],
            "date" => [
                "invoices.invoice_date", "invoices.due_date", "payments.payment_date",
                "invoices.write_off_date",
                "due_date" => ["invoices"],
                "invoice_date" => ["invoices"],
                "payment_date" => ["payments"],
                "write_off_date" => ["invoices"],
            ],
            "in" => ["organisation_id" => ["invoices"]],
        ],
        "payments" => [
            "date" => [
                "invoices.invoice_date", "invoices.due_date",
                "due_date" => ["invoices"],
                "invoice_date" => ["invoices"],
            ]
        ],
    ];

    /**
     *******************************************
     * Set Relationship
     *******************************************
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project(){
        return $this->belongsTo(Project::class,'project_id', "uuid");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organisation(){
        return $this->belongsTo(Organisation::class,'organisation_id', "uuid");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client(){
        return $this->belongsTo(Client::class, 'client_id', "uuid");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments(){
        return $this->hasMany(Payment::class, 'invoice_id', 'uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function accCode(){
        return $this->hasOne(FinanceAccountCode::class, 'uuid', 'acc_code_id');
    }

    /**
     *******************************************
     * Set Scopes
     *******************************************
     */

    /**
     * @param $query
     * @return mixed
     */
    public function scopePending($query, $writeOff = false){
        // Because we set written off invoices balance to 0 and move it to write_off_amt
        if ($writeOff) {
            return $query->whereNotNull('write_off_date');
        } else {
            return $query->where('balance', '>', '0');
        }
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeWriteOff($query, $writeOff = false){
        if ($writeOff) {
            return $query->whereNotNull('write_off_date');
        } else {
            return $query->whereNull('write_off_date');
        }
    }

    /**
     * @param $query
     * @param Project|null $project
     * @return mixed
     */
    public function scopeJoinProject($query, Project $project = null){
        return $query->join("projects", "projects.uuid", "=", "invoices.project_id")
            ->whereNull("projects.deleted_at")
            ->when($project, function($query) use ($project) {
                return $query->where("projects.uuid", "LIKE", $project->uuid);
            })
        ;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeJoinOrganisation($query){
        return $query->join("organisations", "organisations.uuid", "=", "invoices.organisation_id")
            ->whereNull("organisations.deleted_at")
        ;
    }

    /**
     * to return user's projects that assigned to them
     *
     * @param $query
     * @param $projectManagers
     * @return mixed
     */
    public function scopeProjectManagers($query, $projectManagers = []){
        $byProjectManager = (!empty($projectManagers));
        $projectManagers = ((is_array($projectManagers))?$projectManagers:[$projectManagers]);
        $byAuthUser = ($this->authUser() && !$this->authUser()->hasAnyRole(["KAMI", "Admin", "Business Owner"]));
        return $query->when($byProjectManager || $byAuthUser, function ($query) use ($byProjectManager, $projectManagers){
            return $query->join("permissions AS p", \DB::raw("CONCAT('project.view.', `projects`.`id`)"), "=", "p.name")
                ->join("model_has_permissions AS mp", function($query) use ($byProjectManager, $projectManagers) {
                    return $query->on("mp.permission_id", "=", "p.id")
                        ->where("mp.model_type", "=", "App\Models\User")
                        ->whereIn("mp.model_id", (($byProjectManager)?$projectManagers:[$this->authUser()->id]))
                    ;
                })
            ;
        });
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeJoinClient($query){
        return $query->join("clients", "clients.uuid", "=", "invoices.client_id")
            ->whereNull("clients.deleted_at")
        ;
    }

    /**
     * @param $query
     * @param string $joinType
     * @param Project|null $project
     * @return mixed
     */
    public function scopeLeftJoinPayment($query){
        return $query->leftJoin("payments", function($query){
                return $query->on("payments.invoice_id", "=", "invoices.uuid")
                    ->where("payments.cash_flow", "=", "IN")
                    ->whereNull("payments.deleted_at")
                ;
            })
        ;
    }

    /**
     * @param $query
     * @param string $joinType
     * @param Project|null $project
     * @return mixed
     */
    public function scopeJoinPaymentSum($query, $joinType = "", Project $project = null){
        $joinType .= "JoinSub";
        return $query->$joinType($this->paymentSum($project), "payments", function($join){
            return $join->on("payments.invoice_id", "=", "invoices.uuid");
        });
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeLeftJoinAccountCode($query){
        return $query->leftJoin("finance_account_codes", "finance_account_codes.uuid", "=", "invoices.acc_code_id");
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeOverdue($query){
        return $query->whereRaw("CURRENT_DATE() >= `invoices`.`due_date`")
            ->where("invoices.balance", ">", "0")
        ;
    }

    /**
     *******************************************
     * Set Model Attributes
     *******************************************
     */

    /**
     * @return float
     */
    public function getTotalPaymentAttribute(){
        return round($this->payments->sum('payment_amt'), 2);
    }

    public function getAccurateTotalPaymentAttribute() {
        return round($this->payments()->sum('payment_amt'), 2);
    }

    /**
     * @return float
     */
    public function getTotalAmountAttribute(){
        return round($this->nett_certified + $this->gst + $this->sst, 2);
    }

    /**
     * @return float
     */
    public function getAccurateBalanceAttribute(){
        return round($this->total_amount - $this->accurate_total_payment, 2);
    }

    /**
     * @param Project|null $project
     * @return mixed
     */
    public function paymentSum(Project $project = null){
        return Payment::search($this->searchRequest, "invoices_expenses")
            ->where("payments.cash_flow", "=", "IN")
            ->groupBy("payments.invoice_id")
            ->addSelect([
                "payments.invoice_id",
                \DB::raw("MAX(`payments`.`payment_date`) AS `last_payment_date`"),
                \DB::raw("COALESCE(SUM(`payments`.`payment_amt`), 0) AS `payment_amt`"),
                \DB::raw("GROUP_CONCAT(CONCAT(`payments`.`uuid`, ';|', COALESCE(`payments`.`cheque_no`, ''), ';|', `payments`.`payment_date`, ';|', `payments`.`payment_amt`)) as `payment_details`")
            ])
        ;
    }
}
