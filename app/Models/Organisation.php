<?php

namespace App\Models;

use OwenIt\Auditing\Contracts\Auditable;

class Organisation extends Base implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'name',
    ];

    protected $filters = [
        "users" => [
            "filter" => ["name"],
        ],
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invoices(){
        return $this->hasMany(Invoice::class, 'organisation_id', 'uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function expenses(){
        return $this->hasMany(Expense::class, 'organisation_id', 'uuid');
    }
}
