<?php

namespace App\Models;

use OwenIt\Auditing\Contracts\Auditable;

class Payment extends Base implements Auditable
{
    use \OwenIt\Auditing\Auditable;

	protected $fillable = [
        'uuid', 'project_id', 'invoice_id', 'expenses_id', 'cash_flow', 'payment_date', 'cheque_no', 'payment_amt',
        'batch_no', 'batch_remark'
    ];

    protected $hidden = [];

    protected $appends = [];

    protected $dates = ['payment_date'];

    public static $observer = \App\Observers\PaymentObserver::class;

    protected $filters = [
        "payments" => [
            "date_filter" => ["invoices.due_date", "expenses.due_date"],
            "filter" => ["projects.name", "clients.name", "contractors.name", "invoices.cert_no", "expenses.ref_no"],
            "date" => [
                "payment_date", "payments.payment_date", "invoices.due_date", "expenses.due_date",
                "due_date" => [["invoices", "expenses"]],
            ],
        ],
        "invoices_expenses" => [
            "date" => [
                "payments.payment_date",
                "payment_date" => ["payments"],
            ],
        ]
    ];

    protected $joinKeys = [
        "in" => ["invoices", "payments.invoice_id"],
        "out" => ["expenses", "payments.expenses_id"],
    ];

    /**
     *******************************************
     * Set Relationship
     *******************************************
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project(){
        return $this->belongsTo(Project::class,'project_id', 'uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function invoice(){
        return $this->belongsTo(Invoice::class,'invoice_id', 'uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function expenses(){
        return $this->belongsTo(Expense::class,'expenses_id', 'uuid');
    }

    /**
     *******************************************
     * Set Scopes
     *******************************************
     */

    /**
     * @param $query
     * @param $cashflow
     * @param bool $group
     * @return mixed
     */
    public function scopeCashflow($query, $cashflow, $group = false){
        $columns = [];
        $cashflow = ((is_array($cashflow))?$cashflow:[$cashflow]);
        foreach($cashflow as $type){
            list($table, $column) = $this->joinKeys[strtolower($type)];
            $columns[] = $column;
            $query->leftJoin($table, $column, "=", $table . ".uuid")
                ->whereNull($table . ".deleted_at");
        }
        return $query->whereIn("payments.cash_flow", $cashflow)
            ->when($group && !empty($columns), function($query) use ($columns){
                return $query->groupBy($columns)
                    ->addSelect($columns);
            })
        ;
    }

    public function scopeCountPayment($query, Project $project = null){
        return $query->leftJoinSub(
                Payment::search($this->searchRequest)
                    ->where("payments.payment_amt", ">", "0")
                    ->joinProject($project)
                    ->projectManagers($this->searchRequest->get("project_manager"))
                    ->leftJoinInvoice()
                    ->leftJoinExpense()
                    ->select([
                        "payments.cash_flow",
                        "payments.project_id",
                        "payments.invoice_id",
                        "payments.expenses_id",
                        \DB::raw("COALESCE(COUNT(*), 0) AS total"),
                    ])
                    ->groupBy(["payments.cash_flow", "payments.project_id", "payments.invoice_id", "payments.expenses_id"])
            , "count_payments", function($query){
                return $query->on('count_payments.cash_flow', '=', 'payments.cash_flow')
                    ->on('count_payments.project_id', '=', 'payments.project_id')
                    ->on(function($join){
                        $join->orOn('count_payments.invoice_id', '=', 'payments.invoice_id')
                            ->orOn('count_payments.expenses_id', '=', 'payments.expenses_id');
                    })
                ;
            })
        ;
    }

    /**
     * @param $query
     * @param Project|null $project
     * @return mixed
     */
    public function scopeJoinProject($query, Project $project = null){
        return $query->join("projects", "projects.uuid", "=", "payments.project_id")
            ->whereNull("projects.deleted_at")
            ->when($project, function($query) use ($project) {
                return $query->where("projects.uuid", "LIKE", $project->uuid);
            })
        ;
    }

    /**
     * to return user's projects that assigned to them
     *
     * @param $query
     * @param $projectManagers
     * @return mixed
     */
    public function scopeProjectManagers($query, $projectManagers = []){
        $byProjectManager = (!empty($projectManagers));
        $projectManagers = ((is_array($projectManagers))?$projectManagers:[$projectManagers]);
        $byAuthUser = ($this->authUser() && !$this->authUser()->hasAnyRole(["KAMI", "Admin", "Business Owner"]));
        return $query->when($byProjectManager || $byAuthUser, function ($query) use ($byProjectManager, $projectManagers){
            return $query->join("permissions AS p", \DB::raw("CONCAT('project.view.', `projects`.`id`)"), "=", "p.name")
                ->join("model_has_permissions AS mp", function($query) use ($byProjectManager, $projectManagers) {
                    return $query->on("mp.permission_id", "=", "p.id")
                        ->where("mp.model_type", "=", "App\Models\User")
                        ->whereIn("mp.model_id", (($byProjectManager)?$projectManagers:[$this->authUser()->id]))
                    ;
                })
                ;
            })
        ;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeLeftJoinInvoice($query){
        return $query->leftJoin("invoices", "invoices.uuid", "=", "payments.invoice_id")
            ->whereNull("invoices.deleted_at")
            ->leftjoin("clients", "invoices.client_id", "=", "clients.uuid")
            ->whereNull("clients.deleted_at")
        ;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeLeftJoinExpense($query){
        return $query->leftJoin("expenses", "expenses.uuid", "=", "payments.expenses_id")
            ->whereNull("expenses.deleted_at")
            ->leftjoin("contractors", "expenses.contractor_id", "=", "contractors.uuid")
            ->whereNull("contractors.deleted_at")
        ;
    }

    /**
     *******************************************
     * Other Settings
     *******************************************
     */

}
