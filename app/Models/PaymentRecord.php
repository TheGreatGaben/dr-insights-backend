<?php

namespace App\Models;

use OwenIt\Auditing\Contracts\Auditable;

class PaymentRecord extends Base implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $guarded = [
        'id', 'uuid', 'payer_id',
        'created_at', 'updated_at', 'deleted_at',
        'total_paid'
    ];

    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];

    protected $appends = ['payment_count'];

    protected $dates = ["payment_date"];

    protected static $observer = \App\Observers\PaymentRecordsObserver::class;

    protected $filters = [
        'payment_records' => [
            'date' => [
                'payment_records.payment_date',
                'payment_date' => ['payment_records'],
            ]
        ],
    ];

    /**
     *******************************************
     * Set Relationship
     *******************************************
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function payer(){
        return $this->belongsTo(BulkPayer::class, 'payer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments(){
        return $this->hasMany(Payment::class, 'batch_no');
    }

    /**
     *******************************************
     * Set Scopes
     *******************************************
     */


    /**
     *******************************************
     * Set Model Attributes
     *******************************************
     */

    /**
     * @return int
     */
    public function getPaymentCountAttribute(){
        return count($this->payments);
    }

    /**
     *******************************************
     * Other Settings
     *******************************************
     */
}
