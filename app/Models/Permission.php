<?php
namespace App\Models;

use App\Traits\Observable;
use App\Traits\Searchable;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Permission\Models\Permission AS Base;

class Permission extends Base implements Auditable
{
    use Searchable, Observable, \OwenIt\Auditing\Auditable;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'pivot',
    ];

    public static $observer = \App\Observers\PermissionObserver::class;
}
