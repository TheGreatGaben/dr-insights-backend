<?php

namespace App\Models;

use OwenIt\Auditing\Contracts\Auditable;

class Project extends Base implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'name', 'client', 'end_customer', 'schedule', 'project_manager', 'project_completion',
        'contract_sum', 'budget', 'retention_percent', 'retention', 'variation_order', 'payment_term',
        'status', 'simulated_cashflow',
        'remarks',
        'omission_value', 'start_date', 'end_date', 'contingency_amt', 'start_date', 'end_date', 'client_id'
    ];

    protected $appends = [];

    protected $hidden = [];

    protected $dates = ['project_completion'];

    protected static $observer = \App\Observers\ProjectObserver::class;

    protected $filters = [
        "projects" => [
            "filter" => ["projects.name", "clients.name", "overall_ie.suppliers", "users.name"],
            "in" => ["status" => ["projects"]]
        ],
        "invoices" => [
            "date" => [
                "payments.payment_date", "invoices.due_date", "invoices.invoice_date",
                "payment_date" => ["payments"],
                "due_date" => ["invoices"],
                "invoice_date" => ["invoices"],
            ],
        ],
        "expenses" => [
            "date" => [
                "payments.payment_date", "expenses.due_date", "expenses.invoice_date", "expenses.date_received",
                "payment_date" => ["payments"],
                "due_date" => ["expenses"],
                "invoice_date" => ["expenses"],
                "date_received" => ["expenses"],
            ],
        ],
    ];

    /**
     *******************************************
     * Set Relationship
     *******************************************
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function pm(){
        return $this->customhasOne(Permission::class, \DB::raw("REPLACE(`permissions`.`name`, 'project.view.', '')"), 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client(){
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function contractors(){
        return $this->belongsToMany(Contractor::class)->using('App\Pivots\ProjectContractors');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function contractor_pos(){
        return $this->hasManyThrough(
            'App\PurchaseOrder',          // The model to access to
            'App\Pivots\ProjectContractors', // The intermediate table that connects the User with the Podcast.
            'project_id',                 // The column of the intermediate table that connects to this model by its ID.
            'contractor_id',              // The column of the intermediate table that connects the Podcast by its ID.
            'id',                      // The column that connects this model with the intermediate model table.
            'contractor_id'               // The column of the Audio Files table that ties it to the Podcast.
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invoices(){
        return $this->hasMany(Invoice::class, 'project_id', 'uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function expenses(){
        return $this->hasMany(Expense::class, 'project_id', 'uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments(){
        return $this->hasMany(Payment::class, 'project_id', 'uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function purchaseOrders(){
        return $this->hasMany(PurchaseOrder::class, 'project_id', 'uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function variationOrders(){
        return $this->hasMany(VariationOrder::class, 'project_id', 'uuid');
    }

    /**
     *******************************************
     * Set Scopes
     *******************************************
     */

    /**
     * join to get all project manager and also
     * allow to search by project manager in filter
     *
     * @param $query
     * @return mixed
     */
    public function scopeGetProjectManagers($query){
        return $query->leftJoin("permissions", \DB::raw("CONCAT('project.view.', `projects`.`id`)"), "=", "permissions.name")
            ->leftJoin("model_has_permissions", function($query){
                return $query->on("model_has_permissions.permission_id", "=", "permissions.id")
                    ->where("model_has_permissions.model_type", "=", "App\Models\User")
                ;
            })
            ->leftJoin("users", function($query){
                return $query->on("users.id", "=", "model_has_permissions.model_id")
                    // ->whereNull("users.deleted_at")
                ;
            })
        ;
    }

    /**
     * to return user's projects that assigned to them
     *
     * @param $query
     * @param $projectManagers
     * @return mixed
     */
    public function scopeProjectManagers($query, $projectManagers = []){
        $byProjectManager = (!empty($projectManagers));
        $projectManagers = ((is_array($projectManagers))?$projectManagers:[$projectManagers]);
        $byAuthUser = ($this->authUser() && !$this->authUser()->hasAnyRole(["KAMI", "Admin", "Business Owner"]));
        return $query->when($byProjectManager || $byAuthUser, function ($query) use ($byProjectManager, $projectManagers){
            return $query->join("permissions AS p", \DB::raw("CONCAT('project.view.', `projects`.`id`)"), "=", "p.name")
                ->join("model_has_permissions AS mp", function($query) use ($byProjectManager, $projectManagers) {
                    return $query->on("mp.permission_id", "=", "p.id")
                        ->where("mp.model_type", "=", "App\Models\User")
                        ->whereIn("mp.model_id", (($byProjectManager)?$projectManagers:[$this->authUser()->id]))
                    ;
                })
            ;
        });
    }

    /**
     * @param $query
     * @param Client|null $client
     * @return mixed
     */
    public function scopeJoinClient($query, Client $client = null){
        return $query->join("clients", "projects.client_id", "=", "clients.uuid")
            ->whereNull("clients.deleted_at")
            ->when($client, function($query) use ($client){
                return $query->where("clients.id", "=", $client->id);
            })
        ;
    }

    /**
     * @param $query
     * @param bool $overall
     * @param int|null $noOfMonths
     * @return mixed
     */
    public function scopeJoinInvoiceExpensesUnion($query, $overall = false, $noOfMonths = null){
        $table = (($overall)?"overall_":"") . "ie";
        $conditions = $finalConditions = ["SUM" => []];
        $now = \Carbon\Carbon::now();
        for($i = 1 ; $i <= (((int)$noOfMonths)?$noOfMonths:0) ; $i++){
            $currentMonth = $now->copy()->subMonthNoOverflow($i);
            $month = strtolower($currentMonth->format("M"));
            $as = "payment_amt_" . $month;
            $start = $currentMonth->startOfMonth()->format("Y-m-d");
            $end = $currentMonth->endOfMonth()->format("Y-m-d");
            $conditions["SUM"][] = [
                "when" => [
                    "if" => "`payments`.`payment_date` BETWEEN '$start' AND '$end'",
                    "then" => "COALESCE(`payments`.`payment_amt`, 0)"
                ],
                "else" => "0", "as" => $as,
            ];
            foreach(["in", "out"] as $cashflow){
                $finalConditions["SUM"] = array_merge($finalConditions["SUM"], [
                    [
                        "when" => [
                            "if" => "`$table`.`cash_flow` = '" . strtoupper($cashflow) . "'",
                            "then" => "`$table`.`$as`"
                        ],
                        "else" => "0", "as" => "cashflow_" . $cashflow . "_" . $month,
                    ]
                ]);
            }
        }
        $selects = ["invoices" => [], "expenses" => []];
        $start_date = (($this->searchRequest && $this->searchRequest->has("simulate_start_date"))?new \Carbon\Carbon($this->searchRequest->get("simulate_start_date")):\Carbon\Carbon::now())->subMonthNoOverflow(1)->startOfMonth();
        foreach(["invoices", "expenses"] as $type){
            $selects[$type][] = ($type == "invoices")?"invoices.nett_certified AS total_amt":\DB::raw("`expenses`.`invoice_amt` + `expenses`.`dc_note` + `expenses`.`credit_note` AS total_amt");
            $selects[$type][] = \DB::raw("`$type`.`gst` + `$type`.`sst` AS `tax_amt`");
            $selects[$type][] = \DB::raw("`$type`.`payment_amt` AS `total_payment_amt_include_tax`");
            $selects[$type][] = \DB::raw("(CASE WHEN (`$type`.`payment_amt` - (`$type`.`gst` + `$type`.`sst`) < 0) THEN 0 ELSE `$type`.`payment_amt` - (`$type`.`gst` + `$type`.`sst`) END) AS `total_payment_amt`");
            $selects[$type][] = \DB::raw("(CASE WHEN `$type`.`balance` > 0 THEN `$type`.`balance` ELSE 0 END) AS `balance`");
            $selects[$type][] = \DB::raw("(CASE WHEN `$type`.`balance` > 0 THEN 0 ELSE ABS(`$type`.`balance`) END) AS `exceeds`");
            $selects[$type][] = \DB::raw("SUM(COALESCE(`payments`.`payment_amt`, 0)) AS `total_payment_amt_include_tax_in_period`");
            $selects[$type][] = \DB::raw("(CASE WHEN (SUM(COALESCE(`payments`.`payment_amt`, 0)) < (`$type`.`gst` + `$type`.`sst`)) THEN 0 ELSE SUM(COALESCE(`payments`.`payment_amt`, 0)) - (`$type`.`gst` + `$type`.`sst`) END) AS `total_payment_amt_in_period`");
            $selects[$type][] = \DB::raw("(" . (($type == "invoices")?"`invoices`.`nett_certified`":"`expenses`.`invoice_amt` + `expenses`.`dc_note` + `expenses`.`credit_note`") . " + `$type`.`gst` + `$type`.`sst`) - SUM(COALESCE(`payments`.`payment_amt`, 0)) AS `balance_in_period`");
            $selects[$type][] = \DB::raw("(CASE WHEN `$type`.`due_date` >= '" . $start_date->format('Y-m-d') . "' AND `$type`.`due_date` <= '" . $start_date->copy()->addMonthNoOverflow(1)->endOfMonth()->format('Y-m-d') . "' THEN `$type`.`balance` ELSE 0 END) AS `cashflow_30`");
            $selects[$type][] = \DB::raw("(CASE WHEN `$type`.`due_date` >= '" . $start_date->format('Y-m-d') . "' AND `$type`.`due_date` <= '" . $start_date->copy()->addMonthNoOverflow(2)->endOfMonth()->format('Y-m-d') . "' THEN `$type`.`balance` ELSE 0 END) AS `cashflow_60`");
            $selects[$type][] = \DB::raw("(CASE WHEN `$type`.`due_date` >= '" . $start_date->format('Y-m-d') . "' AND `$type`.`due_date` <= '" . $start_date->copy()->addMonthNoOverflow(3)->endOfMonth()->format('Y-m-d') . "' THEN `$type`.`balance` ELSE 0 END) AS `cashflow_90`");
        }
        return $query->leftJoinSub(
                \DB::query()->fromSub(
                    Project::search($this->searchRequest, "expenses", !$overall)
                        ->joinExpensesPayment()
                        ->unionAll(
                            Project::search($this->searchRequest, "invoices", !$overall)
                                ->joinInvoicesPayment()
                                ->addSelect(array_merge([
                                    "projects.uuid AS project_id",
                                    \DB::raw("'IN' AS `cash_flow`"),
                                    \DB::raw("null AS `suppliers`"),
                                ], $selects["invoices"], caseWhen($conditions)))
                                ->groupBy(["projects.uuid", "invoices.id"])
                        )->addSelect(array_merge([
                            "projects.uuid AS project_id",
                            \DB::raw("'OUT' AS `cash_flow`"),
                            \DB::raw("GROUP_CONCAT(DISTINCT `contractors`.`name`, ';|') AS `suppliers`"),
                        ], $selects["expenses"], caseWhen($conditions)))
                        ->groupBy(["projects.uuid", "expenses.id"])
                , $table)->addSelect(array_merge([
                    "project_id",
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'IN' THEN SUM(COALESCE(`$table`.`total_amt`, 0)) ELSE 0 END) AS `total_invoiced`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'IN' THEN SUM(COALESCE(`$table`.`tax_amt`, 0)) ELSE 0 END) AS `invoices_tax_amt`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'IN' THEN SUM(COALESCE(`$table`.`total_payment_amt`, 0)) ELSE 0 END) AS `total_received`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'IN' THEN SUM(COALESCE(`$table`.`total_payment_amt_include_tax`, 0)) ELSE 0 END) AS `total_received_include_tax`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'IN' THEN SUM(COALESCE(`$table`.`balance`, 0)) ELSE 0 END) AS `total_receivable`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'IN' THEN SUM(COALESCE(`$table`.`exceeds`, 0)) ELSE 0 END) AS `total_advance_received`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'IN' THEN SUM(COALESCE(`$table`.`total_payment_amt_in_period`, 0)) ELSE 0 END) AS `total_received_in_period`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'IN' THEN SUM(COALESCE(`$table`.`total_payment_amt_include_tax_in_period`, 0)) ELSE 0 END) AS `total_received_include_tax_in_period`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'IN' THEN SUM(COALESCE(`$table`.`balance_in_period`, 0)) ELSE 0 END) AS `total_receivable_in_period`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'IN' THEN SUM(COALESCE(`$table`.`cashflow_30`, 0)) ELSE 0 END) AS `cashflow_in_30`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'IN' THEN SUM(COALESCE(`$table`.`cashflow_60`, 0)) ELSE 0 END) AS `cashflow_in_60`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'IN' THEN SUM(COALESCE(`$table`.`cashflow_90`, 0)) ELSE 0 END) AS `cashflow_in_90`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'OUT' THEN SUM(COALESCE(`$table`.`total_amt`, 0)) ELSE 0 END) AS `total_expenses`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'OUT' THEN SUM(COALESCE(`$table`.`tax_amt`, 0)) ELSE 0 END) AS `expenses_tax_amt`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'OUT' THEN SUM(COALESCE(`$table`.`total_payment_amt`, 0)) ELSE 0 END) AS `total_paid`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'OUT' THEN SUM(COALESCE(`$table`.`total_payment_amt_include_tax`, 0)) ELSE 0 END) AS `total_paid_include_tax`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'OUT' THEN SUM(COALESCE(`$table`.`balance`, 0)) ELSE 0 END) AS `total_payable`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'OUT' THEN SUM(COALESCE(`$table`.`exceeds`, 0)) ELSE 0 END) AS `total_overpay`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'OUT' THEN SUM(COALESCE(`$table`.`total_payment_amt_in_period`, 0)) ELSE 0 END) AS `total_paid_in_period`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'OUT' THEN SUM(COALESCE(`$table`.`total_payment_amt_include_tax_in_period`, 0)) ELSE 0 END) AS `total_paid_include_tax_in_period`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'OUT' THEN SUM(COALESCE(`$table`.`balance_in_period`, 0)) ELSE 0 END) AS `total_payable_in_period`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'OUT' THEN SUM(COALESCE(`$table`.`cashflow_30`, 0)) ELSE 0 END) AS `cashflow_out_30`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'OUT' THEN SUM(COALESCE(`$table`.`cashflow_60`, 0)) ELSE 0 END) AS `cashflow_out_60`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'OUT' THEN SUM(COALESCE(`$table`.`cashflow_90`, 0)) ELSE 0 END) AS `cashflow_out_90`"),
                    \DB::raw("(CASE WHEN `$table`.`cash_flow` = 'OUT' THEN GROUP_CONCAT(`$table`.`suppliers`, '') ELSE 0 END) AS `suppliers`"),
                ], caseWhen($finalConditions)))
                ->groupBy(["$table.project_id", "$table.cash_flow"])
            , $table, function($query) use ($table){
                return $query->on("projects.uuid", "=", "$table.project_id");
            })
        ;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeJoinInvoicesPayment($query){
        return $query->leftJoin("invoices", function($query){
                return $query->on("projects.uuid", "=", "invoices.project_id")
                    ->whereNull("invoices.deleted_at")
                ;
            })->leftJoin("payments", function($query){
                return $query->on("invoices.uuid", "=", "payments.invoice_id")
                    ->whereNull("payments.deleted_at")
                    ->where("payments.cash_flow", "=", "IN")
                ;
            })
        ;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeJoinExpensesPayment($query){
        return $query->leftJoin("expenses", function($query){
                return $query->on("projects.uuid", "=", "expenses.project_id")
                    ->whereNull("expenses.deleted_at")
                ;
            })->leftJoin("payments", function($query){
                return $query->on("expenses.uuid", "=", "payments.expenses_id")
                    ->whereNull("payments.deleted_at")
                    ->where("payments.cash_flow", "=", "OUT")
                ;
            })->leftJoin("contractors", function($query){
                return $query->on("expenses.contractor_id", "=", "contractors.uuid")
                    ->whereNull("contractors.deleted_at")
                ;
            })
        ;
    }

    /**
     *******************************************
     * Set Model Attributes
     *******************************************
     */

    /**
     * @return mixed
     */
    public function getContractSumAndVoAttribute(){
        return $this->contract_sum + $this->variation_order;
    }

    /**
     * @return float|int
     */
    public function getCurrentMarginAttribute(){
        if(($contract_sum = $this->contract_sum_and_vo - $this->retention) > 0)
            return round(($contract_sum - $this->budget) / ($contract_sum) * 100, 2);
        return 0;
    }

    /**
     * @return float|int
     */
    public function getDurationLaAttribute(){
        if(strtotime($this->end_date) > 0 && strtotime($now = \Carbon\Carbon::now()) >= strtotime($this->start_date)){
            $duration = strtotime($this->end_date) - strtotime($this->start_date);
            $durationLa = round((strtotime($now) - strtotime($this->start_date)) / $duration * 100, 2);
            return (($durationLa > 100)?100:$durationLa);
        }
        return 0;
    }

    /**
     *******************************************
     * Other Settings
     *******************************************
     */


}
