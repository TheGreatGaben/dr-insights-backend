<?php

namespace App\Models;

use OwenIt\Auditing\Contracts\Auditable;

class PurchaseOrder extends Base implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'project_id', 'contractor_id', 'issued_date', 'po_no', 'party', 'description', 'total_value', 'budget',
        'gst', 'sst', 'exclude_tax', 'dc_note', 'credit_note'
    ];

    protected $appends = [];

	protected $dates = ['issued_date'];

    protected $filters = [
        "purchase_orders" => [
            "filter" => ["purchase_orders.po_no", "projects.name", "contractors.name"],
            "string" => [
                "project_id" => ["purchase_orders"],
                "contractor_id" => ["purchase_orders"],
            ],
        ],
    ];

    /**
     *******************************************
     * Set Relationship
     *******************************************
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project(){
        return $this->belongsTo(Project::class,'project_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contractor(){
        return $this->belongsTo(Contractor::class,'contractor_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function expenses(){
        return $this->morphMany(Expense::class, 'payable');
    }

    /**
     *******************************************
     * Set Scopes
     *******************************************
     */

    /**
     * @param $query
     * @return mixed
     */
    public function scopeJoinProject($query, Project $project = null){
        return $query->join("projects", "purchase_orders.project_id", "=", "projects.uuid")
            ->whereNull("projects.deleted_at")
            ->when($project, function($query) use ($project){
                $query->where("projects.id", "=", $project->id);
            })
        ;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeJoinContractor($query, Contractor $contractor = null){
        return $query->join("contractors", "purchase_orders.contractor_id", "=", "contractors.uuid")
            ->whereNull("contractors.deleted_at")
            ->when($contractor, function($query) use ($contractor){
                $query->where("contractors.id", "=", $contractor->id);
            })
        ;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeLeftJoinExpenses($query) {
        return $query->leftJoin("expenses", function($query) {
            $query->on("expenses.payable_id", "=", "purchase_orders.id")
                ->whereNull("expenses.deleted_at")
                ->where(function($query){
                    return $query->where("expenses.payable_type", "=", "App\Models\PurchaseOrder")
                        ->orWhere("expenses.payable_type", "=", "App\PurchaseOrder")
                    ;
                })
            ;
        });
    }
}
