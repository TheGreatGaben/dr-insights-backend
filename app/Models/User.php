<?php
namespace App\Models;

use Carbon\Carbon;
use App\Traits\Observable;
use App\Traits\Searchable;
use Laravel\Passport\HasApiTokens;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements Auditable
{
    use Searchable, Observable, HasApiTokens, Notifiable, HasRoles, \OwenIt\Auditing\Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'pivot',
    ];

    protected $filters = [
        "users" => [
            "filter" => ["name", "email"],
        ],
    ];

    protected $guard_name = "api";

    protected static $observer = \App\Observers\UserObserver::class;

    /**
     *******************************************
     * Set Scopes
     *******************************************
     */

    /**
     * @param $query
     * @param bool $withInvoice
     * @param bool $withExpenses
     * @return mixed
     */
    public function scopeProjects($query){
        return $query->join("model_has_permissions", function($query){
                return $query->on("users.id", "=", "model_has_permissions.model_id")
                    ->where("model_has_permissions.model_type", "=", "App\Models\User")
                ;
            })->join("permissions", function($query){
                return $query->on("permissions.id", "=", "model_has_permissions.permission_id")
                ;
            })->join("projects", function($query){
                return $query->on("projects.id", "=", \DB::raw("REPLACE(`permissions`.`name`, 'project.view.', '')"))
                    ->whereNull("projects.deleted_at")
                ;
            })
        ;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeUnionInvoicesExpenses($query, $noOfMonth = 1){
        $finalConditions["SUM"] = $conditions = $selects = [];
        $now = Carbon::now();
        for($i = 1 ; $i <= (((int)$noOfMonth)?$noOfMonth:1) ; $i++){
            $currentMonth = $now->copy()->subMonthNoOverflow($i);
            $month = strtolower($currentMonth->format("M"));
            $as = "payment_amt_" . $month;
            $start = $currentMonth->startOfMonth()->format("Y-m-d");
            $end = $currentMonth->endOfMonth()->format("Y-m-d");
            $conditions["SUM"][] = [
                "when" => [
                    [
                        "if" => "`payments`.`payment_date` BETWEEN '$start' AND '$end'",
                        "then" => "COALESCE(`payments`.`payment_amt`, 0)"
                    ],
                ],
                "else" => "0", "as" => $as,
            ];
            foreach(["in", "out"] as $cashflow){
                $selects[] = \DB::raw("COALESCE(SUM(`ie`.`cashflow_" . $cashflow . "_" . $month . "`), 0) AS `cashflow_" . $cashflow . "_" . $month . "`");
                $finalConditions["SUM"] = array_merge($finalConditions["SUM"], [
                    [
                        "when" => [
                            "if" => ["`i_e`.`cash_flow` = '" . strtoupper($cashflow) . "'", "`i_e`.`$as` > 0"],
                            "then" => "(CASE WHEN `i_e`.`$as` >= `i_e`.`tax_amt` THEN `i_e`.`$as` - `i_e`.`tax_amt` ELSE 0 END)"
                        ],
                        "else" => "0", "as" => "cashflow_" . $cashflow . "_" . $month,
                    ],
                ]);
            }
            $selects[] = \DB::raw("COALESCE(SUM(`ie`.`cashflow_in_$month` - `ie`.`cashflow_out_$month`), 0) AS `cashflow_nett_$month`");
        }
        return $query->leftJoinSub(
                \DB::query()->fromSub(
                    \DB::query()->fromSub(
                        Project::joinExpensesPayment()
                            ->unionAll(
                                Project::joinInvoicesPayment()
                                    ->addSelect(array_merge([
                                        "projects.uuid AS project_id",
                                        \DB::raw("'IN' AS `cash_flow`"),
                                        \DB::raw("`invoices`.`gst` + `invoices`.`sst` AS `tax_amt`"),
                                    ], caseWhen($conditions)))
                                    ->groupBy(["projects.uuid", "invoices.id"])
                            )->addSelect(array_merge([
                                "projects.uuid AS project_id",
                                \DB::raw("'OUT' AS `cash_flow`"),
                                \DB::raw("`expenses`.`gst` + `expenses`.`sst` AS `tax_amt`"),
                            ], caseWhen($conditions)))
                            ->groupBy(["projects.uuid", "expenses.id"])
                    , "invoices_expenses")
                , "i_e")
                ->addSelect(array_merge(["i_e.project_id"], caseWhen($finalConditions)))
                ->groupBy(["i_e.project_id"])
            , "ie", function($query){
                return $query->on("projects.uuid", "=", "ie.project_id");
            })
            ->addSelect($selects);
        ;
    }

    /**
     *******************************************
     * Set Model Attributes
     *******************************************
     */

    /**
     * @param $pass
     */
    public function setPasswordAttribute($pass){
        $this->attributes['password'] = bcrypt($pass);
    }

}
