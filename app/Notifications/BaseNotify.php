<?php
namespace App\Notifications;

use Illuminate\Notifications\Notification;

class BaseNotify extends Notification
{
    /**
     * @var $subject
     */
    public $subject;

    /**
     * @var $view
     */
    public $view = [];

    /**
     * @var array
     */
    public $viewData = [];

    public function subject($subject){
        $prefix = "";
        if(!in_array(strtolower($env = config("app.env")), ["live", "prod", "production"]))
            $prefix = "[" . ucfirst(($env)?$env:"Dev") . ": " . config('app.name') . "] ";
        $this->subject = $prefix . $subject;
    }
}
