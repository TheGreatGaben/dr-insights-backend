<?php
namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ErrorNotify extends BaseNotify implements ShouldQueue
{
    use Queueable;

    public $tries = 3; // Max tries
    public $timeout = 15; // Timeout seconds

    public $error;
    public $trace;

    /**
     * Create a new notification instance.
     *
     * @param $error
     * @param string $trace
     */
    public function __construct($error, $trace)
    {
        $this->queue = "err_email";

        $this->subject('Error Notification');
        $this->view = 'emails.error';
        $this->viewData = [
            "error" => $error,
            "trace" => $trace
        ];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->subject($this->subject)
            ->view($this->view, $this->viewData);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
