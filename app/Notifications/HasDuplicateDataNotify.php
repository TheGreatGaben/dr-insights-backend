<?php
namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class HasDuplicateDataNotify extends BaseNotify implements ShouldQueue
{
    use Queueable;

    public $tries = 3; // Max tries
    public $timeout = 15; // Timeout seconds

    public $model;
    public $duplicateData;

    /**
     * Create a new notification instance.
     *
     * @param $error
     * @param string $trace
     */
    public function __construct($model, $duplicateData)
    {
        $this->queue = "duplicate_email"; // set the queue name

        $this->subject('Duplicate Data Found for Model: ' . get_class($model));
        $this->view = 'emails.hasDuplicateData';
        $this->viewData = [
            "model" => $model,
            "duplicateData" => $duplicateData
        ];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->subject($this->subject)
            ->view($this->view, $this->viewData);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
