<?php
namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PasswordResetRequest extends BaseNotify
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @param string $token
     * @param bool $isAdmin
     */
    public function __construct($userEmail, $token, $isAdmin = false)
    {
        $this->subject('Forget Password');
        $this->view = 'emails.forgotPassword';
        $this->viewData = [
            "url" => url(config('auth.urls.' . (($isAdmin)?'admin':'app')) . '/password/reset/' . $userEmail . '/' . $token),
        ];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->subject($this->subject)
            ->view($this->view, $this->viewData);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
