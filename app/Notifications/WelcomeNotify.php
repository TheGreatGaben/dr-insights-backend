<?php
namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class WelcomeNotify extends BaseNotify
{
    use Queueable;

    protected $userEmail;
    protected $userPass;
    protected $isAdmin;

    /**
     * Create a new notification instance.
     *
     * @param string $token
     * @param bool $isAdmin
     */
    public function __construct($userEmail, $userPass, $isAdmin = false)
    {
        $this->subject('Welcome To ' . config('app.name'));
        $this->view = 'emails.welcome';
        $this->viewData = [
            "user_email" => $userEmail,
            "user_password" => $userPass,
            "is_admin" => $isAdmin,
        ];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->subject($this->subject)
            ->view($this->view, $this->viewData);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
