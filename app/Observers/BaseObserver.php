<?php

namespace App\Observers;

class BaseObserver
{
    /**
     * compare amounts between original and new
     * will return true if any data different
     *
     * @param $model
     * @param $fields
     * @return bool
     */
    public function isAnyAmountDiff($model, $fields){
        $original = $model->getOriginal();
        $fields = ((is_array($fields))?$fields:[$fields]);
        foreach($fields as $field){
            if(isset($original[$field]) && isset($model->$field) && $original[$field] != $model->$field){
                return true;
            }
        }
        return false;
    }
}
