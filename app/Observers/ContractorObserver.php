<?php

namespace App\Observers;

use App\Models\Project;
use App\Models\Contractor;

class ContractorObserver
{
    /**
     * Handle the project "saving" event.
     *
     * @param  Contractor  $contractor
     * @return void|bool
     */
    public function saving(Contractor $contractor){
        if (!$contractor->id) {
            $trashedContractor = Contractor::where('name', $contractor->name)->withTrashed()->first();
            if($trashedContractor && $trashedContractor->deleted_at) {
                // Restoring would not trigger the "saved" event
                $trashedContractor->restore();
                return false;
            }
        }
    }

    /**
     * Handle the project "saved" event.
     *
     * @param  Contractor  $contractor
     * @return void|bool
     */
    public function saved(Contractor $contractor){
        $this->attachProject($contractor);
    }

    /**
     * Handle the project "deleting" event.
     *
     * @param Contractor $contractor
     * @return void|bool
     * @throws \Illuminate\Validation\ValidationException
     */
    public function deleting(Contractor $contractor){
        if(request()->has("project_id")){
            $project = Project::findByUuid(request()->project_id);
            if($contractor->projects->contains($project->id))
                $contractor->projects()->detach($project->id);
            return false;
        }else{
            if($contractor->projects()->count() > 0){
                throw \Illuminate\Validation\ValidationException::withMessages(['contractor_id' => __("validation.custom.has_other_projects"), 'projects' => $contractor->projects()->get(["projects.uuid", "projects.name"])]);
            }
        }
    }

    /**
     * add relation between contractor and project
     * if project id is provided
     *
     * @param Contractor $contractor
     */
    public function attachProject(Contractor $contractor) {
        if(request()->has('project_id')){
            $projectID = Project::select('id')->where('uuid', request()->get('project_id'))->get()->first()->id;
            if(!$contractor->projects->contains($projectID))
                $contractor->projects()->attach($projectID);
        }
    }
}
