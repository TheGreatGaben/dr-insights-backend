<?php

namespace App\Observers;

use Carbon\Carbon;
use App\Models\Project;
use App\Models\Expense;
use App\Models\Contractor;

class ExpenseObserver extends BaseObserver
{
    /**
     * Handle the project "creating" event.
     *
     * @param Expense $expenses
     * @return void
     */
    public function creating(Expense $expenses){
        $expenses->gst = $expenses->sst = 0;
        if(request()->has("tax_type")){
            $expenses->{request()->tax_type} = request()->tax_amt;
        }
        $expenses->dc_note *= -1;
        $expenses->balance = $expenses->total_amount;
    }

    /**
     * Handle the project "updating" event.
     *
     * @param Expense $expenses
     * @return void
     */
    public function updating(Expense $expenses){
        if(request()->has("tax_type")){
            $expenses->gst = $expenses->sst = 0;
            $expenses->{request()->tax_type} = request()->tax_amt;
        }
        if($this->isAnyAmountDiff($expenses, ["invoice_amt", "gst", "sst", "dc_note"])){
            $expenses->dc_note *= ($expenses->isDirty('dc_note')) ? -1: 1;
            $expenses->balance = $expenses->accurate_balance;
        }
        if (request()->has('write_off_reason')) {
            $expenses->write_off_date = Carbon::now();
            $expenses->write_off_user = request()->user()->id;
            $expenses->write_off_amt = $expenses->balance;
            $expenses->balance = 0;
        }
    }

    /**
     * Handle the project "saving" event.
     * it is for both creating/updating
     *
     * @param Expense $expenses
     * @return void
     */
    public function saving(Expense $expenses){
        if($expenses->isDirty(["contractor"])){
            $contractor = Contractor::where(function($query) use ($expenses){
                    return $query->orWhere('uuid', $expenses->contractor)
                        ->orWhere('name', $expenses->contractor);
                })->withTrashed()->first();
            if($contractor && $contractor->deleted_at){
                $contractor->restore();
            }elseif(!$contractor){
                $contractor = Contractor::create(['name' => $expenses->contractor, 'type' => $expenses->contractor_type]);
            }
            $project = Project::where('uuid', $expenses->project_id)->first();
            if(!$project->contractors->contains($contractor->id)){
                $project->contractors()->attach($contractor);
            }
            $expenses->contractor_id = $contractor->uuid;
            unset($expenses->contractor);
        }
        $this->checkRefNoDuplicate($expenses);
        $this->linkExpensesPO($expenses);
    }

    /**
     * Handle the project "saved" event.
     *
     * @param Expense $expenses
     * @return void
     */
    public function saved(Expense $expenses){
        if($this->isAnyAmountDiff($expenses, ["invoice_amt", "dc_note"])) {
            $project = Project::findByUuid($expenses->project_id);
            $project->total_expenses = $project->expenses()->sum("invoice_amt") + $project->expenses()->sum("dc_note") + $project->expenses()->sum("credit_note");
            $project->save();
        }
    }

    /**
     * Handle the project "deleting" event.
     * it is for both creating/updating
     *
     * @param Expense $expenses
     * @return void
     */
    public function deleting(Expense $expenses){
        $expenses->payments()->delete();
    }

    /**
     * @param Expense $expenses
     */
    public function linkExpensesPO(Expense $expenses){
        if($expenses->isDirty(["payable_id", "payable_type"]) && $expenses->payable_id && $expenses->payable_type){
            if(!($expenses->payable_type)::where('id', $expenses->payable_id)->exists())
                $expenses->payable_id = null;
        }
    }

    /**
     * @param Expense $expenses
     * @throws \Illuminate\Validation\ValidationException
     */
    public function checkRefNoDuplicate(Expense $expenses){
        $exists = Expense::where("project_id", "=", $expenses->project_id)
            ->where("contractor_id", "=", $expenses->contractor_id)
            ->where("ref_no", "=", $expenses->ref_no)
            // ->where("invoice_amt", $expenses->invoice_amt)
            ->when($expenses->id, function($query) use ($expenses){
                return $query->where("id", "!=", $expenses->id);
            })
            ->exists();

        if($exists && (!request()->has("confirm") || !request()->get("confirm"))) {
            throw \Illuminate\Validation\ValidationException::withMessages(['ref_no' => __("validation.custom.ref_no_duplicate"), 'has_duplicate' => true]);
        }
    }
}
