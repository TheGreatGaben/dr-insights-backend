<?php

namespace App\Observers;

use Carbon\Carbon;
use App\Models\Project;
use App\Models\Invoice;

class InvoiceObserver extends BaseObserver
{
    /**
     * Handle the project "creating" event.
     *
     * @param Invoice $invoice
     * @return void
     */
    public function creating(Invoice $invoice){
        $project = Project::findByUuid($invoice->project_id);
        if (request()->has('client_id')) {
            $invoice->client_id = request()->client_id;
        } else {
            $invoice->client_id = $project->client_id;
        }
        $invoice->gst = $invoice->sst = 0;
        if(request()->has("tax_type")){
            $invoice->{request()->tax_type} = request()->tax_amt;
        }
        $invoice->retention *= -1;
        $invoice->prev_certified *= -1;
        $invoice->debit_note *= -1;
        $invoice->due_date = (new Carbon($invoice->invoice_date))->addDays($invoice->term)->format('Y-m-d');
        $invoice->balance = $invoice->total_amount;
    }

    /**
     * Handle the project "updating" event.
     *
     * @param Invoice $invoice
     * @return void
     */
    public function updating(Invoice $invoice){
        if (request()->has('client_id')) {
            $invoice->client_id = request()->client_id;
        }
        if(request()->has("tax_type")){
            $invoice->gst = $invoice->sst = 0;
            $invoice->{request()->tax_type} = request()->tax_amt;
        }
        $invoice->retention *= (($invoice->isDirty("retention"))?-1:1);
        $invoice->prev_certified *= (($invoice->isDirty("prev_certified"))?-1:1);
        $invoice->due_date = (new Carbon($invoice->invoice_date))->addDays($invoice->term)->format('Y-m-d');
        if($this->isAnyAmountDiff($invoice, ["nett_certified", "gst", "sst", "debit_note"])){
            $invoice->debit_note *= (($invoice->isDirty("debit_note"))?-1:1);
            $invoice->balance = $invoice->accurate_balance;
        }
        if (request()->has('write_off_reason')) {
            $invoice->write_off_date = Carbon::now();
            $invoice->write_off_user = request()->user()->id;
            $invoice->write_off_amt = $invoice->balance;
            $invoice->balance = 0;
        }
    }

    /**
     * Handle the project "deleting" event.
     *
     * @param Invoice $invoice
     * @return void
     */
    public function deleting(Invoice $invoice){
        $invoice->payments()->delete();
    }
}
