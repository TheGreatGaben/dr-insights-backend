<?php

namespace App\Observers;

use Carbon\Carbon;
use App\Models\Invoice;
use App\Models\Expense;
use App\Models\Payment;
use App\Models\PaymentRecord;

class PaymentObserver
{
    /**
     * Handle the payment "saving" event.
     *
     * @param  Payment  $payment
     * @return void
     */
    public function saving(Payment $payment){
        if($payment->batch_no){
            $payment->cash_flow = (($payment->cash_flow)?$payment->cash_flow:"OUT");
            $payment->payment_date = (($payment->payment_date)?$payment->payment_date:Carbon::now());
        }
    }

    /**
     * Handle the payment "saved" event.
     *
     * @param  Payment  $payment
     * @return void
     */
    public function saved(Payment $payment){
        $this->updateBalance($payment);
        $this->updatePaymentRecord($payment);
    }

    /**
     * Handle the payment "deleted" event.
     *
     * @param  Payment  $payment
     * @return void
     */
    public function deleted(Payment $payment){
        $this->updateBalance($payment);
        $this->updatePaymentRecord($payment);
    }

    /**
     * Handle the payment "restored" event.
     *
     * @param  Payment  $payment
     * @return void
     */
    public function restored(Payment $payment){
        $this->updateBalance($payment);
        $this->updatePaymentRecord($payment);
    }

    /**
     * @param Payment $payment
     */
    private function updateBalance(Payment $payment){
	$parent_model = null;
        if(strtolower($payment->cash_flow) == 'out'){
            $parent_model = Expense::where('uuid', $payment->expenses_id)->first();
        }elseif(strtolower($payment->cash_flow) == 'in'){
            $parent_model = Invoice::where('uuid', $payment->invoice_id)->first();
        }
        if($parent_model != null){
            $parent_model->qSave([
                'payment_amt' => $parent_model->accurate_total_payment,
                'balance' => $parent_model->accurate_balance,
            ]);
        }
    }

    /**
     * @param $payment
     */
    private function updatePaymentRecord(Payment $payment){
        $paymentRecord = PaymentRecord::find($payment->batch_no);
        if($paymentRecord != null){
            $paymentRecord->total_paid = $paymentRecord->payments()->sum('payment_amt');
            $paymentRecord->save();
        }
    }
}
