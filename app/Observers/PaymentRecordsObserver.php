<?php

namespace App\Observers;

use Carbon\Carbon;
use App\Models\BulkPayer;
use App\Models\PaymentRecord;

class PaymentRecordsObserver
{
    /**
     * Handle the payment "saving" event.
     *
     * @param PaymentRecord $paymentRecord
     * @return void
     */
    public function saving(PaymentRecord $paymentRecord){
        if(request()->has("payer_name")){
            $payer = BulkPayer::where("payer_name", "=", request()->get("payer_name"))->first();
            if(!$payer){
                $payer = BulkPayer::create(request()->only(["payer_name"]));
            }
            $paymentRecord->payer_id = $payer->id;
        }
        $paymentRecord->payment_date = ((request()->has("payment_date"))?request()->get("payment_date"):(($paymentRecord->payment_date)?$paymentRecord->payment_date:Carbon::now()));
    }

    /**
     * Handle the payment "created" event.
     *
     * @param PaymentRecord $paymentRecord
     * @return void
     * @throws \Exception
     */
    public function created(PaymentRecord $paymentRecord){
        $payments = request()->get('payments');
        $paymentRecord->payments()->createMany($payments);
    }

    /**
     * Handle the payment "deleting" event.
     *
     * @param  PaymentRecord  $paymentRecord
     * @return void
     */
    public function deleting(PaymentRecord $paymentRecord){
        $paymentRecord->payments()->delete();
    }
}
