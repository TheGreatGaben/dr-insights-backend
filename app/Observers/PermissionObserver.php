<?php

namespace App\Observers;

use App\Models\Role;
use App\Models\Permission;
use App\Queries\v2 AS Query;

class PermissionObserver
{
    /**
     * Handle the project "created" event.
     *
     * @param  Permission  $permission
     * @return void
     */
    public function created(Permission $permission){
        if(request()->has("roles")){
            $all_roles = [];
            $roles = request()->get("roles");
            $roles = (is_array($roles)?$roles:[$roles]);
            foreach($roles as $role_id){
                $role = Role::find($role_id);
                $role->givePermissionTo($permission);
                $all_roles[] = ["id" => $role_id, "name" => $role->name];
            }
            $permission->allRoles = $all_roles;
        }
    }

    /**
     * Handle the project "updated" event.
     *
     * @param  Permission  $project
     * @return void
     */
    public function updated(Permission $permission){
        if(request()->has("roles")){
            $all_roles = [];
            // get all current project manager, will remove if not in new project manager list
            $current_roles = with(new Query\PermissionQuery(null, $permission))->getPermissionRole()->pluck("id")->toArray();
            // new roles list
            $selected_roles = request()->get("roles");
            $selected_roles = (is_array($selected_roles)?$selected_roles:[$selected_roles]);
            // remove old roles if any
            $diff_roles = array_diff($current_roles, $selected_roles);
            foreach($diff_roles as $role_id) {
                $role = Role::find($role_id);
                $role->revokePermissionTo($permission);
            }
            // update new roles
            foreach($selected_roles as $role_id){
                $role = Role::find($role_id);
                if(!in_array($role_id, $current_roles)) {
                    $role->givePermissionTo($permission);
                }
                $all_roles[] = ["id" => $role_id, "name" => $role->name];
            }
            $permission->allRoles = $all_roles;
        }
    }
}
