<?php

namespace App\Observers;

use App\Models\User;
use App\Models\Client;
use App\Models\Project;
use App\Models\Permission;

class ProjectObserver
{
    /**
     * Handle the project "created" event.
     *
     * @param  Project  $project
     * @return void
     */
    public function created(Project $project){
        // create new permission once project is created
        Permission::create(['name' => "project.view." . $project->id, 'guard_name' => 'api']);
        // if project assign to any project managers, here will give permission to them
        if(request()->has("project_managers")){
            $project_managers = request()->get("project_managers");
            $project_managers = ((is_array($project_managers))?$project_managers:[$project_managers]);
            foreach($project_managers as $project_manager_id){
                $project_manager = User::find($project_manager_id);
                $project_manager->givePermissionTo("project.view." . $project->id);
            }
        }
    }

    /**
     * Handle the project "updated" event.
     *
     * @param  Project  $project
     * @return void
     */
    public function updated(Project $project){
        if(request()->has("project_managers")){
            // get all current project manager, will remove if not in new project manager list
            $current_project_managers = User::select("id")->permission("project.view." . $project->id)->get()->pluck('id')->toArray();
            // new project manager list
            $selected_project_managers = request()->get("project_managers");
            $selected_project_managers = ((is_array($selected_project_managers))?$selected_project_managers:[$selected_project_managers]);
            // remove old project manger if any
            $diff_project_managers = array_diff($current_project_managers, $selected_project_managers);
            foreach($diff_project_managers as $project_manager_id) {
                $project_manager = User::find($project_manager_id);
                $project_manager->revokePermissionTo("project.view." . $project->id);
            }
            // update new project manager
            foreach($selected_project_managers as $project_manager_id){
                if(!in_array($project_manager_id, $current_project_managers)){
                    $project_manager = User::find($project_manager_id);
                    $project_manager->givePermissionTo("project.view." . $project->id);
                }
            }
        }
    }

    /**
     * Handle the project "saving" event.
     * it is for both creating/updating
     *
     * @param  Project  $project
     * @return void
     */
    public function saving(Project $project){
        if($project->isDirty(["client"])){
            $client = Client::where("name", "=", $project->client)->first();
            if($client == null){
                $client = Client::create([
                    'name' => $project->client
                ]);
            }
            $project->client_id = $client->uuid;
            $project->invoices()->update(['client_id' => $client->uuid]);
        }
    }

    /**
     * Handle the project "deleting" event.
     * it is for both creating/updating
     *
     * @param  Project  $project
     * @return void
     */
    public function deleting(Project $project){
        $project->payments()->delete();
        $project->invoices()->delete();
        $project->purchaseOrders()->delete();
        $project->variationOrders()->delete();
        $project->expenses()->delete();
    }
}
