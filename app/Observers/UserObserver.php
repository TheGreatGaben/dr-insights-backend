<?php

namespace App\Observers;

use App\Models\User;
use App\Events\NewUserAddedEvent;

class UserObserver
{
    /**
     * Handle the project "created" event.
     *
     * @param  User  $user
     * @return void
     */
    public function created(User $user){
        //event(new NewUserAddedEvent($user, request()->get("password"), request()->isAdmin));
    }

    /**
     * Handle the project "saved" event.
     * it is for both created/updated
     *
     * @param  User  $user
     * @return void
     */
    public function saved(User $user){
        if(request()->has("role") && !empty(request()->only("role"))){
            $user->syncRoles(request()->only("role"));
        }
    }
}
