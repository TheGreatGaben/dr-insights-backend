<?php

namespace App\Pivots;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ProjectContractors extends Pivot
{

    protected $table = "contractor_project";

    public function project(){
        return $this->belongsTo(\App\Models\Project::class);
    }

    public function contractor(){
        return $this->belongsTo(\App\Models\Contractor::class);
    }

    public function pos(){
        return $this->hasManyThrough(\App\Models\PurchaseOrder::class, \App\Models\Contractor::class);
    }

}
