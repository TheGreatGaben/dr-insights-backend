<?php
namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class BasePolicy
{
    use HandlesAuthorization;

    /**
     * @var
     */
    protected $model;

    /**
     * @var
     */
    protected $rolesCanDoAll;

    /**
     * return base model class name
     *
     * @return mixed
     */
    public function getModel(){
        return $this->model;
    }
}
