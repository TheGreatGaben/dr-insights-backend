<?php
namespace App\Policies;

use App\Models\User;
use App\Models\Client;

class ClientPolicy extends BasePolicy
{
    protected $model = Client::class;

    protected $rolesCanDoAll = [
        "KAMI" => 1,
        "Admin" => 2,
    ];

    /**
     * Determine whether the user can view the resource.
     *
     * @param User $user
     * @param Client|null $client
     * @return bool
     */
    public function view(User $user, Client $client = null)
    {
        if(
            $user->hasAnyRole(array_merge($this->rolesCanDoAll, ["Business Owner" => 3, "Project Manager" => 4])) ||
            $user->can('client')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can create resource.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('client.add')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can update the resource.
     *
     * @param User $user
     * @param Client $client
     * @return bool
     */
    public function update(User $user, Client $client)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('client.edit')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can delete the resource.
     *
     * @param User $user
     * @param Client $client
     * @return bool
     */
    public function delete(User $user, Client $client)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('client.delete')
        ){
            return true;
        }
    }
}
