<?php
namespace App\Policies;

use App\Models\User;
use App\Models\Contractor;

class ContractorPolicy extends BasePolicy
{
    protected $model = Contractor::class;

    protected $rolesCanDoAll = [
        "KAMI" => 1,
        "Admin" => 2,
    ];

    /**
     * Determine whether the user can view the resource.
     *
     * @param User $user
     * @param Contractor|null $contractor
     * @return bool
     */
    public function view(User $user, Contractor $contractor = null)
    {
        if(
            $user->hasAnyRole(array_merge($this->rolesCanDoAll, ["Business Owner" => 3, "Project Manager" => 4])) ||
            $user->can('contractor')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can create resource.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('contractor.add')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can update the resource.
     *
     * @param User $user
     * @param Contractor|null $contractor
     * @return bool
     */
    public function update(User $user, Contractor $contractor = null)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('contractor.edit')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can delete the resource.
     *
     * @param User $user
     * @param Contractor $contractor
     * @return bool
     */
    public function delete(User $user, Contractor $contractor)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('contractor.delete')
        ){
            return true;
        }
    }
}
