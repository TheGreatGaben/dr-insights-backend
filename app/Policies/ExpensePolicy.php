<?php
namespace App\Policies;

use App\Models\User;
use App\Models\Expense;

class ExpensePolicy extends BasePolicy
{
    protected $model = Expense::class;

    protected $rolesCanDoAll = [
        "KAMI" => 1,
        "Admin" => 2,
    ];

    /**
     * Determine whether the user can view the resource.
     *
     * @param User $user
     * @param Expense|null $expense
     * @return bool
     */
    public function view(User $user, Expense $expense = null)
    {
        if(
            $user->hasAnyRole(array_merge($this->rolesCanDoAll, ["Business Owner" => 3, "Project Manager" => 4])) ||
            $user->can('project.expense')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can create resource.
     *
     * @param  User $user
     * @return bool
     */
    public function create(User $user)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('project.expense.add')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can update the resource.
     *
     * @param User $user
     * @param Expense $expense
     * @return bool
     */
    public function update(User $user, Expense $expense)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('project.expense.edit')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can delete the resource.
     *
     * @param User $user
     * @param Expense $expense
     * @return bool
     */
    public function delete(User $user, Expense $expense)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('project.expense.delete')
        ){
            return true;
        }
    }
}
