<?php

namespace App\Policies;

use App\Models\User;
use App\Models\FinanceAccountCode;

class FinanceAccountCodePolicy extends BasePolicy
{
    protected $model = FinanceAccountCode::class;

    protected $rolesCanDoAll = [
        "KAMI" => 1,
        "Admin" => 2,
    ];

    /**
     * Determine whether the user can view the resource.
     *
     * @param User $user
     * @param FinanceAccountCode|null $financeAccountCode
     * @return bool
     */
    public function view(User $user, FinanceAccountCode $financeAccountCode = null)
    {
        if(
            $user->hasAnyRole(array_merge($this->rolesCanDoAll, [
                "Business Owner" => 3,
                "Project Manager" => 4,
            ])) ||
            $user->can('finance_account_code')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can create resource.
     *
     * @param  User $user
     * @return bool
     */
    public function create(User $user)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('finance_account_code.add')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can update the resource.
     *
     * @param User $user
     * @param FinanceAccountCode $financeAccountCode
     * @return bool
     */
    public function update(User $user, FinanceAccountCode $financeAccountCode)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('finance_account_code.edit')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can delete the resource.
     *
     * @param User $user
     * @param FinanceAccountCode $financeAccountCode
     * @return bool
     */
    public function delete(User $user, FinanceAccountCode $financeAccountCode)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('finance_account_code.delete')
        ){
            return true;
        }
    }
}
