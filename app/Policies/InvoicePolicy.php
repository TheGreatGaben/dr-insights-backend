<?php
namespace App\Policies;

use App\Models\User;
use App\Models\Invoice;

class InvoicePolicy extends BasePolicy
{
    protected $model = Invoice::class;

    protected $rolesCanDoAll = [
        "KAMI" => 1,
        "Admin" => 2,
    ];

    /**
     * Determine whether the user can view the resource.
     *
     * @param User $user
     * @param Invoice|null $invoice
     * @return bool
     */
    public function view(User $user, Invoice $invoice = null)
    {
        if(
            $user->hasAnyRole(array_merge($this->rolesCanDoAll, ["Business Owner" => 3, "Project Manager" => 4])) ||
            $user->can('project.invoice')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can create resource.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('project.invoice.add')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can update the resource.
     *
     * @param User $user
     * @param Invoice $invoice
     * @return bool
     */
    public function update(User $user, Invoice $invoice)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('project.invoice.edit')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can delete the resource.
     *
     * @param  User $user
     * @param  Invoice $invoice
     * @return bool
     */
    public function delete(User $user, Invoice $invoice)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('project.invoice.delete')
        ){
            return true;
        }
    }
}
