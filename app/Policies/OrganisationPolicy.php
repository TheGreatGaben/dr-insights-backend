<?php
namespace App\Policies;

use App\Models\User;
use App\Models\Organisation;

class OrganisationPolicy extends BasePolicy
{
    protected $model = Organisation::class;

    protected $rolesCanDoAll = [
        "KAMI" => 1,
        "Admin" => 2,
        "Business Owner" => 3,
    ];

    /**
     * Determine whether the user can view the resource.
     *
     * @param User $user
     * @param Organisation|null $organisation
     * @return bool
     */
    public function view(User $user, Organisation $organisation = null)
    {
        if(
            $user->hasAnyRole(array_merge($this->rolesCanDoAll, ["Business Owner" => 3, "Project Manager" => 4])) ||
            $user->can($this->model)
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can create resource.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can($this->model . '.add')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can update the resource.
     *
     * @param User $user
     * @param Organisation $organisation
     * @return bool
     */
    public function update(User $user, Organisation $organisation = null)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can($this->model . '.edit')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can delete the resource.
     *
     * @param User $user
     * @param Organisation $organisation
     * @return bool
     */
    public function delete(User $user, Organisation $organisation)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can($this->model . '.delete')
        ){
            return true;
        }
    }
}
