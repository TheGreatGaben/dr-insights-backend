<?php
namespace App\Policies;

use App\Models\User;
use App\Models\PurchaseOrder;

class POPolicy extends BasePolicy
{
    protected $model = PurchaseOrder::class;

    protected $rolesCanDoAll = [
        "KAMI" => 1,
        "Admin" => 2,
    ];

    /**
     * Determine whether the user can view the resource.
     *
     * @param User $user
     * @return bool
     */
    public function view(User $user)
    {
        if(
            $user->hasAnyRole(array_merge($this->rolesCanDoAll, ["Business Owner" => 3, "Project Manager" => 4])) ||
            $user->can('project.po')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can create resource.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('project.po.add')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can update the resource.
     *
     * @param User $user
     * @param PurchaseOrder $po
     * @return bool
     */
    public function update(User $user, PurchaseOrder $po)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('project.po.edit')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can delete the resource.
     *
     * @param User $user
     * @param PurchaseOrder $po
     * @return bool
     */
    public function delete(User $user, PurchaseOrder $po)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('project.po.delete')
        ){
            return true;
        }
    }
}
