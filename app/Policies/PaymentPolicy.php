<?php
namespace App\Policies;

use App\Models\User;
use App\Models\Payment;

class PaymentPolicy extends BasePolicy
{
    protected $model = Payment::class;

    protected $rolesCanDoAll = [
        "KAMI" => 1,
        "Admin" => 2,
    ];

    /**
     * Determine whether the user can view the resource.
     *
     * @param User $user
     * @param Payment|null $payment
     * @return bool
     */
    public function view(User $user, Payment $payment = null)
    {
        if(
            $user->hasAnyRole(array_merge($this->rolesCanDoAll, ["Business Owner" => 3, "Project Manager" => 4])) ||
            $user->can('project.payment')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can create resource.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('project.payment.add')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can update the resource.
     *
     * @param User $user
     * @param Payment $payment
     * @return bool
     */
    public function update(User $user, Payment $payment)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('project.payment.edit')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can delete the resource.
     *
     * @param User $user
     * @param Payment $payment
     * @return bool
     */
    public function delete(User $user, Payment $payment)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('project.payment.delete')
        ){
            return true;
        }
    }
}
