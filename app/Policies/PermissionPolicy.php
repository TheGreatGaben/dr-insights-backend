<?php
namespace App\Policies;

use App\Models\User;
use App\Models\Permission;

class PermissionPolicy extends BasePolicy
{
    protected $model = Permission::class;

    protected $rolesCanDoAll = [
        "KAMI" => 1,
    ];

    /**
     * Determine whether the user can view the resource.
     *
     * @param User $user
     * @return bool
     */
    public function view(User $user)
    {
        if(
            $user->hasAnyRole(array_merge($this->rolesCanDoAll, ["Admin" => 2])) ||
            $user->can('permission')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can create resource.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('permission.add')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can update the resource.
     *
     * @param User $user
     * @param Permission $permission
     * @return bool
     */
    public function update(User $user, Permission $permission)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('permission.edit')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can delete the resource.
     *
     * @param User $user
     * @param Permission $permission
     * @return bool
     */
    public function delete(User $user, Permission $permission)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('permission.delete')
        ){
            return true;
        }
    }
}
