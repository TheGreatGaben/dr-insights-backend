<?php
namespace App\Policies;

use App\Models\User;
use App\Models\Project;

class ProjectPolicy extends BasePolicy
{
    protected $model = Project::class;

    protected $rolesCanDoAll = [
        "KAMI" => 1,
        "Admin" => 2,
    ];

    /**
     * Determine whether the user can view the resource.
     *
     * @param User $user
     * @return bool
     */
    public function view(User $user, Project $project = null)
    {
        if(
            $user->hasAnyRole(array_merge($this->rolesCanDoAll, ["Business Owner" => 3])) ||
            $user->can('project') ||
            (!$project && $user->hasRole(["Project Manager" => 4])) ||
            ($project && $user->can("project.view." . $project->id))
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can create resource.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('project.add')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can update the resource.
     *
     * @param User $user
     * @param Project $project
     * @return bool
     */
    public function update(User $user, Project $project)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('project.edit')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can delete the resource.
     *
     * @param User $user
     * @param Project $project
     * @return bool
     */
    public function delete(User $user, Project $project)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('project.delete')
        ){
            return true;
        }
    }
}
