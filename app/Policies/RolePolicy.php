<?php
namespace App\Policies;

use App\Models\User;
use App\Models\Role;

class RolePolicy extends BasePolicy
{
    protected $model = Role::class;

    protected $rolesCanDoAll = [
        "KAMI" => 1,
    ];

    /**
     * Determine whether the user can view the resource.
     *
     * @param User $user
     * @return bool
     */
    public function view(User $user)
    {
        if(
            $user->hasAnyRole(array_merge($this->rolesCanDoAll, ["Admin" => 2])) ||
            $user->can('role')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can create resource.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('role.add')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can update the resource.
     *
     * @param User $user
     * @param Role $role
     * @return bool
     */
    public function update(User $user, Role $role)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('role.edit')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can delete the resource.
     *
     * @param  \App\User $user
     * @param Role $role
     * @return bool
     */
    public function delete(User $user, Role $role)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('role.delete')
        ){
            return true;
        }
    }
}
