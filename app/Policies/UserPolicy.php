<?php
namespace App\Policies;

use App\Models\User;

class UserPolicy extends BasePolicy
{
    protected $model = User::class;

    protected $rolesCanDoAll = [
        "KAMI" => 1,
        "Admin" => 2,
    ];

    private $rolesCanImpersonate = [
        "KAMI" => 1,
    ];

    /**
     * Determine whether the user can view the resource.
     *
     * @param User $user
     * @param User|null $selected_user
     * @return bool
     */
    public function view(User $user, User $selected_user = null)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('user') ||
            (!$selected_user || ($selected_user && $user->id == $selected_user->id))
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can create resource.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('user.add')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can update the resource.
     *
     * @param User $user
     * @param User $selected_user
     * @return bool
     */
    public function update(User $user, User $selected_user = null)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('user.edit') ||
            ($selected_user && $user->id == $selected_user->id)
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can delete the resource.
     *
     * @param User $user
     * @param User $selected_user
     * @return bool
     */
    public function delete(User $user, User $selected_user)
    {
        if(
            $user->hasAnyRole($this->rolesCanDoAll) ||
            $user->can('user.delete')
        ){
            return true;
        }
    }

    /**
     * Determine whether the user can impersonate another user.
     *
     * @param User $user
     * @return bool
     */
    public function impersonate(User $user)
    {
        if(
            $user->hasAnyRole($this->rolesCanImpersonate) ||
            $user->can('user.impersonate')
        ){
            return true;
        }
    }
}
