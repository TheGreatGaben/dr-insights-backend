<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // register extra service provider if exists
        // these service provider is not class
        foreach(["Helper", "Collection", "Validation", "QueueJob"] as $fileName)
            if(file_exists($path = app_path("Providers" . DIRECTORY_SEPARATOR . $fileName . "ServiceProvider.php")))
                include($path);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
