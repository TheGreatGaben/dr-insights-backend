<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        'App\Models\User' => 'App\Policies\UserPolicy',
        'App\Models\Role' => 'App\Policies\RolePolicy',
        'App\Models\Permission' => 'App\Policies\PermissionPolicy',
        'App\Models\Client' => 'App\Policies\ClientPolicy',
        'App\Models\Contractor' => 'App\Policies\ContractorPolicy',
        'App\Models\Project' => 'App\Policies\ProjectPolicy',
        'App\Models\Invoice' => 'App\Policies\InvoicePolicy',
        'App\Models\Expense' => 'App\Policies\ExpensePolicy',
        'App\Models\Payment' => 'App\Policies\PaymentPolicy',
        'App\Models\PurchaseOrder' => 'App\Policies\POPolicy',
        'App\Models\VariationOrder' => 'App\Policies\VOPolicy',
        'App\Models\FinanceAccountCode' => 'App\Policies\FinanceAccountCodePolicy',
        'App\Models\Organisation' => 'App\Policies\OrganisationPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();
    }
}
