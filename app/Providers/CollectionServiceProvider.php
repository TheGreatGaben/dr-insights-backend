<?php
use Illuminate\Http\Request;
use \Illuminate\Support\Collection;

Collection::macro("sumAll", function($columnConditions, Request $request = null, $conditionsForAll = [], $append = null, $dp = 2){
    $result = [];
    $request = (($request)?$request:request());
    foreach($columnConditions as $column => $conditions)
        $asColumns[((count($as = explode(":", ((is_array($conditions))?$column:$conditions))) > 1)?$as[1]:$as[0])] = $as[0];
    $this->each(function($collection, $index) use (&$result, $asColumns, $columnConditions, $request, $conditionsForAll, $dp){
        if(iif($request, $conditionsForAll, $collection))
            foreach($asColumns as $asColumn => $column)
                if((!isset($columns["$column:$asColumn"])) || (isset($columns["$column:$asColumn"]) && iif($request, $columnConditions["$column:$asColumn"], $collection)))
                    $result[$asColumn] = ((isset($result[$asColumn]))?round($result[$asColumn] + $collection->$column, $dp):$collection->$column);
    });
    if($append)
        foreach($asColumns as $asColumn => $column)
            $append->$asColumn = ((isset($result[$asColumn]))?$result[$asColumn]:0);
    return (($append)?$append:$result);
});
