<?php
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;

// store all query processed into config and debug it if needed
if(config("app.debug"))
    \DB::listen(function($query){
        ((is_array($allQuery = config("all_query")))?:$allQuery = []);
        $bindings = $query->bindings;
        foreach($bindings as $index => $binding)
            if(is_object($binding))
                if(strtolower(get_class($binding)) == "datetime")
                    $bindings[$index] = $binding->format("Y-m-d H:i:s");
        $allQuery[] = vsprintf(str_replace('?', "'%s'", $query->sql), $bindings);
        config(["all_query" => $allQuery]);
    });

if(!function_exists("hasValue")){
    /**
     * check the key exists or not
     * and return value from $from.
     * Will auto check from request()
     * if $from does not set.
     *
     * @param $key
     * @param $from
     * @return mixed
     */
    function hasValue($key, $from = null){
        $from = (($from)?$from:request());
        if(is_array($from) && isset($from[$key]))
            return $from[$key];
        elseif($from instanceof Request && $from->filled($key))
            return $from->input($key);
        elseif(is_object($from) && property_exists($from, $key) && $from->$key)
            return $from->$key;
        return null;
    }
}

if(!function_exists("ifData")){
    /**
     * helping to do data checking by array
     *
     * @return bool|null
     */
    function ifData(){
        $bool = null;
        $args = func_get_args();
        if(($not = substr($args[0], 0, 1) == "!"))
            $args[0] = substr($args[0], 1);
        switch($args[0]){
            case '=':
                $bool = ($args[1] == $args[2]);break;
            case '>':
                $bool = ($args[1] > $args[2]);break;
            case '>=':
                $bool = ($args[1] >= $args[2]);break;
            case '<':
                $bool = ($args[1] < $args[2]);break;
            case '<=':
                $bool = ($args[1] <= $args[2]);break;
            case '<>':
                $bool = (isset($args[2]["start"]) && isset($args[2]["end"]) && $args[1] > $args[2]["start"] && $args[1] < $args[2]["end"]);break;
            case '<=>':
                $bool = (isset($args[2]["start"]) && isset($args[2]["end"]) && $args[1] >= $args[2]["start"] && $args[1] <= $args[2]["end"]);break;
            case '|<>':
                $bool = ((isset($args[2]["start"]) && $args[1] > $args[2]["start"]) || (isset($args[2]["end"]) && $args[1] < $args[2]["end"]));break;
            case '|<=>':
                $bool = ((isset($args[2]["start"]) && $args[1] >= $args[2]["start"]) || (isset($args[2]["end"]) && $args[1] <= $args[2]["end"]));break;
            case 'in':
                $bool = in_array($args[1], $args[2]);break;
        }
        return (($not)?!$bool:$bool);
    }
}

if(!function_exists("iif")){
    /**
     * helping to do data checking by array
     *
     * @see ifData()
     * @param Request|null $request
     * @param array $filters
     * @param null $model
     * @return bool
     */
    function iif(Request $request = null, $filters = [], $model = null){
        if(is_array($filters) && !empty($filters))
            foreach($filters as $filter){
                $original = $filter[0];
                $filter[0] = (is_string($filter[0]) && (substr($filter[0], 0, 1) == "`" && substr($filter[0], -1, 1) == "`")?substr($filter[0], 1, -1):$filter[0]);
                if(count($filter) == 2)
                    $filter[] = (($request->has($filter[0]))?$request->get($filter[0]):"");
                $filter[0] = (($original == $filter[0])?$filter[0]:$model->{$filter[0]});
                list($value1, $operator, $value2) = $filter;
                if(!ifData($operator, $value1, $value2))
                    return false;
            }
        return true;
    }
}

if(!function_exists("caseWhen")){
    /**
     * generate case when string for sql easily
     * @param $allArrays
     * @param bool $dbRaw
     * @return array
     */
    function caseWhen($allCases, $dbRaw = true){
        $results = [];
        if(is_array($allCases) && !empty($allCases)){
            foreach($allCases as $functions => $cases)
                if(preg_match("/[" . implode("|", ["count", "sum", "avg", "min", "max", "round", "abs", "ceil", "floor", "pow", "sqrt"]) . "]/i", $functions)){
                    foreach(caseWhen($cases, false) as $case){
                        list($string, $as) = explode(" AS ", $case);
                        foreach (explode(",", $functions) as $function)
                            $string = strtoupper($function) . "(" . trim($string) . ")";
                        $string .= (($as)?" AS " . trim($as):"");
                        $results[] = (($dbRaw)?\DB::raw($string):$string);
                    }
                }else{
                    $if = [];
                    if(isset($cases["when"]) && isset($cases["when"]["if"]) && isset($cases["when"]["then"]))
                        $cases["when"] = [$cases["when"]];
                    foreach($cases["when"] as $index => $ifThen){
                        if(isset($ifThen["if"]) && isset($ifThen["then"]))
                            $ifs = ((is_array($ifThen["if"])) ? $ifThen["if"]:[$ifThen["if"]]);
                            foreach ($ifs as $index => $conditions)
                                if (is_array($conditions))
                                    $ifs[$index] = "(" . implode(" OR ", $conditions) . ")";
                            $if[] = Str::replaceArray("?", [implode(" AND ", $ifs), $ifThen["then"]], "WHEN ? THEN ?");
                        }
                    if(!empty($if)){
                        $string = "(CASE " . implode(" ", $if) . ((($else = hasValue("else", $cases)) != null)?" ELSE " . $else:"") . " END)" . ((($as = hasValue("as", $cases)) != null)?" AS " . $as:"");
                        $results[] = (($dbRaw)?\DB::raw($string):$string);
                    }
                }
        }
        return $results;
    }
}
