<?php

use Illuminate\Support\Facades\Queue;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;

Queue::before(function (JobProcessing $event) {
    if($event->job->resolveName() == "App\\Notifications\\ErrorNotify"){
        // set another smpt server to send error email
        $configs = [];
        foreach(["driver", "host", "port", "username", "password", "encryption"] as $config)
            $configs["mail." . $config] = config("mail.err_" . $config);
        config($configs);
        // try search and delete all duplicate email
        $string = explode("\n", unserialize($event->job->payload()["data"]["command"])->notification->trace)[0];
        DB::table("jobs")
            ->whereRaw("`payload` LIKE '%" . str_replace(["\\", "/"], ["\\\\\\\\\\\\\\\\", "\\\\\\\\/"], $string) . "%'")
            ->delete();
    }
});

Queue::after(function (JobProcessed $event) {

});

Queue::looping(function () {

});
