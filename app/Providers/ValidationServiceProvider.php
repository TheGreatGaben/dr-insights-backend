<?php

Validator::extend('orExists', function ($attribute, $value, $parameters, $validator){
    $table = array_shift($parameters);
    $value = is_array($value)?$value:[$value];
    $query = \DB::table($table)->where(function($query) use($parameters, $value){
        foreach($parameters as $column)
            $query->orWhereIn($column, $value);
    });
    return $query->count() > 0;
});
