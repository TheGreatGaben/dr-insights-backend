<?php
namespace App\Queries\v2;

use App\traits\Searchable;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;

class BaseQuery
{
    use Searchable;

    /**
     * default model
     *
     * @var Model
     */
    protected $model;

    /**
     * default model name for query,
     * will auto load model by this value
     *
     * @var String
     */
    protected $modelName;

    /**
     * determine that any model is loaded
     * and only return data with loaded model
     *
     * @var bool
     */
    protected $modelLoaded;

    /**
     * store customize request if any
     * default will load from request()
     *
     * @var Request
     */
    protected $request;

    /**
     * store query here to get
     *
     * @var $query
     */
    protected $query;

    /**
     * customize filter array here to search
     * without via model eloquent scope method
     *
     * @var array
     */
    protected $filters = [];

    /**
     * default sort by for query
     *
     * @var $sortBy
     */
    protected $sortBy;

    /**
     * set to true will add sort by primary key at last
     * this is to prevent some data with same value
     * sequence not consistent sometimes
     *
     * @var bool
     */
    protected $sortByKey = true;

    /**
     * to do sorting in sql by some calculation
     *
     * @var array
     */
    protected $sortByFormula = [];

    /**
     * to decide return sorting data to frontend or not
     *
     * @var array
     */
    protected $returnSorting = true;

    /**
     * BaseQuery constructor.
     *
     * @param Request|null $request
     * @param Model|null $model
     */
    public function __construct(Request $request = null, Model $model = null){
        $this->request = (($request)?$request:request());
        $this->model = (($model)?$model:new $this->modelName());
    }

    /**
     * return default query to easy chain
     * with auto filter by loaded model
     *
     * @param bool $all
     * @return mixed
     */
    public function query($all = false){
        $this->modelLoaded = false;
        return $this->model->when(!$all && $this->model->id, function($query){
            $this->modelLoaded = true;
            return $query->where($this->model->getTable() . ".id", "=", $this->model->id);
        });
    }

    /**
     * return all or paginate or first only
     * of query results depend on querystring and params
     *
     * @param bool $first
     * @param bool $toArray
     * @param string $pageName
     * @return mixed
     */
    public function get($first = false, $toArray = false, $pageName = "page"){
        if($first || $this->modelLoaded){
            $result = $this->query->first();
        }else{
            $this->sorting();
            $limit = (($this->request->get("limit") > 0)?$this->request->get("limit"):null);
            $result = (($this->request->has($pageName))?$this->query->paginate($limit, ["*"], $pageName):$this->query->limit($limit)->get());
        }
        return (($toArray)?(($result instanceOf \Illuminate\Support\Collection)?$result->toArray():(array)$result):$result);
    }

    /**
     * process the query results with
     * the callback function
     *
     * @param $collection
     * @param \Closure $function
     * @return array|mixed
     */
    public function set($collection, \Closure $function){
        if(is_array($collection) && !empty($collection)){
            $collectionData = ((isset($collection["data"]))?$collection["data"]:$collection);
            foreach($collectionData as $index => $data)
                $collectionData[$index] = $function($data, $index);
            $return = ((isset($collection["data"]))?($collection["data"] = $collectionData):["data" => $collectionData]);
            return $return;
        }else{
            if($collection instanceof Collection)
                $collection->each($function);
            elseif($collection instanceof \Illuminate\Pagination\AbstractPaginator)
                $collection->getCollection()->each($function);
            else
                $function($collection, 0);
            return $collection;
        }
    }

    /**
     * will return results that already processed
     * from the callback function
     *
     * @param \Closure $function
     * @param bool $first
     * @param bool $toArray
     * @return array|mixed
     */
    public function getSet(\Closure $function, $first = false, $toArray = false){
        return $this->set($this->get($first, $toArray), $function);
    }

    /**
     * return all query results
     *
     * @param bool $toArray
     * @return mixed
     */
    public function all($toArray = false){
        $result = $this->query->get();
        return (($toArray)?$result->toArray():$result);
    }

    /**
     * return all query results that already processed
     * from the callback function
     *
     * @return mixed
     */
    public function allSet(\Closure $function){
        return $this->set($this->all(), $function);
    }

    /**
     * add searching query by using
     * App\Traits\Searchable class
     *
     * @param null $query
     * @param string $filters
     * @param bool $enable
     * @return query
     * @throws \Exception
     */
    public function search($filters = "", $query = null, $enable = true){
        return $this->searching((($query)?$query:$this->query), $this->request, ((is_array($filters))?$filters:((isset($this->filters[$filters]))?$this->filters[$filters]:[])), $enable);
    }

    /**
     * Set up sorting rules for query
     *
     * @return $this
     */
    private function sorting(){
        list($sort, $order) = $this->getSortAndOrder();
        if($sort && $order){
            if($this->returnSorting)
                config(["currentSorting" => [$sort, strtoupper($order)]]);
            $this->query = $this->query->orderBy(((isset($this->sortByFormula[$sort]))?\DB::raw("(" . $this->sortByFormula[$sort] . ")"):$sort), $order);
        }
        $this->query = $this->query->when($this->sortByKey && $this->model->getKeyName(), function($query){
            return $query->orderBy($this->model->getTable() . "." . $this->model->getKeyName(), "ASC");
        });
        return $this;
    }

    /**
     * will get query data to collection and then
     * generate pagination with sorting automatically
     * depends on querystring. This function allow
     * to process data first before sorting and paginate
     *
     * @param \Closure|null $function
     * @param null $items
     * @param string $pageName
     * @return LengthAwarePaginator
     */
    public function collect(\Closure $function = null, $items = null, $pageName = "page"){
        $page = (($this->request->has($pageName))?$this->request->get($pageName):null);
        $limit = (($this->request->has("limit"))?$this->request->get("limit"):null);
        $items = (($items)?(($items instanceof Collection)?$items:collect($items)):$this->query->get());
        if($function)
            $items = $items->each($function);
        $items = $this->cSorting($items)->values();
        if($page){
            $limit = (($limit)?$limit:15);
            $items = new LengthAwarePaginator($items->forPage($page, $limit), $items->count(), $limit, $page, [
                "pageName" => $pageName,
                "path" => $this->request->url() . "?" . http_build_query($this->request->except($pageName)),
            ]);
        }else{
            $items = $items->forPage($page, $limit);
        }
        return $items;
    }

    /**
     * Collection add sort by condition
     *
     * @param $items
     * @return mixed
     */
    private function cSorting($items){
        list($sort, $order) = $this->getSortAndOrder();
        $sortBy = "sortBy" . (($order == "desc")?"Desc":"");
        if($sort && $order && $this->returnSorting)
            config(["currentSorting" => [$sort, strtoupper($order)]]);
        return $items->$sortBy(function($item) use ($sort){
            $sortArray = [$item->{$item->getKeyName()}];
            if($sort)
                array_unshift($sortArray, $item->$sort);
            return $sortArray;
        });
    }

    /**
     * determine and return sort by column name
     * and also order by asc/desc
     *
     * @return array
     */
    private function getSortAndOrder(){
        $sort = (($this->request->has("sort") && $this->request->get("sort"))?$this->request->get("sort"):$this->sortBy);
        if(in_array($order = substr($sort, 0, 1), ["+", "-"]))
            $sort = substr($this->request->get("sort"), 1);
        return [$sort, $this->getOrder($order)];
    }

    /**
     * determine and return order by asc/desc
     *
     * @param string $order
     * @return mixed|string
     */
    private function getOrder($order = ""){
        if(in_array($order, ["+", "-"]))
            return (($order == "+")?"asc":"desc");
        return (($this->request->has("order") && $this->request->get("order"))?strtolower($this->request->get("order")):"asc");
    }

    /**
     * print out sql in string with binding values
     *
     * @return string
     */
    public function sql(){
        return vsprintf(str_replace('?', "'%s'", $this->query->toSql()), $this->query->getBindings());
    }

    /**
     * debug query result in table view
     *
     * @return string
     */
    public function table($ddTable){
        if(is_array($this->debugColumns)){
            $th = "<th>No.</th>";
            // generate table head
            foreach($ddTable as $key => $column)
                $th .= "<th>" . ((!is_int($key))?$key:$column) . "</th>";
            // generate table body
            $this->query->get()->each(function($collection, $index) use (&$rows, $ddTable){
                $td = "<td>" . ($index+1) . "</td>";
                foreach($ddTable as $key)
                    $td .= "<td>" . $collection->$key . "</td>";
                $rows .= "<tr>$td</tr>";
            });
            echo "<table><tr>$th</tr>$rows</table>";
        }
    }

    /**
     * to debug query sql in string
     * or query results in table view
     *
     * @param array $ddTable
     * @param null $query
     */
    public function dd($ddTable = [], $query = null){
        if($query)
            $this->query = $query;
        dd(((is_array($ddTable) && !empty($ddTable))?$this->table($ddTable):$this->sql()));
    }

    /**
     * to cater pages that may have more than 1 pagination
     *
     * @param $prefix
     * @param array $extra_params
     * @return array
     */
    public function prefixParams($prefix, $extra_params = []){
        $paginateData = [];
        $prefix = rtrim($prefix,"_") . "_";
        foreach(array_merge(["page", "limit", "sort", "order"], $extra_params) as $param)
            if($this->request->has("{$prefix}{$param}"))
                $paginateData[$param] = $this->request->get("{$prefix}{$param}");
        return $paginateData;
    }

    /**
     * set to don't return sorting data and allow to chain
     *
     * @return $this
     */
    public function noSort(){
        $this->returnSorting = false;
        return $this;
    }
}
