<?php
namespace App\Queries\v2;

use App\Models\BulkPayer;
use Illuminate\Http\Request;

class BulkPayerQuery extends BaseQuery
{
    /**
     * default model name for query
     *
     * @var String
     */
    protected $modelName = BulkPayer::class;

    /**
     * BulkPayerQuery constructor.
     *
     * @param Request|null $request
     * @param BulkPayer|null $payer
     */
    public function __construct(Request $request = null, BulkPayer $payer = null){
        parent::__construct($request, $payer);
    }

    /**
     * @return mixed
     */
    public function getAll(){
        $payers = $this->getAllQuery()->get();
        return $payers;
    }

    /**
     * @return $this
     */
    public function getAllQuery(){
        $this->sortBy = "bulk_payers.name";
        $this->query = $this->query()
            ->search($this->request)
        ;
        return $this;
    }
}
