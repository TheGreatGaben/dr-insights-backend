<?php
namespace App\Queries\v2;

use App\Models\Client;
use App\Models\Project;
use Illuminate\Http\Request;

class ClientQuery extends BaseQuery
{
    /**
     * default model name for query
     *
     * @var String
     */
    protected $modelName = Client::class;

    /**
     * ClientQuery constructor.
     *
     * @param Request|null $request
     * @param Client|null $client
     */
    public function __construct(Request $request = null, Client $client = null){
        parent::__construct($request, $client);
    }

    /**
     * @return mixed
     */
    public function getAll(){
        $clients = $this->getAllQuery()->get();
        return $clients;
    }

    /**
     * @return array|mixed
     */
    public function getAllWithProjectInvoices(){
        $clients = $this->getAllQuery()->getSet(function($client, $index){
            if($index == 0){
                $this->request->replace($this->prefixParams("invoice", ["filter"]));
            }
            $client->invoices = (new static($this->request, $client))->getClientsReportQuery()->get();
        });
        return $clients;
    }

    /**
     * @return $this
     */
    public function getAllQuery(){
        $this->sortBy = "clients.name";
        $this->query = $this->query()
            ->search($this->request)
            ->leftJoinProject()
            ->leftJoinInvoiceSum()
            ->addSelect($this->selectColumns())
            ->groupBy(["clients.id"])
        ;
        return $this;
    }

    /**
     * @return array
     */
    public function selectColumns(){
        $columns = [
            "clients.id",
            "clients.uuid",
            "clients.name",
        ];
        if(!$this->request->get("summary")){
            $columns = array_merge($columns, [
                "clients.address_1",
                "clients.address_2",
                "clients.postcode",
                "clients.city",
                "clients.state",
                "clients.country",
                "clients.tel_no",
                "clients.fax_no",
                "clients.person_in_charge",
                "clients.end_customer",
                \DB::raw("COUNT(DISTINCT `projects`.`id`) AS `total_project`"),
                \DB::raw("SUM(`projects`.`contract_sum`) AS `total_contract_sum`"),
                \DB::raw("SUM(`projects`.`variation_order`) AS `total_variation_order`"),
                \DB::raw("SUM(`projects`.`contract_sum` + `projects`.`variation_order`) AS `total_co_vo`"),
                \DB::raw("SUM(`invoices`.`total_invoiced`) AS `total_invoiced`"),
                \DB::raw("SUM(`invoices`.`total_received`) AS `total_received`"),
                \DB::raw("SUM(`invoices`.`total_receivable`) AS `total_receivable`"),
                \DB::raw("AVG(`invoices`.`aging`) AS `aging`"),
            ]);
        }
        return $columns;
    }

    /**
     * @return mixed
     */
    public function getClientsReport(){
        $clients = $this->getClientsReportQuery()->get();
        return $clients;
    }

    /**
     * @return $this
     */
    public function getClientsReportQuery(){
        $this->sortBy = "invoices.id";
        $this->query = Project::search($this->request, "clients_invoices")
            ->joinClient($this->model)
            ->joinInvoicesPayment()
            ->addSelect([
                "projects.id AS project_id",
                "projects.uuid AS project_uuid",
                \DB::raw("CONCAT_WS(' - ', `clients`.`end_customer`, `projects`.`name`) AS `project_name`"),
                "invoices.id",
                "invoices.uuid",
                "invoices.cert_no",
                "invoices.invoice_no",
                \DB::raw("`invoices`.`nett_certified` + `invoices`.`gst` + `invoices`.`sst` - `invoices`.`debit_note` AS `invoiced_amt`"),
                "invoices.payment_amt",
                "invoices.balance",
                "invoices.invoice_date",
                "invoices.due_date",
                \DB::raw("MAX(payments.payment_date) AS last_payment_date"),
                \DB::raw("(CASE WHEN `invoices`.`balance` > 0 THEN DATEDIFF(NOW(), `invoices`.`invoice_date`) ELSE DATEDIFF(MAX(`payments`.`payment_date`), `invoices`.`invoice_date`) END) AS `aging`"),
            ])
            ->groupBy(["clients.id", "projects.id", "invoices.id"])
        ;
        return $this;
    }
}
