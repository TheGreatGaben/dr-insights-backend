<?php
namespace App\Queries\v2;

use App\Models\Project;
use App\Models\Expense;
use App\Models\Contractor;
use Illuminate\Http\Request;

class ContractorQuery extends BaseQuery
{
    /**
     * default model name for query
     *
     * @var String
     */
    protected $modelName = Contractor::class;

    /**
     * ContractorQuery constructor.
     *
     * @param Request|null $request
     * @param Contractor|null $contractor
     */
    public function __construct(Request $request = null, Contractor $contractor = null){
        parent::__construct($request, $contractor);
    }

    /**
     * @param Project|null $project
     * @return mixed
     */
    public function getAll(Project $project = null){
        $contractors = $this->getAllQuery($project)->get();
        return $contractors;
    }

    /**
     * @return array|mixed
     */
    public function getAllContractorsWithExpenses(){
        $contractors = $this->getAllQuery()->getSet(function($contractor, $index){
            if($index == 0){
                $this->request->replace($this->prefixParams("expense", ["filter"]));
            }
            $contractor->expenses = (new static($this->request, $contractor))->getContractorsReportQuery()->get();
        });
        return $contractors;
    }

    /**
     * @param Project|null $project
     * @return $this
     */
    public function getAllQuery(Project $project = null){
        $this->sortBy = "contractors.name";
        $this->query = $this->query()
            ->search($this->request)
            ->leftJoinExpensesSum($project)
            ->leftJoinPurchaseOrderSum($project)
            ->leftJoinVariationOrderSum($project)
            ->addSelect($this->selectColumns())
            ->groupBy([
                "contractors.id",
            ])
        ;
        if($project){
            $this->query = $this->query->joinProject($project);
        }else{
            $this->query = $this->query->leftJoinProjectTotal()
                ->addSelect(["projects.total_projects"]);
        }
        return $this;
    }

    /**
     * @return array
     */
    public function selectColumns(){
        $columns = [
            "contractors.id",
            "contractors.uuid",
            "contractors.name",
        ];
        if(!$this->request->get("summary")){
            $columns = array_merge($columns, [
                "contractors.type",
                "contractors.term",
                "contractors.address_1",
                "contractors.address_2",
                "contractors.postcode",
                "contractors.city",
                "contractors.state",
                "contractors.country",
                "contractors.tel_no",
                "contractors.fax_no",
                \DB::raw("SUM(COALESCE(`purchase_orders`.`total_value`, 0)) AS `total_purchase_order`"),
                \DB::raw("SUM(COALESCE(`variation_orders`.`total_value`, 0)) AS `total_variation_order`"),
                \DB::raw("SUM(COALESCE(`purchase_orders`.`total_value`, 0)) + SUM(COALESCE(`variation_orders`.`total_value`, 0)) AS `total_amount`"),
                \DB::raw("SUM(COALESCE(`expenses`.`total_expenses`, 0)) AS `total_expenses`"),
                \DB::raw("SUM(COALESCE(`expenses`.`payment_amt`, 0)) AS `total_paid`"),
                \DB::raw("SUM(COALESCE(`expenses`.`balance`, 0)) AS `total_payable`"),
                \DB::raw("AVG(COALESCE(`expenses`.`aging`, 0)) AS `aging`"),
            ]);
        }
        return $columns;
    }

    /**
     * @return mixed
     */
    public function getContractorsReport(){
        $contractor = $this->getContractorsReportQuery()->get();
        return $contractor;
    }

    /**
     * @return $this
     */
    public function getContractorsReportQuery(){
        $this->sortBy = "expenses.id";
        $this->query = Project::search($this->request, "contractors_expenses")
            ->joinExpensesPayment()
            ->when($this->model->id, function($query){
                return $query->where("expenses.contractor_id", "=", $this->model->uuid);
            })
            ->addSelect([
                "projects.id AS project_id",
                "projects.uuid AS project_uuid",
                \DB::raw("CONCAT_WS(' - ', `projects`.`end_customer`, `projects`.`name`) AS `project_name`"),
                "expenses.id",
                "expenses.uuid",
                "expenses.ref_no",
                "expenses.invoice_date",
                "expenses.due_date",
                "expenses.date_received",
                \DB::raw("MAX(payments.payment_date) AS last_payment_date"),
                \DB::raw("`expenses`.`invoice_amt` + `expenses`.`gst` + `expenses`.`sst` AS `total_expenses`"),
                \DB::raw("`expenses`.`payment_amt` AS `total_paid`"),
                \DB::raw("`expenses`.`balance` AS `total_payable`"),
                \DB::raw("(CASE WHEN `expenses`.`balance` > 0 THEN DATEDIFF(NOW(), `expenses`.`date_received`) ELSE DATEDIFF(MAX(`payments`.`payment_date`), `expenses`.`date_received`) END) AS `aging`"),
            ])
            ->groupBy(["projects.id", "expenses.id"])
        ;
        return $this;
    }
}
