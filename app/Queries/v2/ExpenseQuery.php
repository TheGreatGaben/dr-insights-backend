<?php
namespace App\Queries\v2;

use App\Models\Project;
use App\Models\Expense;
use Illuminate\Http\Request;

class ExpenseQuery extends BaseQuery
{
    /**
     * default model name for query
     *
     * @var String
     */
    protected $modelName = Expense::class;

    /**
     * customize filter array here to search
     * without via model eloquent scope method
     *
     * @var array
     */
    protected $filters = [
        "payments" => [
            "date" => ["payment_date"]
        ],
    ];

    /**
     * ExpenseQuery constructor.
     *
     * @param Request|null $request
     * @param Expense|null $expense
     */
    public function __construct(Request $request = null, Expense $expense = null){
        parent::__construct($request, $expense);
    }

    /**
     * basic query that may reuse many
     * time within query class
     *
     * @param Project|null $project
     * @return mixed
     */
    public function basicQuery(Project $project = null){
        return parent::query()
            ->search($this->request)
            ->joinProject($project)
            ->joinOrganisation()
            ->projectManagers($this->request->get("project_manager"))
            ->joinContractor()
            ->leftJoinAccountCode()
            ->leftJoinPayment()
        ;
    }

    /**
     * get expenses overall summary data
     *
     * @return mixed
     */
    public function getSummary(){
        $expenses = $this->getSummaryQuery()->get(true, true);
        return $expenses;
    }

    /**
     * generate invoices overall summary query
     *
     * @return $this
     */
    public function getSummaryQuery(){
        $this->query = \DB::query()->fromSub($this->getSummarySubQuery()->query, 'expenses')
            ->addSelect([
                \DB::raw("COALESCE(SUM(`expenses`.`invoice_amt` + `expenses`.`dc_note` + `expenses`.`credit_note`), 0) AS `total_expenses`"),
                \DB::raw("COALESCE(SUM(`expenses`.`gst` + `expenses`.`sst`), 0) AS `total_tax`"),
                \DB::raw("COALESCE(SUM(CASE WHEN `expenses`.`invoice_amt` > 0 AND `expenses`.`payment_amt` < (`expenses`.`gst` + `expenses`.`sst`) THEN 0 ELSE `expenses`.`payment_amt` - (`expenses`.`gst` + `expenses`.`sst`) END), 0) AS `total_paid`"),
                \DB::raw("COALESCE(SUM(`expenses`.`payment_amt`), 0) AS `total_paid_include_tax`"),
                \DB::raw("COALESCE(SUM(CASE WHEN `expenses`.`balance` >= 0 THEN `expenses`.`balance` ELSE 0 END), 0) AS `total_payable`"),
                \DB::raw("COALESCE(ABS(SUM(CASE WHEN `expenses`.`balance` >= 0 THEN 0 ELSE `expenses`.`balance` END)), 0) AS `total_overpay`"),

                \DB::raw("COALESCE(SUM(CASE WHEN `expenses`.`invoice_amt` > 0 AND `expenses`.`total_paid` >= (`expenses`.`gst` + `expenses`.`sst`) THEN `expenses`.`total_paid` - (`expenses`.`gst` + `expenses`.`sst`) ELSE 0 END), 0) AS `total_paid_in_period`"),
                \DB::raw("COALESCE(SUM(`expenses`.`total_paid`), 0) AS `total_paid_include_tax_in_period`"),
                \DB::raw("COALESCE(SUM(CASE WHEN `expenses`.`invoice_amt` + `expenses`.`dc_note` + `expenses`.`credit_note` >= `expenses`.`total_paid` - (`expenses`.`gst` + `expenses`.`sst`) THEN `expenses`.`invoice_amt` + `expenses`.`dc_note` - (CASE WHEN `expenses`.`total_paid` >= (`expenses`.`gst` + `expenses`.`sst`) THEN `expenses`.`total_paid` - (`expenses`.`gst` + `expenses`.`sst`) ELSE 0 END) ELSE 0 END), 0) AS `total_payable_in_period`"),
                \DB::raw("COALESCE(ABS(SUM(CASE WHEN `expenses`.`invoice_amt` + `expenses`.`dc_note` + `expenses`.`credit_note` >= `expenses`.`total_paid` - (`expenses`.`gst` + `expenses`.`sst`) THEN 0 ELSE `expenses`.`invoice_amt` + `expenses`.`dc_note` - (CASE WHEN `expenses`.`total_paid` >= (`expenses`.`gst` + `expenses`.`sst`) THEN `expenses`.`total_paid` - (`expenses`.`gst` + `expenses`.`sst`) ELSE 0 END) END)), 0) AS `total_overpay_in_period`"),
            ]);
        return $this;
    }

    /**
     * get expenses overdue summary data
     *
     * @return mixed
     */
    public function getOverdueSummary(){
        $invoices = $this->getOverdueSummaryQuery()->get(true, true);
        return $invoices;
    }

    /**
     * generate expenses overdue summary query
     *
     * @return $this
     */
    public function getOverdueSummaryQuery(){
        $this->query = \DB::query()->fromSub($this->getSummarySubQuery()->query->overdue(), "expenses")
            ->addSelect([
                \DB::raw("COALESCE(SUM(`expenses`.`overdue`), 0) AS overdue_amt"),
                \DB::raw("COALESCE(SUM(`expenses`.`overdue_30`), 0) AS overdue_amt_30"),
                \DB::raw("COALESCE(SUM(`expenses`.`overdue_60`), 0) AS overdue_amt_60"),
                \DB::raw("COALESCE(SUM(`expenses`.`overdue_above`), 0) AS overdue_amt_above"),
            ])
        ;
        return $this;
    }

    /**
     * expesnes sub query for generate summary
     *
     * @return $this
     */
    public function getSummarySubQuery(){
        $this->query = $this->basicQuery()
            ->when($this->request->has("payment_date") && !$this->request->has("due_date"), function($query){
                return $query->whereNotNull("payments.expenses_id");
            })
            ->addSelect([
                "expenses.*",
                \DB::raw("COALESCE(SUM(`payments`.`payment_amt`), 0) AS `total_paid`"),
                \DB::raw("(CASE WHEN DATEDIFF(CURRENT_DATE(), `expenses`.`due_date`) > 0 THEN COALESCE(`expenses`.`balance`, 0) ELSE 0 END) AS `overdue`"),
                \DB::raw("(CASE WHEN DATEDIFF(CURRENT_DATE(), `expenses`.`due_date`) > 0 AND DATEDIFF(CURRENT_DATE(), `expenses`.`due_date`) <= 30 THEN COALESCE(`expenses`.`balance`, 0) ELSE 0 END) AS `overdue_30`"),
                \DB::raw("(CASE WHEN DATEDIFF(CURRENT_DATE(), `expenses`.`due_date`) > 30 AND DATEDIFF(CURRENT_DATE(), `expenses`.`due_date`) <= 60 THEN COALESCE(`expenses`.`balance`, 0) ELSE 0 END) AS `overdue_60`"),
                \DB::raw("(CASE WHEN DATEDIFF(CURRENT_DATE(), `expenses`.`due_date`) > 60 THEN COALESCE(`expenses`.`balance`, 0) ELSE 0 END) AS `overdue_above`"),
            ])
            ->groupBy(["expenses.id"])
        ;
        return $this;
    }

    /**
     * get all expenses data depend on status
     *
     * @param Project|null $project
     * @return array|mixed
     * @throws \Exception
     */
    public function getAll(Project $project = null){
        $this->sortBy = "invoice_date";
        $callback = function($expense, $index){
            $details = [];
            if($expense->payment_details){
                foreach(explode(",", $expense->payment_details) as $payment_detail){
                    list($uuid, $cheque, $date, $amount) = explode(";|", $payment_detail);
                    $details[] = [
                        "uuid" => $uuid,
                        "cheque_no" => $cheque,
                        "payment_date" => $date,
                        "payment_amt" => $amount,
                    ];
                }
            }
            $expense->payment_details = $details;
        };

        if($this->request->has("status") && $this->request->get("status") == "pending"){
            $expenses = $this->getAllPendingQuery($project)->getSet($callback);
        }else if($this->request->has("status") && $this->request->get("status") == "writeOff"){
            $expenses = $this->getAllPendingQuery($project, true)->getSet($callback);
        }else if($this->isOverdue()){
            $expenses = $this->getAllPendingQuery($project, false)->getSet($callback);
        }else{
            $expenses = $this->getAllQuery($project)->getSet($callback);
        }
        return $expenses;
    }

    /**
     * generate sql to get all expenses
     *
     * @param Project|null $project
     * @return $this
     * @throws \Exception
     */
    public function getAllQuery(Project $project = null){
        $this->query = $this->basicQuery($project)
            ->joinPurchaseOrder('left')
            ->joinVariationOrder('left')
            ->when($this->request->has("payment_date") && !$this->request->has("due_date"), function($query){
                return $query->whereNotNull("payments.expenses_id");
            })
            ->addSelect([
                "expenses.id",
                "expenses.uuid",
                "expenses.invoice_date",
                "expenses.term",
                "expenses.due_date",
                "expenses.date_received",
                "expenses.ref_no",
                "expenses.description",
                "projects.name AS project_name",
                "organisations.name as organisation_name",
                "expenses.organisation_id",
                "finance_account_codes.name AS acc_code_name",
                "finance_account_codes.uuid AS acc_code_id",
                "expenses.payable_type",
                "expenses.payable_id",
                "purchase_orders.po_no",
                "variation_orders.vo_no",
                \DB::raw("`expenses`.`invoice_amt` + `expenses`.`dc_note` + `expenses`.`credit_note` AS `total_expenses`"),
                "expenses.invoice_amt",
                "expenses.exclude_tax",
                "expenses.gst",
                "expenses.sst",
                \DB::raw("`expenses`.`gst` + `expenses`.`sst` AS `tax_amt`"),
                "expenses.dc_note",
                "expenses.credit_note",
                "expenses.balance",
                "expenses.write_off_date",
                "expenses.write_off_amt",
                "expenses.write_off_reason",
                \DB::raw("(CASE WHEN `expenses`.`invoice_amt` > 0 AND `expenses`.`payment_amt` >= (`expenses`.`gst` + `expenses`.`sst`) THEN `expenses`.`payment_amt` - (`expenses`.`gst` + `expenses`.`sst`) ELSE 0 END) AS `total_paid`"),
                "expenses.payment_amt AS total_paid_include_tax",
                \DB::raw("CASE WHEN `expenses`.`balance` > 0 THEN `expenses`.`balance` ELSE 0 END AS `total_payable`"),
                \DB::raw("ABS(CASE WHEN `expenses`.`balance` > 0 THEN 0 ELSE `expenses`.`balance` END) AS `total_overpaid`"),
                \DB::raw("(CASE WHEN `expenses`.`invoice_amt` > 0 AND COALESCE(SUM(`payments`.`payment_amt`), 0) >= (`expenses`.`gst` + `expenses`.`sst`) THEN COALESCE(SUM(`payments`.`payment_amt`), 0) - (`expenses`.`gst` + `expenses`.`sst`) ELSE 0 END) AS `total_paid_in_period`"),
                \DB::raw("COALESCE(SUM(`payments`.`payment_amt`), 0) AS `total_paid_include_tax_in_period`"),
                \DB::raw("(CASE WHEN `expenses`.`invoice_amt` + `expenses`.`dc_note` + `expenses`.`credit_note` >= (COALESCE(SUM(`payments`.`payment_amt`), 0) - `expenses`.`gst` - `expenses`.`sst`) THEN `expenses`.`invoice_amt` + `expenses`.`dc_note` - (CASE WHEN COALESCE(SUM(`payments`.`payment_amt`), 0) >= `expenses`.`gst` - `expenses`.`sst` THEN COALESCE(SUM(`payments`.`payment_amt`), 0) - `expenses`.`gst` - `expenses`.`sst` ELSE 0 END ) ELSE 0 END) AS `total_payable_in_period`"),
                \DB::raw("ABS(CASE WHEN `expenses`.`invoice_amt` + `expenses`.`dc_note` + `expenses`.`credit_note` >= COALESCE(SUM(`payments`.`payment_amt`), 0) - (`expenses`.`gst` + `expenses`.`sst`) THEN 0 ELSE `expenses`.`invoice_amt` + `expenses`.`dc_note` - (CASE WHEN COALESCE(SUM(`payments`.`payment_amt`), 0) >= (`expenses`.`gst` + `expenses`.`sst`) THEN COALESCE(SUM(`payments`.`payment_amt`), 0) - (`expenses`.`gst` + `expenses`.`sst`) ELSE 0 END) END) AS `total_overpaid_in_period`"),
                \DB::raw("GROUP_CONCAT(CONCAT(`payments`.`uuid`, ';|', COALESCE(`payments`.`cheque_no`, ''), ';|', `payments`.`payment_date`, ';|', `payments`.`payment_amt`)) as `payment_details`"),
                \DB::raw("CASE WHEN `expenses`.`due_date` >= CURRENT_DATE() THEN 0 ELSE COALESCE(DATEDIFF(CURRENT_DATE(), `expenses`.`due_date`), 0) END AS overdue"),
                "contractors.name AS contractor_name",
                "expenses.contractor_type",
                "contractors.address_1 AS contractor_address_1",
                "contractors.address_2 AS contractor_address_2",
                "contractors.postcode AS contractor_postcode",
                "contractors.city AS contractor_city",
                "contractors.state AS contractor_state",
                "contractors.country AS contractor_country",
                "contractors.tel_no AS contractor_tel_no",
                "contractors.fax_no AS contractor_fax_no",
            ])
            ->groupBy("expenses.id", "projects.id", "contractors.id", "purchase_orders.id", "variation_orders.id", "finance_account_codes.name", "organisations.name")
            ->when($this->request->has("status") && $this->request->get("status") == "paid", function($query) {
                return $query->having("total_paid_in_period", ">", 0);
            })
            ->when($this->request->has("status") && $this->request->get("status") == "overpaid", function($query) {
                return $query->having("total_overpaid_in_period", ">", 0);
            })

        ;
        $this->query = $this->search("payments");
        return $this;
    }

    /**
     * generate sql to get all pending expenses
     * or maybe writeoff expenses
     *
     * @param Project|null $project
     * @param bool $writeOff
     * @return $this
     */
    public function getAllPendingQuery(Project $project = null, $writeOff = false){
        $this->query = $this->basicQuery($project)
            ->joinPurchaseOrder('left')
            ->joinVariationOrder('left')
            ->pending($writeOff)
            ->addSelect([
                "expenses.id",
                "expenses.uuid",
                "expenses.ref_no",
                "expenses.description",
                "projects.uuid AS project_id",
                "projects.name AS project_name",
                "organisations.name AS organisation_name",
                "expenses.organisation_id",
                "finance_account_codes.name AS acc_code_name",
                "finance_account_codes.uuid AS acc_code_id",
                "expenses.contractor_id",
                "contractors.name AS contractor_name",
                "expenses.contractor_type",
                "purchase_orders.po_no",
                "variation_orders.vo_no",
                "expenses.invoice_amt",
                "expenses.dc_note",
                "expenses.credit_note",
                "expenses.gst",
                "expenses.sst",
                "expenses.write_off_date",
                "expenses.write_off_reason",
                "expenses.write_off_amt",
                \DB::raw("`expenses`.`gst` + `expenses`.`sst` AS `tax_amt`"),
                \DB::raw("(`expenses`.`invoice_amt` + `expenses`.`dc_note` + `expenses`.`credit_note`) AS total_expenses"),
                \DB::raw("GROUP_CONCAT(CONCAT(`payments`.`uuid`, ';|', COALESCE(`payments`.`cheque_no`, ''), ';|', `payments`.`payment_date`, ';|', `payments`.`payment_amt`)) as `payment_details`"),
                "expenses.payment_amt",
                "expenses.balance",
                "expenses.invoice_date",
                "expenses.term",
                "expenses.due_date",
                $this->getOverdueFormula(true),
                "expenses.date_received",
                "expenses.payment_amt AS total_paid_include_tax",
                \DB::raw("COALESCE(SUM(`payments`.`payment_amt`), 0) AS `total_paid_include_tax_in_period`"),
                \DB::raw("(CASE WHEN `expenses`.`invoice_amt` > 0 AND (COALESCE(SUM(`payments`.`payment_amt`), 0) - `expenses`.`gst` - `expenses`.`sst`) < 0 THEN 0 ELSE COALESCE(SUM(`payments`.`payment_amt`), 0) - `expenses`.`gst` - `expenses`.`sst` END) AS `total_paid_in_period`"),
                \DB::raw("(CASE WHEN `expenses`.`invoice_amt` + `expenses`.`dc_note` + `expenses`.`credit_note` >= (COALESCE(SUM(`payments`.`payment_amt`), 0) - `expenses`.`gst` - `expenses`.`sst`) THEN `expenses`.`invoice_amt` + `expenses`.`dc_note` - (CASE WHEN COALESCE(SUM(`payments`.`payment_amt`), 0) >= `expenses`.`gst` - `expenses`.`sst` THEN COALESCE(SUM(`payments`.`payment_amt`), 0) - `expenses`.`gst` - `expenses`.`sst` ELSE 0 END ) ELSE 0 END) AS `total_payable_in_period`"),
                \DB::raw("ABS(CASE WHEN `expenses`.`invoice_amt` + `expenses`.`dc_note` + `expenses`.`credit_note` >= (COALESCE(SUM(`payments`.`payment_amt`), 0) - `expenses`.`gst` - `expenses`.`sst`) THEN 0 ELSE `expenses`.`invoice_amt` + `expenses`.`dc_note` - (COALESCE(SUM(`payments`.`payment_amt`), 0) - `expenses`.`gst` - `expenses`.`sst`) END) AS `total_overpaid_in_period`"),
            ])
            ->when($this->isOverdue(), function ($query) {
                $this->getOverduePayload($query);
            })
            ->groupBy("expenses.id", "projects.id", "contractors.id", "purchase_orders.id", "variation_orders.id", "finance_account_codes.name", "organisations.name")
        ;
        return $this;
    }

    /**
     * @return bool
     */
    private function isOverdue() {
        return $this->request->has("status") && $this->request->get("status") == "overdue";
    }

    /**
     * @param bool $with_as
     * @return mixed
     */
    private function getOverdueFormula($with_as = false) {
        return \DB::raw("CASE WHEN `expenses`.`due_date` >= CURRENT_DATE() THEN 0 ELSE COALESCE(DATEDIFF(CURRENT_DATE(), `expenses`.`due_date`), 0) END" . (($with_as)?" AS overdue":""));
    }

    /**
     * @param $query
     */
    private function getOverduePayload($query){
        $overdue_start = $this->request->input("overdue.start");
        $overdue_end = $this->request->input("overdue.end");
        if($overdue_start && $overdue_end){
            $query->whereBetween($this->getOverdueFormula(), [$overdue_start, $overdue_end]);
        }elseif($overdue_start){
            $query->where($this->getOverdueFormula(), ">=", $overdue_start);
        }elseif($overdue_end){
            $query->where($this->getOverdueFormula(), "<=", $overdue_end);
        }else{
            $query->where($this->getOverdueFormula(), '>', 0);
        }
    }
}
