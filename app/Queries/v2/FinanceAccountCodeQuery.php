<?php
namespace App\Queries\v2;

use App\Models\FinanceAccountCode;
use Illuminate\Http\Request;

class FinanceAccountCodeQuery extends BaseQuery
{
    /**
     * default model name for query
     *
     * @var String
     */
    protected $modelName = FinanceAccountCode::class;

    /**
     * BulkPayerQuery constructor.
     *
     * @param Request|null $request
     * @param BulkPayer|null $payer
     */
    public function __construct(Request $request = null, FinanceAccountCode $financeAccountCode = null){
        parent::__construct($request, $financeAccountCode);
    }

    /**
     * @return mixed
     */
    public function getAll(){
        if(($parent_ids = hasValue("parent_id"))){
            $parent_ids = ((is_array($parent_ids))?$parent_ids:[$parent_ids]);
            $financeAccountCode = collect([]);
            foreach($parent_ids as $parent_id)
                if(($parent = FinanceAccountCode::findByUuid($parent_id)))
                    $financeAccountCode = $financeAccountCode->merge($this->getAllQuery($parent->id)->get()->toTree());
                return $financeAccountCode;
        }else {
            $financeAccountCode = $this->getAllQuery()->get();
            return ((hasValue("to_tree"))?$financeAccountCode->toTree():$financeAccountCode);
        }
    }

    /**
     * @param string $parent_id
     * @return $this
     */
    public function getAllQuery($parent_id = ""){
        $this->sortBy = "finance_account_codes.code";
        $this->query = $this->query()
            ->search($this->request, ["filter" => ["finance_account_codes.name", "finance_account_codes.code"]])
            ->when($parent_id, function($query) use ($parent_id){
                $query->descendantsAndSelf($parent_id);
            })
            ->when(hasValue("depth"), function($query){
                $query->withDepth();
            });
        ;
        return $this;
    }
}
