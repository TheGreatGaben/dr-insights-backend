<?php
namespace App\Queries\v2;

use App\Models\Project;
use App\Models\Invoice;
use Illuminate\Http\Request;

class InvoiceQuery extends BaseQuery
{
    /**
     * default model name for query
     *
     * @var String
     */
    protected $modelName = Invoice::class;

    /**
     * InvoiceQuery constructor.
     *
     * @param Request|null $request
     * @param Invoice|null $invoice
     */
    public function __construct(Request $request = null, Invoice $invoice = null){
        parent::__construct($request, $invoice);
    }

    /**
     * basic query that may reuse many
     * time within query class
     *
     * @param Project|null $project
     * @return mixed
     */
    public function basicQuery(Project $project = null){
        return parent::query()
            ->search($this->request)
            ->joinProject($project)
            ->joinOrganisation()
            ->projectManagers($this->request->get("project_manager"))
            ->joinClient()
            ->leftJoinAccountCode()
            ->leftJoinPayment()
        ;
    }

    /**
     * get invoices overall summary data
     *
     * @return mixed
     */
    public function getSummary(){
        $invoices = $this->getSummaryQuery()->get(true, true);
        return $invoices;
    }

    /**
     * generate invoices overall summary query
     *
     * @return $this
     */
    public function getSummaryQuery(){
        $this->query = \DB::query()->fromSub($this->getSummarySubQuery()->query, "invoices")
            ->addSelect([
                \DB::raw("COALESCE(SUM(`invoices`.`nett_certified`), 0) AS `total_invoiced`"),
                \DB::raw("COALESCE(SUM(`invoices`.`gst` + `invoices`.`sst`), 0) AS `total_tax`"),
                \DB::raw("COALESCE(SUM(CASE WHEN `invoices`.`nett_certified` > 0 AND `invoices`.`payment_amt` < (`invoices`.`gst` + `invoices`.`sst`) THEN 0 ELSE `invoices`.`payment_amt` - (`invoices`.`gst` + `invoices`.`sst`) END), 0) AS `total_received`"),
                \DB::raw("COALESCE(SUM(`invoices`.`payment_amt`), 0) AS `total_received_include_tax`"),
                \DB::raw("COALESCE(SUM(CASE WHEN `invoices`.`balance` >= 0 THEN `invoices`.`balance` ELSE 0 END), 0) AS `total_receivable`"),
                \DB::raw("COALESCE(ABS(SUM(CASE WHEN `invoices`.`balance` >= 0 THEN 0 ELSE `invoices`.`balance` END)), 0) AS `total_advance_received`"),
                \DB::raw("COALESCE(SUM(CASE WHEN `invoices`.`nett_certified` > 0 AND `invoices`.`total_paid` < (`invoices`.`gst` + `invoices`.`sst`) THEN 0 ELSE `invoices`.`total_paid` - (`invoices`.`gst` + `invoices`.`sst`) END), 0) AS `total_received_in_period`"),
                \DB::raw("COALESCE(SUM(`invoices`.`total_paid`), 0) AS `total_received_include_tax_in_period`"),
                \DB::raw("COALESCE(SUM(CASE WHEN `invoices`.`nett_certified` >= `invoices`.`total_paid` - (`invoices`.`gst` + `invoices`.`sst`) THEN `invoices`.`nett_certified` - (`invoices`.`total_paid` - (`invoices`.`gst` + `invoices`.`sst`)) ELSE 0 END), 0) AS `total_receivable_in_period`"),
                \DB::raw("COALESCE(ABS(SUM(CASE WHEN `invoices`.`nett_certified` >= `invoices`.`total_paid` - (`invoices`.`gst` + `invoices`.`sst`) THEN 0 ELSE `invoices`.`nett_certified` - (`invoices`.`total_paid` - (`invoices`.`gst` + `invoices`.`sst`)) END)), 0) AS `total_advance_received_in_period`"),
            ])
        ;
        return $this;
    }

    /**
     * get invoices overdue summary data
     *
     * @return mixed
     */
    public function getOverdueSummary(){
        $invoices = $this->getOverdueSummaryQuery()->get(true, true);
        return $invoices;
    }

    /**
     * generate invoices overdue summary query
     *
     * @return $this
     */
    public function getOverdueSummaryQuery(){
        $this->query = \DB::query()->fromSub($this->getSummarySubQuery()->query->overdue(), "invoices")
            ->addSelect([
                \DB::raw("COALESCE(SUM(`invoices`.`overdue`), 0) AS overdue_amt"),
                \DB::raw("COALESCE(SUM(`invoices`.`overdue_30`), 0) AS overdue_amt_30"),
                \DB::raw("COALESCE(SUM(`invoices`.`overdue_60`), 0) AS overdue_amt_60"),
                \DB::raw("COALESCE(SUM(`invoices`.`overdue_above`), 0) AS overdue_amt_above"),
            ])
        ;
        return $this;
    }

    /**
     * invoices sub query for generate summary
     *
     * @return $this
     */
    public function getSummarySubQuery(){
        $this->query = $this->basicQuery()
            ->when($this->request->has("payment_date") && !$this->request->has("due_date"), function($query){
                return $query->whereNotNull("payments.invoice_id");
            })
            ->addSelect([
                "invoices.*",
                \DB::raw("SUM(COALESCE(`payments`.`payment_amt`, 0)) AS `total_paid`"),
                \DB::raw("(CASE WHEN DATEDIFF(CURRENT_DATE(), `invoices`.`due_date`) > 0 THEN COALESCE(`invoices`.`balance`, 0) ELSE 0 END) AS `overdue`"),
                \DB::raw("(CASE WHEN DATEDIFF(CURRENT_DATE(), `invoices`.`due_date`) > 0 AND DATEDIFF(CURRENT_DATE(), `invoices`.`due_date`) <= 30 THEN COALESCE(`invoices`.`balance`, 0) ELSE 0 END) AS `overdue_30`"),
                \DB::raw("(CASE WHEN DATEDIFF(CURRENT_DATE(), `invoices`.`due_date`) > 30 AND DATEDIFF(CURRENT_DATE(), `invoices`.`due_date`) <= 60 THEN COALESCE(`invoices`.`balance`, 0) ELSE 0 END) AS `overdue_60`"),
                \DB::raw("(CASE WHEN DATEDIFF(CURRENT_DATE(), `invoices`.`due_date`) > 60 THEN COALESCE(`invoices`.`balance`, 0) ELSE 0 END) AS `overdue_above`"),
            ])
            ->groupBy(["invoices.id"])
        ;
        return $this;
    }

    /**
     * get all invoices data depend on status
     *
     * @param Project|null $project
     * @return array|mixed
     */
    public function getAll(Project $project = null){
        $this->sortBy = "invoice_date";
        $callback = function($invoice, $index){
            $details = [];
            if($invoice->payment_details){
                foreach (explode(",", $invoice->payment_details) as $payment_detail) {
                    list($uuid, $cheque, $date, $amount) = explode(";|", $payment_detail);
                    $details[] = [
                        "uuid" => $uuid,
                        "cheque_no" => $cheque,
                        "payment_date" => $date,
                        "payment_amt" => $amount,
                    ];
                }
            }
            $invoice->payment_details = $details;
        };

        if($this->request->has("status") && $this->request->get("status") == "pending"){
            $invoices = $this->getAllPendingQuery($project)->getSet($callback);
        }else if($this->request->has("status") && $this->request->get("status") == "writeOff"){
            $invoices = $this->getAllPendingQuery($project, true)->getSet($callback);
        }else if($this->isOverdue()){
            $invoices = $this->getAllPendingQuery($project, false)->getSet($callback);
        }else{
            $invoices = $this->getAllQuery($project)->getSet($callback);
        }
        return $invoices;
    }

    /**
     * generate sql to get all invoices
     *
     * @param Project|null $project
     * @return $this
     */
    public function getAllQuery(Project $project = null){
        $this->query = $this->basicQuery($project)
            ->when($this->request->has("payment_date") && !$this->request->has("due_date"), function($query){
                return $query->whereNotNull("payments.invoice_id");
            })
            ->addSelect([
                "invoices.id",
                "invoices.uuid",
                "projects.uuid AS project_uuid",
                "projects.name AS project_name",
                "organisations.name AS organisation_name",
                "invoices.organisation_id",
                "clients.uuid AS client_uuid",
                "clients.name AS client_name",
                "finance_account_codes.name AS acc_code_name",
                "finance_account_codes.uuid AS acc_code_id",
                "invoices.cert_no",
                "invoices.invoice_no",
                "invoices.description",
                "invoices.prev_certified",
                "invoices.nett_certified as total_invoiced",
                "invoices.work_done_certified",
                "invoices.vo",
                "invoices.retention",
                "invoices.retention_released",
                "invoices.debit_note",
                "invoices.exclude_tax",
                "invoices.gst",
                "invoices.sst",
                "invoices.write_off_date",
                "invoices.write_off_amt",
                "invoices.write_off_reason",
                \DB::raw("`invoices`.`gst` + `invoices`.`sst` AS `tax_amt`"),
                "invoices.payment_amt AS total_received_include_tax",
                \DB::raw("(CASE WHEN `invoices`.`nett_certified` > 0 AND `invoices`.`payment_amt` < (`invoices`.`gst` + `invoices`.`sst`) THEN 0 ELSE `invoices`.`payment_amt` - (`invoices`.`gst` + `invoices`.`sst`) END) AS `total_received`"),
                \DB::raw("CASE WHEN `invoices`.`balance` > 0 THEN `invoices`.`balance` ELSE 0 END AS `total_receivable`"),
                \DB::raw("ABS(CASE WHEN `invoices`.`balance` > 0 THEN 0 ELSE `invoices`.`balance` END) AS `total_advance_received`"),
                \DB::raw("(CASE WHEN `invoices`.`nett_certified` > 0 AND COALESCE(SUM(`payments`.`payment_amt`), 0) < (`invoices`.`gst` + `invoices`.`sst`) THEN 0 ELSE COALESCE(SUM(`payments`.`payment_amt`), 0) - (`invoices`.`gst` + `invoices`.`sst`) END) AS `total_received_in_period`"),
                \DB::raw("(CASE WHEN `invoices`.`nett_certified` >= COALESCE(SUM(`payments`.`payment_amt`), 0) - (`invoices`.`gst` + `invoices`.`sst`) THEN `invoices`.`nett_certified` - (COALESCE(SUM(`payments`.`payment_amt`), 0) - (`invoices`.`gst` + `invoices`.`sst`)) ELSE 0 END) AS `total_receivable_in_period`"),
                \DB::raw("ABS(CASE WHEN `invoices`.`nett_certified` >= COALESCE(SUM(`payments`.`payment_amt`), 0) - (`invoices`.`gst` + `invoices`.`sst`) THEN 0 ELSE `invoices`.`nett_certified` - (COALESCE(SUM(`payments`.`payment_amt`), 0) - `invoices`.`gst` - `invoices`.`sst`) END) AS `total_advance_received_in_period`"),
                // \DB::raw("CASE WHEN `invoices`.`nett_certified` >= (SUM(COALESCE(`payments`.`payment_amt`, 0)) - `invoices`.`gst` - `invoices`.`sst`) THEN 0 ELSE `invoices`.`nett_certified` - (SUM(COALESCE(`payments`.`payment_amt`, 0)) - `invoices`.`gst` - `invoices`.`sst`) END AS `total_advance_received_in_period`"),
                "invoices.invoice_date",
                "invoices.due_date",
                \DB::raw("GROUP_CONCAT(CONCAT(`payments`.`uuid`, ';|', COALESCE(`payments`.`cheque_no`, ''), ';|', `payments`.`payment_date`, ';|', `payments`.`payment_amt`)) as `payment_details`"),
            ])
            ->groupBy(\DB::raw("`projects`.`name`, `invoices`.`id`, `clients`.`name`, `finance_account_codes`.`name`, `organisations`.`name`"))
            ->when($this->request->has("status") && $this->request->get("status") == "received", function($query) {
                return $query->having("total_received_in_period", ">", 0);
            })
            ->when($this->request->has("status") && $this->request->get("status") == "advance", function($query) {
                return $query->having("total_advance_received_in_period", ">", 0);
            })
        ;
        return $this;
    }

    /**
     * generate sql to get all pending invoices
     * or maybe writeoff invoices
     *
     * @param Project|null $project
     * @param bool $writeOff
     * @return $this
     */
    public function getAllPendingQuery(Project $project = null, $writeOff = false){
        $this->query = $this->basicQuery($project)
            ->pending($writeOff)
            ->addSelect([
                "invoices.id",
                "invoices.uuid",
                "projects.uuid AS project_uuid",
                "projects.name AS project_name",
                "organisations.name AS organisation_name",
                "invoices.organisation_id",
                "clients.uuid AS client_uuid",
                "clients.name AS client_name",
                "finance_account_codes.name AS acc_code_name",
                "finance_account_codes.uuid AS acc_code_id",
                "invoices.cert_no",
                "invoices.invoice_no",
                "invoices.description",
                "invoices.prev_certified",
                "invoices.nett_certified as total_invoiced",
                "invoices.work_done_certified",
                "invoices.debit_note",
                "invoices.vo",
                "invoices.retention",
                "invoices.retention_released",
                "invoices.exclude_tax",
                "invoices.gst",
                "invoices.sst",
                "invoices.write_off_date",
                "invoices.write_off_reason",
                "invoices.write_off_amt",
                \DB::raw("`invoices`.`gst` + `invoices`.`sst` AS `tax_amt`"),
                "invoices.payment_amt AS total_received_include_tax",
                \DB::raw("(CASE WHEN (`invoices`.`payment_amt` - `invoices`.`gst` - `invoices`.`sst`) < 0 THEN 0 ELSE `invoices`.`payment_amt` - `invoices`.`gst` - `invoices`.`sst` END) AS `total_received`"),
                \DB::raw("CASE WHEN `invoices`.`balance` > 0 THEN `invoices`.`balance` ELSE 0 END AS `total_receivable`"),
                \DB::raw("ABS(CASE WHEN `invoices`.`balance` > 0 THEN 0 ELSE `invoices`.`balance` END) AS `total_advance_received`"),
                \DB::raw("GROUP_CONCAT(CONCAT(`payments`.`uuid`, ';|', COALESCE(`payments`.`cheque_no`, ''), ';|', `payments`.`payment_date`, ';|', `payments`.`payment_amt`)) as `payment_details`"),
                "invoices.invoice_date",
                "invoices.due_date",
                $this->getOverdueFormula(true),
                \DB::raw("(CASE WHEN `invoices`.`nett_certified` > 0 AND (COALESCE(SUM(`payments`.`payment_amt`), 0) - `invoices`.`gst` - `invoices`.`sst` < 0) THEN 0 ELSE COALESCE(SUM(`payments`.`payment_amt`), 0) - `invoices`.`gst` - `invoices`.`sst` END) AS `total_received_in_period`"),
                \DB::raw("(CASE WHEN `invoices`.`nett_certified` >= (COALESCE(SUM(`payments`.`payment_amt`), 0) - `invoices`.`gst` - `invoices`.`sst`) THEN `invoices`.`nett_certified` - (CASE WHEN COALESCE(SUM(`payments`.`payment_amt`), 0) >= (`invoices`.`gst` + `invoices`.`sst`) THEN COALESCE(SUM(`payments`.`payment_amt`), 0) - (`invoices`.`gst` + `invoices`.`sst`) ELSE 0 END) ELSE 0 END) AS `total_receivable_in_period`"),
                \DB::raw("ABS(CASE WHEN `invoices`.`nett_certified` >= (COALESCE(SUM(`payments`.`payment_amt`), 0) - `invoices`.`gst` - `invoices`.`sst`) THEN 0 ELSE `invoices`.`nett_certified` - (CASE WHEN COALESCE(SUM(`payments`.`payment_amt`), 0) >= (`invoices`.`gst` + `invoices`.`sst`) THEN COALESCE(SUM(`payments`.`payment_amt`), 0) - (`invoices`.`gst` + `invoices`.`sst`) ELSE 0 END) END) AS `total_advance_received_in_period`"),
            ])
            ->when($this->isOverdue(), function ($query) {
                $this->getOverduePayload($query);
            })
            ->groupBy(\DB::raw("`projects`.`name`, `invoices`.`id`, `clients`.`name`, `finance_account_codes`.`name`, `organisations`.`name`"));
        return $this;
    }

    /**
     * @return bool
     */
    private function isOverdue() {
        return $this->request->has("status") && $this->request->get("status") == "overdue";
    }

    /**
     * @param bool $withAs
     * @return mixed
     */
    private function getOverdueFormula($withAs = false) {
        return \DB::raw("CASE WHEN `invoices`.`due_date` >= CURRENT_DATE() THEN 0 ELSE COALESCE(DATEDIFF(CURRENT_DATE(), `invoices`.`due_date`), 0) END" . (($withAs)?" AS overdue":""));
    }

    /**
     * @param $query
     */
    private function getOverduePayload($query) {
        $overdue_start = $this->request->input("overdue.start");
        $overdue_end = $this->request->input("overdue.end");
        if($overdue_start && $overdue_end){
            $query->whereBetween($this->getOverdueFormula(), [$overdue_start, $overdue_end]);
        }elseif($overdue_start){
            $query->where($this->getOverdueFormula(), ">=", $overdue_start);
        }elseif($overdue_end){
            $query->where($this->getOverdueFormula(), "<=", $overdue_end);
        }else{
            $query->where($this->getOverdueFormula(), '>', 0);
        }
    }
}
