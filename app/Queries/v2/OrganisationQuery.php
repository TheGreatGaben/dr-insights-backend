<?php
namespace App\Queries\v2;

use App\Models\Organisation;
use Illuminate\Http\Request;

class OrganisationQuery extends BaseQuery
{
    /**
     * default model name for query
     *
     * @var String
     */
    protected $modelName = Organisation::class;

    /**
     * OrganisationQuery constructor.
     *
     * @param Request|null $request
     * @param Organisation|null $organisation
     */
    public function __construct(Request $request = null, Organisation $organisation = null){
        parent::__construct($request, $organisation);
    }

    /**
     * @return mixed
     */
    public function getAllOrganisation(){
        $organisations = $this->getAllOrganisationQuery()->get();
        return $organisations;
    }

    /**
     * @return $this
     */
    public function getAllOrganisationQuery(){
        $this->sortBy = "organisations.name";
        $this->query = $this->query()
            ->search($this->request)
            ->addSelect([
                "organisations.id",
                "organisations.uuid",
                "organisations.name",
            ])
        ;
        return $this;
    }
}
