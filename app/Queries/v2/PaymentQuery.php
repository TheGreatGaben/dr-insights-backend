<?php
namespace App\Queries\v2;

use App\Models\Payment;
use App\Models\Project;
use Illuminate\Http\Request;

class PaymentQuery extends BaseQuery
{
    /**
     * default model name for query
     *
     * @var String
     */
    protected $modelName = Payment::class;

    /**
     * store the current payments cashflow type
     *
     * @var String
     */
    protected $cashflow;

    /**
     * to do sorting in sql by some calculation
     *
     * @var array
     */
    protected $sortByFormula = [
        "credit" => "(CASE WHEN `payments`.`cash_flow` = 'IN' THEN `payments`.`payment_amt` ELSE 0 END)",
        "debit" => "(CASE WHEN `payments`.`cash_flow` = 'OUT' THEN `payments`.`payment_amt` ELSE 0 END)",
        "cert_or_ref_no" => "(CASE WHEN `payments`.`cash_flow` = 'IN' THEN `invoices`.`cert_no` ELSE `expenses`.`ref_no` END)",
        "client_or_contractor_name" => "(CASE WHEN `payments`.`cash_flow` = 'IN' THEN `clients`.`name` ELSE `contractors`.`name` END)",
    ];

    /**
     * customize filter array here to search
     * without via model eloquent scope method
     *
     * @var array
     */
    protected $filters = [
        "payments" => [
            "date" => ["payment_date"]
        ],
        "invoices" => [
            "date" => ["due_date"]
        ],
        "expenses" => [
            "date" => ["due_date"]
        ],
    ];

    /**
     * @param Request|null $request
     * @param Payment|null $payment
     */
    public function __construct(Request $request = null, Payment $payment = null){
        parent::__construct($request, $payment);

        if($request->has("cashflow") && in_array(strtolower($request->cashflow), ["in", "out"])){
            $this->cashflow = strtolower($request->cashflow);
        }
    }

    /**
     * override parent query
     *
     * @param bool $all
     * @return mixed
     */
    public function query($all = false){
        return parent::query()->when($this->cashflow, function($query){
            return $query->where("payments.cash_flow", "=", $this->cashflow);
        });
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getSummary(){
        $payments = $this->getSummaryQuery()->get(true);
        return $payments;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function getSummaryQuery(){
        $this->query = \DB::query()->fromSub($this->subSumPaymentQuery(), "payments")
            ->joinProject()
            ->leftJoinInvoice()
            ->leftJoinExpense()
            ->addSelect([
                \DB::raw("SUM(`payments`.`payment_amt` - `invoices`.`gst` - `invoices`.`sst`) AS cashflow_in"),
                \DB::raw("SUM(`payments`.`payment_amt` - `expenses`.`gst` - `expenses`.`sst`) AS cashflow_out"),
            ])
        ;
        return $this;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function subSumPaymentQuery(){
        return $this->search(\DB::table("payments")
            ->whereNull("payments.deleted_at")
            ->addSelect([
                "payments.project_id",
                "payments.cash_flow",
                "payments.invoice_id",
                "payments.expenses_id",
                \DB::raw("SUM(payments.payment_amt) AS payment_amt"),
            ])
            ->groupBy(["payments.project_id", "payments.cash_flow", "payments.invoice_id", "payments.expenses_id"])
        , "payments");
    }

    /**
     * @param Project|null $project
     * @return mixed
     */
    public function getAll(Project $project = null){
        $payments = $this->getAllQuery($project)->get();
        return $payments;
    }

    /**
     * @param Project|null $project
     * @return $this
     */
    public function getAllQuery(Project $project = null){
        $this->query = $this->query()
            ->search($this->request)
            ->joinProject($project)
            ->projectManagers($this->request->get("project_manager"))
            ->leftJoinInvoice()
            ->leftJoinExpense()
            ->countPayment($project)
            ->addSelect([
                "projects.id AS project_id",
                "projects.uuid AS project_uuid",
                "projects.name AS project_name",
                "payments.id",
                "payments.uuid",
                "payments.invoice_id",
                "payments.expenses_id",
                "payments.cash_flow",
                "payments.cheque_no",
                "count_payments.total",
                \DB::raw("CASE WHEN `payments`.`payment_amt` < 0 OR `payments`.`payment_amt` >= ((CASE WHEN `payments`.`cash_flow` = 'IN' THEN (`invoices`.`gst` + invoices.`sst`) ELSE (`expenses`.`gst` + expenses.`sst`) END) / count_payments.total) THEN `payments`.`payment_amt` - ((CASE WHEN `payments`.`cash_flow` = 'IN' THEN (`invoices`.`gst` + invoices.`sst`) ELSE (`expenses`.`gst` + expenses.`sst`) END) / count_payments.total) ELSE 0 END AS `payment_amt`"),
                \DB::raw("COALESCE((CASE WHEN `payments`.`cash_flow` = 'IN' THEN (`invoices`.`gst` + invoices.`sst`) ELSE (`expenses`.`gst` + expenses.`sst`) END), 0) AS `tax_amt`"),
                "payments.payment_amt AS payment_amt_with_tax",
                "payments.payment_date",
                \DB::raw("CASE WHEN `payments`.`cash_flow` = 'IN' THEN `invoices`.`cert_no` ELSE `expenses`.`ref_no` END AS `cert_no`"),
                \DB::raw("CASE WHEN `payments`.`cash_flow` = 'IN' THEN `invoices`.`due_date` ELSE `expenses`.`due_date` END AS `due_date`"),
                "clients.name AS client_name",
                "contractors.name AS supplier_name",
            ])
            ->groupBy(["payments.id","projects.id","invoices.id","clients.id","expenses.id","contractors.id","count_payments.invoice_id", "count_payments.expenses_id"])
        ;
        return $this;
    }
}
