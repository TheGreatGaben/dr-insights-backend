<?php
namespace App\Queries\v2;

use App\Models\Payment;
use App\Models\PaymentRecord;
use Illuminate\Http\Request;

class PaymentRecordQuery extends BaseQuery
{
    /**
     * default model name for query
     *
     * @var String
     */
    protected $modelName = PaymentRecord::class;

    /**
     * PaymentRecordQuery constructor.
     *
     * @param Request|null $request
     * @param PaymentRecord|null $paymentRecord
     */
    public function __construct(Request $request = null, PaymentRecord $paymentRecord = null){
        parent::__construct($request, $paymentRecord);
    }

    /**
     * @return mixed
     */
    public function getAll(){
        $paymentRecords = $this->getAllQuery()->getSet(function($paymentRecord, $index){
            $paymentRecord->payments = $this->getBulkPaymentsQuery($paymentRecord->id)->all(true);
            $paymentRecord->payment_count = count($paymentRecord->payments);
        });
        return $paymentRecords;
    }

    /**
     * @return $this
     */
    public function getAllQuery(){
        $this->sortBy = "payment_records.id";
        $this->query = $this->query()
            ->search($this->request)
            ->join('bulk_payers', 'bulk_payers.id', '=', 'payment_records.payer_id')
            ->whereNull('bulk_payers.deleted_at')
            ->addSelect([
                "payment_records.*",
                "bulk_payers.payer_name",
            ])
        ;
        return $this;
    }

    /**
     * @param $paymentRecordID
     * @return $this
     */
    public function getBulkPaymentsQuery($paymentRecordID){
        $this->sortBy = "projects.name";
        $this->query = Payment::search($this->request)
            ->withTrashed()
            ->join('expenses', 'expenses.uuid', '=', 'payments.expenses_id')
            ->join('contractors', 'expenses.contractor_id', '=', 'contractors.uuid')
            ->join('projects', 'projects.uuid', '=', 'payments.project_id')
            // ->whereNull('expenses.deleted_at')
            // ->whereNull('projects.deleted_at')
            ->where('payments.batch_no', $paymentRecordID)
            ->addSelect(
                'projects.name as project_name',
                'contractors.name AS contractor',
                'expenses.invoice_date',
                'expenses.date_received',
                'expenses.ref_no',
                'expenses.invoice_amt',
                'payments.*'
            )
        ;
        return $this;
    }
}
