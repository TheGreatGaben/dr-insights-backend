<?php
namespace App\Queries\v2;

use App\Models\Permission;
use Illuminate\Http\Request;

class PermissionQuery extends BaseQuery
{
    /**
     * @var Permission
     */
    protected $modelName = Permission::class;

    /**
     * PermissionQuery constructor.
     *
     * @param Request|null $request
     * @param Permission $permission
     */
    public function __construct(Request $request = null, Permission $permission = null){
        parent::__construct($request, $permission);
    }

    /**
     * Return all general permissions
     *
     * @param bool $excludeProject
     * @return mixed
     */
    public function getAllPermission($excludeProject = true){
        return $this->getAllPermissionQuery($excludeProject)->get();
    }

    /**
     * @param bool $excludeProject
     * @return $this
     */
    public function getAllPermissionQuery($excludeProject = true){
        $this->sortBy = "permissions.name";
        $this->query = $this->query()
            ->search($this->request)
            ->when($excludeProject, function($query){
                return $query->where("permissions.name", "NOT LIKE", "project.view.%");
            })
            ->addSelect([
                "permissions.id",
                "permissions.name",
            ])
        ;
        return $this;
    }

    /**
     * Return all roles that have the specific permission
     *
     * @param Permission $permission
     * @return \Illuminate\Database\Eloquent\Collection|Role[]
     */
    public function getPermissionRole(){
        $permissions = $this->getPermissionRoleQuery()->get();
        return $permissions;
    }

    /**
     * @return $this
     */
    public function getPermissionRoleQuery(){
        $this->sortBy = "permissions.name";
        $this->query = $this->query()
            ->search($this->request)
            ->join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->join("roles","role_has_permissions.role_id","=","roles.id")
            ->addSelect([
                "permissions.id",
                "permissions.id",
                "roles.id AS role_id",
                "roles.name AS role_name",
            ]);
        ;
        return $this;
    }

    /**
     * Return all permissions for project
     *
     * @return mixed
     */
    public function getAllProjectPermissions(){
        $permissions = $this->getAllProjectPermissionsQuery()->get();
        return $permissions;
    }

    /**
     * @return $this
     */
    public function getAllProjectPermissionsQuery(){
        $this->sortBy = "permissions.name";
        $this->query = $this->query()
            ->search($this->request)
            ->join("projects", \DB::raw("CONCAT('project.view.', `projects`.`id`)"), "=", "permissions.name")
            ->whereNull("projects.deleted_at")
            ->where("permissions.name", "LIKE", "project.view.%")
            ->addSelect([
                "permissions.id AS id",
                "permissions.name AS name",
                "projects.id AS project_id",
                "projects.uuid AS project_uuid",
                "projects.name AS project_name"
            ])
        ;
        return $this;
    }
}
