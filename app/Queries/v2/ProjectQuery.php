<?php
namespace App\Queries\v2;

use App\Models\User;
use App\Models\Project;
use Illuminate\Http\Request;

class ProjectQuery extends BaseQuery
{
    /**
     * default model name for query
     *
     * @var String
     */
    protected $modelName = Project::class;

    /**
     * to do sorting in sql by some calculation
     *
     * @var array
     */
    protected $sortByFormula = [
        "current_duration" => "COALESCE(SUM(DISTINCT `overall_ie`.`total_invoiced`), 0) / `contract_sum_vo_retention` * 100",
        "progress_diff" => "(CASE WHEN `contract_sum_vo_retention` > 0 THEN (COALESCE(SUM(DISTINCT `overall_ie`.`total_invoiced`), 0) / `contract_sum_vo_retention` * 100) ELSE 0 END) - (CASE WHEN `duration_la` > 100 THEN 100 ELSE `duration_la` END)",
        "profit_margin" => "(CASE WHEN COALESCE(SUM(DISTINCT `overall_ie`.`total_invoiced`), 0) > 0 THEN (COALESCE(SUM(DISTINCT `overall_ie`.`total_invoiced`), 0) - COALESCE(SUM(DISTINCT `overall_ie`.`total_expenses`), 0)) / COALESCE(SUM(DISTINCT `overall_ie`.`total_invoiced`), 0) * 100 ELSE 0 END)",
        "work_certified_margin" => "(CASE WHEN `contract_sum_and_vo` > 0 THEN COALESCE(SUM(DISTINCT `overall_ie`.`total_invoiced`), 0) / `contract_sum_and_vo` * 100 ELSE 0 END)",
        "current_margin" => "(`projects`.`contract_sum` - `projects`.`budget`) / `contract_sum_vo_retention` * 100",
        "overall_cashflow_margin" => "(CASE WHEN COALESCE(SUM(DISTINCT `overall_ie`.`total_received`), 0) > 0 THEN (COALESCE(SUM(DISTINCT `overall_ie`.`total_received`), 0) - COALESCE(SUM(DISTINCT `overall_ie`.`total_paid`), 0)) / COALESCE(SUM(DISTINCT `overall_ie`.`total_received`), 0) * 100 ELSE 0 END)",
        "cashflow_margin" => "(CASE WHEN COALESCE(SUM(DISTINCT `ie`.`total_received`), 0) > 0 THEN (COALESCE(SUM(DISTINCT `ie`.`total_received`), 0) - COALESCE(SUM(DISTINCT `ie`.`total_paid`), 0)) / COALESCE(SUM(DISTINCT `ie`.`total_received`), 0) * 100 ELSE 0 END)",
        "cashflow_margin_in_period" => "(CASE WHEN COALESCE(SUM(DISTINCT `ie`.`total_received_in_period`), 0) > 0 THEN (COALESCE(SUM(DISTINCT `ie`.`total_received_in_period`), 0) - COALESCE(SUM(DISTINCT `ie`.`total_paid_in_period`), 0)) / COALESCE(SUM(DISTINCT `ie`.`total_received_in_period`), 0) * 100 ELSE 0 END)",
    ];

    /**
     * ProjectQuery constructor.
     *
     * @param Request|null $request
     * @param Project|null $project
     */
    public function __construct(Request $request = null, Project $project = null){
        parent::__construct($request, $project);
    }

    /**
     * @return mixed
     */
    public function getSummary(){
        $projects = $this->getSummaryQuery()->get(true);
        return $projects;
    }

    /**
     * @return $this
     */
    public function getSummaryQuery(){
        $this->query = $this->query()
            ->search($this->request)
            ->projectManagers($this->request->get("project_manager"))
            ->joinClient()
            ->joinInvoiceExpensesUnion()
            ->addSelect([
                \DB::raw("SUM(`projects`.`contract_sum`) AS `contract_sum`"),
                \DB::raw("SUM(`projects`.`variation_order`) AS `variation_order`"),
                \DB::raw("SUM(`projects`.`retention`) AS `retention`"),
                \DB::raw("SUM(`ie`.`total_invoiced`) AS `total_invoiced`"),
                \DB::raw("SUM(`ie`.`invoices_tax_amt`) AS `total_invoices_tax`"),
                \DB::raw("SUM(`ie`.`total_received`) AS `total_received`"),
                \DB::raw("SUM(`ie`.`total_received_include_tax`) AS `total_received_include_tax`"),
                \DB::raw("SUM(`ie`.`total_receivable`) AS `total_receivable`"),
                \DB::raw("SUM(`ie`.`total_advance_received`) AS `total_advance_received`"),
                \DB::raw("SUM(`ie`.`total_expenses`) AS `total_expenses`"),
                \DB::raw("SUM(`ie`.`expenses_tax_amt`) AS `total_expenses_tax`"),
                \DB::raw("SUM(`ie`.`total_paid`) AS `total_paid`"),
                \DB::raw("SUM(`ie`.`total_paid_include_tax`) AS `total_paid_include_tax`"),
                \DB::raw("SUM(`ie`.`total_payable`) AS `total_payable`"),
                \DB::raw("SUM(`ie`.`total_overpay`) AS `total_overpay`"),
                \DB::raw("SUM(`ie`.`cashflow_in_30` - `ie`.`cashflow_out_30`) AS `cashflow_30`"),
                \DB::raw("SUM(`ie`.`cashflow_in_60` - `ie`.`cashflow_out_60`) AS `cashflow_60`"),
                \DB::raw("SUM(`ie`.`cashflow_in_90` - `ie`.`cashflow_out_90`) AS `cashflow_90`"),
            ])
        ;
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getAll(){
        $now = \Carbon\Carbon::now();
        $months = [];
        $no_of_months = (int)hasValue("no_of_months");
        for($i = (($no_of_months)?$no_of_months:1) ; $i > 0 ; $i--) {
            $currentMonth = $now->copy()->subMonthNoOverflow($i);
            $months[] = strtolower($currentMonth->format("M"));
        }
        $projects = $this->getAllQuery()->getSet(function($project, $index) use ($months){
            $project->current_margin = $project->current_margin;
            $project->duration_la = $project->duration_la;
            $project->projected_margin = ((($project->contract_sum + $project->variation_order) > 0)?round(($project->contract_sum + $project->variation_order - $project->budget) / ($project->contract_sum + $project->variation_order) * 100, 2):0);
            $project->profit_margin = ($project->overall_invoiced > 0)?round(($project->overall_invoiced - $project->overall_expenses) / $project->overall_invoiced * 100, 2):0;
            $project->current_duration = ((($project->contract_sum_vo_retention) > 0)?round($project->overall_invoiced / ($project->contract_sum_vo_retention) * 100, 2):0);
            $project->progress_diff = round($project->current_duration - $project->duration_la, 2);
            $project->work_certified_margin = ((($project->contract_sum_and_vo) > 0)?round($project->overall_invoiced / $project->contract_sum_and_vo * 100, 2):0);

            $project->overall_cashflow_margin = (($project->overall_received > 0)?round($project->overall_cashflow_nett / $project->overall_received * 100, 2):0);
            $project->cashflow_margin = (($project->total_received > 0)?round($project->cashflow_nett / $project->total_received * 100, 2):0);
            $project->cashflow_margin_in_period = (($project->total_received_in_period > 0)?round($project->cashflow_nett_in_period / $project->total_received_in_period * 100, 2):0);

            foreach(["project_managers", "suppliers"] as $field){
                $details = [];
                if($project->$field)
                    foreach(explode(";|,", $project->$field) as $data)
                        if(count($array = explode("=", $data)) > 1)
                            $details[] = ["id" => $array[0], "name" => rtrim($array[1], ";|")];
                        else
                            $details[]["name"] = rtrim($data, ";|");
                $project->$field = array_filter($details);
            }

            $data = [];
            foreach($months as $index => $month){
                $data[$index]["month"] = $month;
                foreach(["in", "out", "nett"] as $cashflow){
                    $string = "cashflow_" . $cashflow . "_" . $month;
                    $data[$index][$cashflow] = $project->$string;
                    unset($project->$string);
                }
            }
            $project->chart_data = $data;
        });
        return $projects;
    }

    /**
     * @return $this
     */
    public function getAllQuery(){
        $this->sortBy = "projects.name";
        $noOfMonths = hasValue("no_of_months");
        $selects = [];
        $now = \Carbon\Carbon::now();
        for($i = 1 ; $i <= (((int)$noOfMonths)?$noOfMonths:0) ; $i++){
            $currentMonth = $now->copy()->subMonthNoOverflow($i);
            $month = strtolower($currentMonth->format("M"));
            foreach(["in", "out"] as $cashflow){
                $selects[] = \DB::raw("SUM(DISTINCT `ie`.`cashflow_" . $cashflow . "_" . $month . "`) AS `cashflow_" . $cashflow . "_" . $month . "`");
            }
            $selects[] = \DB::raw("SUM(DISTINCT `ie`.`cashflow_in_$month`) - SUM(DISTINCT `ie`.`cashflow_out_$month`) AS `cashflow_nett_$month`");
        }
        $this->query = $this->query()
            ->search($this->request)
            ->getProjectManagers()
            ->projectManagers($this->request->get("project_manager"))
            ->joinClient()
            ->joinInvoiceExpensesUnion(true, $noOfMonths)
            ->joinInvoiceExpensesUnion(false, $noOfMonths)
            ->addSelect(array_merge([
                "projects.id",
                "projects.uuid",
                "projects.name",
                "projects.budget",
                "projects.remarks",
                "projects.end_customer",
                "clients.uuid AS client_uuid",
                "clients.name AS client",
                "clients.address_1 AS client_address_1",
                "clients.address_2 AS client_address_1",
                "clients.postcode AS client_postcode",
                "clients.city AS client_city",
                "clients.state AS client_state",
                "clients.country AS client_country",
                "clients.tel_no AS client_tel_no",
                "clients.fax_no AS client_fax_no",
                "clients.person_in_charge AS client_person_in_charge",
                "clients.end_customer AS client_end_customer",
                "projects.schedule",
                "projects.retention",
                "projects.retention_percent",
                "projects.contract_sum",
                "projects.variation_order",
                \DB::raw("COALESCE(`projects`.`contract_sum` + `projects`.`variation_order`, 0) as `contract_sum_and_vo`"),
                \DB::raw("COALESCE(`projects`.`contract_sum` + `projects`.`variation_order` - `projects`.`retention`, 0) as `contract_sum_vo_retention`"),
                "projects.omission_value",
                "projects.contingency_amt",
                "projects.start_date",
                "projects.end_date",
                \DB::raw("DATEDIFF(`projects`.`end_date`, `projects`.`start_date`) AS `duration`"),
                \DB::raw("COALESCE(DATEDIFF(NOW(), `projects`.`start_date`) / DATEDIFF(`projects`.`end_date`, `projects`.`start_date`) * 100, 0) AS `duration_la`"),
                "projects.status",
                \DB::raw("GROUP_CONCAT(DISTINCT CONCAT(`users`.`id`, '=', `users`.`name`), ';|') AS `project_managers`"),
                // overall
                \DB::raw("COALESCE(SUM(DISTINCT `overall_ie`.`total_invoiced`), 0) AS `overall_invoiced`"),
                \DB::raw("COALESCE(SUM(DISTINCT `overall_ie`.`invoices_tax_amt`), 0) AS `overall_invoices_tax_amt`"),
                \DB::raw("COALESCE(SUM(DISTINCT `overall_ie`.`total_received`), 0) AS `overall_received`"),
                \DB::raw("COALESCE(SUM(DISTINCT `overall_ie`.`total_received_include_tax`), 0) AS `overall_received_include_tax`"),
                \DB::raw("COALESCE(SUM(DISTINCT `overall_ie`.`total_receivable`), 0) AS `overall_receivable`"),
                \DB::raw("COALESCE(SUM(DISTINCT `overall_ie`.`total_advance_received`), 0) AS `overall_advance_received`"),
                \DB::raw("COALESCE(SUM(DISTINCT `overall_ie`.`total_expenses`), 0) AS `overall_expenses`"),
                \DB::raw("COALESCE(SUM(DISTINCT `overall_ie`.`expenses_tax_amt`), 0) AS `overall_expenses_tax_amt`"),
                \DB::raw("COALESCE(SUM(DISTINCT `overall_ie`.`total_paid`), 0) AS `overall_paid`"),
                \DB::raw("COALESCE(SUM(DISTINCT `overall_ie`.`total_paid_include_tax`), 0) AS `overall_paid_include_tax`"),
                \DB::raw("COALESCE(SUM(DISTINCT `overall_ie`.`total_payable`), 0) AS `overall_payable`"),
                \DB::raw("COALESCE(SUM(DISTINCT `overall_ie`.`total_overpay`), 0) AS `overall_overpay`"),
                \DB::raw("COALESCE(SUM(DISTINCT `overall_ie`.`total_received`), 0) - COALESCE(SUM(DISTINCT `overall_ie`.`total_paid`), 0) AS `overall_cashflow_nett`"),
                \DB::raw("GROUP_CONCAT(DISTINCT `overall_ie`.`suppliers`) AS `suppliers`"),
                // total
                \DB::raw("COALESCE(SUM(DISTINCT `ie`.`total_invoiced`), 0) AS `total_invoiced`"),
                \DB::raw("COALESCE(SUM(DISTINCT `ie`.`invoices_tax_amt`), 0) AS `total_invoices_tax`"),
                \DB::raw("COALESCE(SUM(DISTINCT `ie`.`total_received`), 0) AS `total_received`"),
                \DB::raw("COALESCE(SUM(DISTINCT `ie`.`total_received_include_tax`), 0) AS `total_received_include_tax`"),
                \DB::raw("COALESCE(SUM(DISTINCT `ie`.`total_receivable`), 0) AS `total_receivable`"),
                \DB::raw("COALESCE(SUM(DISTINCT `ie`.`total_advance_received`), 0) AS `total_advance_received`"),
                \DB::raw("COALESCE(SUM(DISTINCT `ie`.`total_expenses`), 0) AS `total_expenses`"),
                \DB::raw("COALESCE(SUM(DISTINCT `ie`.`expenses_tax_amt`), 0) AS `total_expenses_tax`"),
                \DB::raw("COALESCE(SUM(DISTINCT `ie`.`total_paid`), 0) AS `total_paid`"),
                \DB::raw("COALESCE(SUM(DISTINCT `ie`.`total_paid_include_tax`), 0) AS `total_paid_include_tax`"),
                \DB::raw("COALESCE(SUM(DISTINCT `ie`.`total_payable`), 0) AS `total_payable`"),
                \DB::raw("COALESCE(SUM(DISTINCT `ie`.`total_overpay`), 0) AS `total_overpay`"),
                \DB::raw("COALESCE(SUM(DISTINCT `ie`.`total_received`), 0) - COALESCE(SUM(DISTINCT `ie`.`total_paid`), 0) AS `cashflow_nett`"),
                // in period
                \DB::raw("COALESCE(SUM(DISTINCT `ie`.`total_received_in_period`), 0) AS `total_received_in_period`"),
                \DB::raw("COALESCE(SUM(DISTINCT `ie`.`total_received_include_tax_in_period`), 0) AS `total_received_include_tax_in_period`"),
                \DB::raw("COALESCE(SUM(DISTINCT `ie`.`total_receivable_in_period`), 0) AS `total_receivable_in_period`"),
                \DB::raw("COALESCE(SUM(DISTINCT `ie`.`total_paid_in_period`), 0) AS `total_paid_in_period`"),
                \DB::raw("COALESCE(SUM(DISTINCT `ie`.`total_paid_include_tax_in_period`), 0) AS `total_paid_include_tax_in_period`"),
                \DB::raw("COALESCE(SUM(DISTINCT `ie`.`total_payable_in_period`), 0) AS `total_payable_in_period`"),
                \DB::raw("COALESCE(SUM(DISTINCT `ie`.`total_received_in_period`), 0) - COALESCE(SUM(DISTINCT `ie`.`total_paid_in_period`), 0) AS `cashflow_nett_in_period`"),
                // cashflow section
                \DB::raw("SUM(DISTINCT COALESCE(`ie`.`cashflow_in_30`, 0) - COALESCE(`ie`.`cashflow_out_30`, 0)) AS `cashflow_30`"),
                \DB::raw("SUM(DISTINCT COALESCE(`ie`.`cashflow_in_60`, 0) - COALESCE(`ie`.`cashflow_out_60`, 0)) AS `cashflow_60`"),
                \DB::raw("SUM(DISTINCT COALESCE(`ie`.`cashflow_in_90`, 0) - COALESCE(`ie`.`cashflow_out_90`, 0)) AS `cashflow_90`"),
            ], $selects))
            ->groupBy(["projects.id", "clients.id"])
        ;
        return $this;
    }

    /**
     * @return $this
     */
    public function getProjectsManagerQuery(){
        $this->sortBy = "projects.name";
        $this->query = User::join("model_has_permissions", "model_has_permissions.model_id", "=", "users.id")
            ->join("permissions", "model_has_permissions.permission_id", "=", "permissions.id")
            ->where("model_has_permissions.model_type", "=", "App\Models\User")
            ->join("projects", \DB::raw("CONCAT('project.view.', `projects`.`id`)"), "=", "permissions.name")
            ->addSelect([
                "users.id",
                "users.name",
                "projects.id AS project_id",
                "projects.uuid AS project_uuid",
                "projects.name AS project_name",
            ])
        ;
        return $this;
    }

    /**
     * @return $this
     */
    public function getProjectsForBulkPayment(){
        $this->sortBy = "projects.name";
        $this->query = $this->query()
            ->search($this->request, 'projects')
            ->joinClient()
            ->joinExpenses()
            ->groupBy('projects.uuid', 'projects.name', 'clients.name', 'expenses.project_id')
            ->addSelect([
                'projects.uuid',
                'projects.name',
                'clients.name AS client',
                "expenses.project_id",
                \DB::raw("GROUP_CONCAT(DISTINCT `contractors`.`name`, ';|') AS `suppliers`")
            ])
        ;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProjectionCashflow(){
        $from = \Carbon\Carbon::now()->subMonthNoOverflow(1)->startOfMonth();
        $to = $from->copy()->addMonthNoOverflow(round(hasValue("days") / 30))->endOfMonth();
        $this->request->merge(["due_date" => ["start" => $from, "end" => $to], "balance" => 0]);
        $contractors = [];
        $all_contractors = \App\Models\Contractor::select(["uuid", "name"])->get();
        foreach($all_contractors as $index => $value)
            $contractors[$value["uuid"]] = $value["name"];
        unset($all_contractors);
        $project = $this->getProjectionCashflowQuery()->getSet(function($project, $index) use ($contractors){
            foreach($project->expenses as $expense){
                $expense->contractor = ((isset($contractors[$expense->contractor_id]))?$contractors[$expense->contractor_id]:"-");
            }
        });
        $project->cashflow_in = $project->invoices->sum("balance");
        $project->cashflow_out = $project->expenses->sum("balance");
        $project->cashflow_nett = $project->cashflow_in - $project->cashflow_out;
        return $project;
    }

    /**
     * @return $this
     */
    public function getProjectionCashflowQuery(){
        $this->query = $this->query()
            ->withSearch([
                "invoices;project_id,due_date,cert_no,nett_certified,gst,sst,payment_amt,balance" => ["date" => ["due_date"], ">" => ["balance"]],
                "expenses;project_id,contractor_id,due_date,ref_no,invoice_amt,dc_note,credit_note,gst,sst,payment_amt,balance" => ["date" => ["due_date"], ">" => ["balance"]],
            ], $this->request)
            ->addSelect(["id", "uuid"])
        ;
        return $this;
    }

    public function try(){
        $projects = $this->tryQuery()->collect(function($project, $index){
            $project->contract_sum_vo = $project->contract_sum + $project->variation_order;
            $project->contract_sum_vo_retention = $project->contract_sum + $project->variation_order - $project->retention;
            $project->duration_la = $project->duration_la;
            $project->current_margin = $project->current_margin;
            // overall invoices
//            $project->overall_invoiced = round($project->invoices->sum("nett_certified"), 2);
//            $project->overall_invoices_tax = round($project->invoices->sum("gst") + $project->invoices->sum("sst"), 2);
//            $project->overall_received_with_tax = round($project->invoices->sum("payment_amt"), 2);
//            $project->overall_received = round($project->overall_received_with_tax - $project->overall_invoices_tax, 2);
//            $project->overall_receivable = round($project->invoices->where("balance", ">", 0)->sum("balance"), 2);
//            $project->overall_advance_received = -round($project->invoices->where("balance", "<", 0)->sum("balance"), 2);
            $project->invoices->sumAll([
                "nett_certified:overall_invoiced",
                "gst:overall_invoices_gst",
                "sst:overall_invoices_sst",
                "payment_amt:overall_received_with_tax",
                "balance:overall_receivable" => [["`balance`", ">", "0"]],
                "balance:overall_advance_received" => [["`balance`", "<", "0"]],
            ], $this->request, [], $project);
            $project->overall_invoices_tax = round($project->overall_invoices_gst + $project->overall_invoices_sst, 2);
            $project->overall_received = round($project->overall_received_with_tax - $project->overall_invoices_tax);
            // overall expenses
//            $project->overall_expenses = round($project->expenses->sum("invoice_amt") + $project->expenses->sum("dc_note"), 2);
//            $project->overall_expenses_tax = round($project->expenses->sum("gst") + $project->expenses->sum("sst"), 2);
//            $project->overall_paid_with_tax = round($project->expenses->sum("payment_amt"), 2);
//            $project->overall_paid = round($project->overall_paid_with_tax - $project->overall_expenses_tax, 2);
//            $project->overall_payable = round($project->expenses->where("balance", ">", 0)->sum("balance"), 2);
//            $project->overall_overpay = -round($project->expenses->where("balance", "<", 0)->sum("balance"), 2);
            $project->expenses->sumAll([
                "invoice_amt:overall_expenses",
                "dc_note:overall_dc_note",
                "credit_note:overall_credit_note",
                "gst:overall_expenses_gst",
                "sst:overall_expenses_sst",
                "payment_amt:overall_paid_with_tax",
                "balance:overall_payable" => [["`balance`", ">", "0"]],
                "balance:overall_overpay" => [["`balance`", "<", "0"]],
            ], $this->request, [], $project);
            $project->overall_expenses = round($project->overall_expenses + $project->overall_dc_note + $project->overall_credit_note, 2);
            $project->overall_expenses_tax = round($project->overall_expenses_gst + $project->overall_expenses_sst, 2);
            $project->overall_paid = round($project->overall_paid_with_tax - $project->overall_expenses_tax, 2);
            unset($project->contractors, $project->invoices, $project->expenses);
        });
        return $projects;
    }

    public function tryQuery(){
        $this->sortBy = "name";
        $filters = [
            "or" => [
                "filter" => ["name;projects.name", "client"],
                "date_time_filter" => ["created_at", "updated_at"],
                "has" => [
                    [
                        "query" => \App\Models\Contractor::
                            join("contractor_project", "contractors.id", "=", "contractor_project.contractor_id")
                            ->whereNull("contractors.deleted_at")
                        ,
                        "join" => ["`projects`.`id`", "`contractor_project`.`project_id`"],
                        "search" => [
                            "filter" => ["name", "address_1"],
                            "date_filter" => ["created_at", "updated_at"],
                        ],
                    ],
                    [
                        "query" => \DB::table("permissions")
                            ->join("model_has_permissions", "permissions.id", "=", "model_has_permissions.permission_id")
                            ->join("users", "users.id", "=", "model_has_permissions.model_id")
                            ->where("model_has_permissions.model_type", "=", "App\Models\User")
                        ,
                        "join" => ["`projects`.`id`", "REPLACE(`permissions`.`name`, 'project.view.', '')"],
                        "search" => [
                            "!=" => [["users.name", "permissions.name"]],
                        ],
                    ],
                ],
            ]
        ];
        $this->query = $this->query()
            ->search($this->request, $filters)
            ->withSearch([
//                "pm.users;id,name;orWhereHas" => ["filter" => ["name"]],
//                "pm.users;id,name",
//                "pm;id,name,guard_name",
//                "contractors;id,uuid,name,type;whereHas" => ["filter" => ["name"]],
                "contractors;id,uuid,name,type",
                "invoices.payments;id,uuid,project_id,invoice_id,payment_amt,payment_date",
                "invoices;id,uuid,project_id,nett_certified,gst,sst,payment_amt,balance,due_date",
                "expenses.payments;id,uuid,project_id,expenses_id,payment_amt,payment_date",
                "expenses;id,uuid,project_id,invoice_amt,dc_note,credit_note,gst,sst,payment_amt,balance,due_date",
            ], $this->request)
        ;
        return $this;
    }
}
