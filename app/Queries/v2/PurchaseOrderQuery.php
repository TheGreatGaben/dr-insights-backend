<?php
namespace App\Queries\v2;

use App\Models\PurchaseOrder;
use Illuminate\Http\Request;

class PurchaseOrderQuery extends BaseQuery
{
    /**
     * default model name for query
     *
     * @var String
     */
    protected $modelName = PurchaseOrder::class;

    /**
     * PurchaseOrderQuery constructor.
     *
     * @param Request|null $request
     * @param PurchaseOrder|null $purchaseOrder
     */
    public function __construct(Request $request = null, PurchaseOrder $purchaseOrder = null){
        parent::__construct($request, $purchaseOrder);
    }

    /**
     * @return mixed
     */
    public function getAll(){
        if($this->request->has("summary") && $this->request->get("summary")){
            $purchaseOrder = $this->getAllSummaryQuery()->get();
        }else{
            $purchaseOrder = $this->getAllQuery()->get();
        }
        return $purchaseOrder;
    }

    /**
     * @return $this
     */
    public function getAllQuery(){
        $this->query = $this->query()
            ->search($this->request)
            ->joinProject()
            ->joinContractor()
            ->leftJoinExpenses()
            ->addSelect([
                "purchase_orders.*",
                \DB::raw("COALESCE(SUM(`expenses`.`invoice_amt` + `expenses`.`dc_note`), 0) AS `total_expenses`"),
                \DB::raw("COALESCE(SUM(`expenses`.`payment_amt`), 0) AS `total_paid`"),
                "projects.uuid AS project_uuid",
                "projects.name AS project_name",
                "contractors.uuid AS contractor_uuid",
                "contractors.name AS contractor_name",
            ])
            ->groupBy(["purchase_orders.id", "projects.id", "contractors.id"])
        ;
        return $this;
    }

    /**
     * @return $this
     */
    public function getAllSummaryQuery(){
        $this->sortByKey = false;
        $this->sortBy = "purchase_orders.po_no";
        $this->query = \DB::query()->fromSub($this->getAllQuery()->query, "purchase_orders")
            ->addSelect([
                "purchase_orders.po_no",
                "purchase_orders.project_name",
                "purchase_orders.contractor_name",
                "purchase_orders.issued_date",
                \DB::raw("SUM(purchase_orders.budget) AS budget"),
                \DB::raw("SUM(purchase_orders.dc_note) AS dc_note"),
                \DB::raw("SUM(purchase_orders.gst) AS gst"),
                \DB::raw("SUM(purchase_orders.sst) AS sst"),
                \DB::raw("SUM(purchase_orders.total_value) AS total_value"),
                \DB::raw("SUM(`purchase_orders`.`total_expenses`) AS `total_expenses`"),
                \DB::raw("SUM(`purchase_orders`.`total_paid`) AS `total_paid`"),
            ])
            ->groupBy(["purchase_orders.po_no", "purchase_orders.issued_date", "purchase_orders.project_name", "purchase_orders.contractor_name"])
        ;
        return $this;
    }
}
