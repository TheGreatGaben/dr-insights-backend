<?php
namespace App\Queries\v2;

use App\Models\Role;
use Illuminate\Http\Request;

class RoleQuery extends BaseQuery
{
    /**
     * default model name for query
     *
     * @var String
     */
    protected $modelName = Role::class;

    /**
     * @var array
     */
    protected $roles_level = [
        "1" => ["2", "3", "4"], // KAMI can assign user to Admin, Business Owner and Project manager
        "2" => ["3", "4"], // Admin can assign user to Business Owner and Project manager
        "3" => ["4"], // Business Owner can assign user to Project manager
        "4" => [],
    ];

    /**
     * RoleQuery constructor.
     *
     * @param Request|null $request
     * @param Role $permission
     */
    public function __construct(Request $request = null, Role $role = null){
        parent::__construct($request, $role);
    }

    /**
     * @return mixed
     */
    public function getAllRole(){
        return $this->getAllRoleQuery()->get();
    }

    /**
     * @return $this
     */
    public function getAllRoleQuery(){
        $this->sortBy = "roles.name";
        $this->query = $this->query()
            ->search($this->request)
            ->addSelect([
                "roles.id",
                "roles.name",
            ])
        ;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRolePermission(){
        return $this->getRolePermissionQuery()->get();
    }

    /**
     * @return $this
     */
    public function getRolePermissionQuery(){
        $this->sortBy = "roles.name";
        $this->query = $this->query()
            ->search($this->request)
            ->join("role_has_permissions", "role_has_permissions.role_id", "=", "roles.id")
            ->join("permissions", "role_has_permissions.permission_id", "=", "permissions.id")
            ->addSelect([
                "roles.id",
                "roles.name",
                "permissions.id AS permission_id",
                "permissions.name AS permission_name",
            ]);
        ;
        return $this;
    }
}
