<?php
namespace App\Queries\v2;


use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\User;

class TokenQuery
{
    public function auditImpersonationSession(User $impersonator, User $impersonated, $tokenID)
    {
        DB::insert('insert into user_impersonation_session values (?, ?, ?, ?)', [$tokenID, $impersonated->id, $impersonator->id, Carbon::now()]);
    }

    public function deleteTempToken($tokenID)
    {
        DB::delete('delete from oauth_access_tokens where id = ?', [$tokenID]);
    }
}
