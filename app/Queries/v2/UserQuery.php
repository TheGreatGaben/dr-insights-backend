<?php
namespace App\Queries\v2;

use App\Models\User;
use App\Models\Project;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserQuery extends BaseQuery
{
    /**
     * default model name for query
     *
     * @var String
     */
    protected $modelName = User::class;

    /**
     * @var bool
     */
    protected $withRolePermission = true;

    /**
     * UserQuery constructor.
     *
     * @param Request|null $request
     * @param User|null $user
     */
    public function __construct(Request $request = null, User $user = null){
        parent::__construct($request, $user);
    }

    /**
     * @return mixed
     */
    public function getAllUser(){
        $allProjects = [];
        foreach(Project::addSelect(["id", "name"])->get()->toArray() as $project){
            $allProjects[$project["id"]] = $project["name"];
        }
        $users = $this->getAllUserQuery()->getSet(function($user, $index) use ($allProjects){
            $projectPermissions = $allPermissions = [];
            $user->roles->each(function($role, $index) use (&$allPermissions){
                $role->permissions->each(function($permission, $index) use (&$allPermissions){
                    $allPermissions[] = ["id" => $permission->id, "name" => $permission->name];
                });
                unset($role->permissions);
            });
            $user->permissions->each(function($permission, $index) use ($allProjects, &$projectPermissions){
                $project_id = substr($permission->name, strlen("project.view."));
                if(isset($allProjects[$project_id])){
                    $projectPermissions[] = [
                        "id" => $permission->id,
                        "name" => $permission->name,
                        "project_id" => $project_id,
                        "project_name" => $allProjects[$project_id],
                    ];
                }
            });
            $user->project_permissions = $projectPermissions;
            $user->all_permissions = array_merge($allPermissions, $projectPermissions);
            unset($user->permissions, $user->roles->permissions);
        });
        return $users;
    }

    /**
     * @return $this
     */
    public function getAllUserQuery(){
        $this->sortBy = "users.name";
        $this->query = $this->query()
            ->search($this->request)
            ->when($this->withRolePermission, function($query){
                return $query->with('roles.permissions:id,name')
                    ->with('roles:id,name')
                    ->with('permissions:id,name');
            })
            ->addSelect([
                "users.id",
                "users.name",
                "users.email",
            ])
        ;
        return $this;
    }

    /**
     * @param $role
     * @return mixed
     */
    public function getUserByRoles($role){
        $roles = $this->getUserByRolesQuery($role)->get();
        return $roles;
    }

    /**
     * @param $role
     * @return $this
     */
    public function getUserByRolesQuery($role){
        $this->sortBy = "users.name";
        $this->query = $this->query()
            ->search($this->request)
            ->role($role)
            ->when($this->withRolePermission, function($query){
                return $query->with('roles:id,name')
                    ->with('permissions:id,name');
            })
            ->addSelect([
                "users.id",
                "users.name",
                "users.email",
            ])
        ;
        return $this;
    }

    /**
     * @return $this
     */
    public function hideRolePermission(){
        $this->withRolePermission = false;
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getProjectManagerSummary(){
        $now = Carbon::now();
        $months = [];
        $no_of_months = (int)hasValue("no_of_months");
        for($i = (($no_of_months)?$no_of_months:1) ; $i > 0 ; $i--) {
            $currentMonth = $now->copy()->subMonthNoOverflow($i);
            $months[] = strtolower($currentMonth->format("M"));
        }
        $projectManagers = $this->getProjectManagerSummaryQuery()->getSet(function($user, $index) use ($months){
            $chart_data = [];
            $overall = 0;
            foreach($months as $index => $month){
                $chart_data[$index]["month"] = $month;
                foreach(["in", "out", "nett"] as $cashflow){
                    $string = "cashflow_" . $cashflow . "_" . $month;
                    $chart_data[$index][$cashflow] = $user->$string;
                    if ($cashflow == "nett") {
                        $overall += $user->$string;
                    }
                    unset($user->$string);
                }
            }
            $user->overall_nett = round($overall, 2);
            $user->chart_data = $chart_data;
        });
        return $projectManagers;
    }

    /**
     * @return $this
     */
    public function getProjectManagerSummaryQuery(){
        $projectManager = array_filter((is_array($projectManager = hasValue("project_manager")))?$projectManager:[$projectManager]);
        $isProjectManager = !auth()->user()->hasAnyRole([1,2,3]);
        $this->query = User::search($this->request)
            ->when($isProjectManager, function($query){
                return $query->where("users.id", "=", auth()->user()->id);
            })
            ->when(!$isProjectManager && !empty($projectManager), function($query) use ($projectManager){
                return $query->whereIn("users.id", $projectManager);
            })
            ->role(4)
            ->projects()
            ->unionInvoicesExpenses(hasValue("no_of_months"))
            ->addSelect([
                "users.id",
                "users.name",
                "users.email",
            ])
            ->groupBy(["users.id"])
        ;
        return $this;
    }
}
