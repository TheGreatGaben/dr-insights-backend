<?php
namespace App\Queries\v2;

use App\Models\Project;
use App\Models\Contractor;
use App\Models\VariationOrder;
use Illuminate\Http\Request;

class VariationOrderQuery extends BaseQuery
{
    /**
     * default model name for query
     *
     * @var String
     */
    protected $modelName = VariationOrder::class;

    /**
     * PurchaseOrderQuery constructor.
     *
     * @param Request|null $request
     * @param VariationOrder|null $variationOrder
     */
    public function __construct(Request $request = null, VariationOrder $variationOrder = null){
        parent::__construct($request, $variationOrder);
    }

    /**
     * @return mixed
     */
    public function getAll(){
        $variationOrder = $this->getAllQuery()->get();
        return $variationOrder;
    }

    /**
     * @return $this
     */
    public function getAllQuery(){
        $this->query = $this->query()
            ->search($this->request)
            ->joinProject()
            ->joinContractor()
            ->leftJoinExpenses()
            ->addSelect([
                "variation_orders.*",
                "expenses.uuid AS expenses_uuid",
                \DB::raw("COALESCE(`expenses`.`invoice_amt` + `expenses`.`dc_note`, 0) AS `total_expenses`"),
                \DB::raw("COALESCE(`expenses`.`payment_amt`, 0) AS `total_paid`"),
                "projects.name AS project_name",
                "contractors.name AS contractor_name",
            ])
        ;
        return $this;
    }
}
