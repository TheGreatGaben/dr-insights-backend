<?php
// if name contain '&', please change it to '%26'
// if not result will not found
// because '&' symbol is a spacial character in url

if(!isset($from_seeder) || !$from_seeder){
    move_client_contractor_data(request()->get("type"), request()->get("from") && request()->get("to"));
}

function move_client_contractor_data($type, $from_name, $to_name){
    if($type && $from_name && $to_name){
        if(in_array($model_name = strtolower($type), ["client", "contractor"])){
            $model = "\\App\\Models\\" . ucfirst($model_name);
            $from = with(new $model())->where("name", "like", $from_name);
            $to = with(new $model())->where("name", "like", $to_name);

            if(empty($multiple_results) && $from->exists() && $to->exists()) {
                $from = $from->first();
                $to = $to->first();
                if($from->uuid == $to->uuid){
                    dump("Both from and to are same " . ucfirst($model_name) . ". Where " . $from_name . " = " . $to_name);
                }else{
                    if($model_name == "contractor"){
                        $expenses = \App\Models\Expense::where("contractor_id", "=", $from->uuid);
                        if($expenses->count()){
                            dump("Please remember these expenses ID for revert in future if needed: \n\n" . $expenses->get()->implode("id", ", "));
                            $expenses->update(["contractor_id" => $to->uuid]);
                        }
                        $purchase_orders = \App\Models\PurchaseOrder::where("contractor_id", "=", $from->uuid);
                        if($purchase_orders->count()){
                            dump("Please remember these purchase orders ID for revert in future if needed: \n\n" . $purchase_orders->get()->implode("id", ", "));
                            $purchase_orders->update(["contractor_id" => $to->uuid]);
                        }
                        $variation_orders = \App\Models\VariationOrder::where("contractor_id", "=", $from->uuid);
                        if($variation_orders->count()){
                            dump("Please remember these variation orders ID for revert in future if needed: \n\n" . $variation_orders->get()->implode("id", ", "));
                            $variation_orders->update(["contractor_id" => $to->uuid]);
                        }
                        \DB::table("contractor_project")
                            ->where("contractor_id", $from->id)
                            ->update(["contractor_id" => $to->id])
                        ;
                    }else{
                        $projects = \App\Models\Project::where("client_id", "=", $from->uuid);
                        $invoices = \App\Models\Invoice::where("client_id", "=", $from->uuid);
                        if($invoices->count()){
                            dump("Please remember these invoices ID for revert in future if needed: \n\n" . $invoices->get()->implode("id", ", "));
                            $invoices->update(["client_id" => $to->uuid]);
                        }
                        if($projects->count()){
                            dump("Please remember these projects ID for revert in future if needed: \n\n" . $projects->get()->implode("id", ", "));
                            $projects->update(["client_id" => $to->uuid]);
                        }
                    }
                    $from->delete();
                    dump(ucfirst($model_name) . " all data under " . $from->name . " is moved and belongs to " . $to->name . " now. And " . $from->name . " is deleted.");
                    dump("From " . ucfirst($model_name) . " uuid = " . $from->uuid . ", To " . ucfirst($model_name) . " uuid = " . $to->uuid);
                }
            }else{
                $from = $from->withTrashed();
                $to = $to->withTrashed();
                if($from->exists() && $to->exists()){
                    dump("Either from or to " . ucfirst($model_name) . " already deleted. Where " . $from_name . " = " . $to_name);
                }else{
                    dump("Either from or to " . ucfirst($model_name) . " is not found. Where " . $from_name . " = " . $to_name);
                }
            }
        }
    }
}
