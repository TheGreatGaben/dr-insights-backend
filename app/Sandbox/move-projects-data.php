<?php
// this script use to move project data to another project

if(!isset($from_seeder) || !$from_seeder){
    move_project();
}

function move_project(){
    if(request()->filled(["from", "to"])){
        $from = $to = null;
        foreach(["from", "to"] as $type){
            $value = request()->get($type);
            if(is_numeric($value)){
                $$type = \App\Models\Project::find($value);
            }else{
                $$type = \App\Models\Project::findByUuid($value);
                if(!$$type){
                    $$type = \App\Models\Project::where("name", request()->get($type))->first();
                }
            }
        }
        if($from && $to){
            // move all data under source(from) project to target(to) project
            $invoices = \App\Models\Invoice::where("project_id", "=", $from->uuid)->whereNull("deleted_at");
            if($invoices->count()){
                dump("Please remember these invoices ID for revert in future if needed: \n\n" . $invoices->get()->implode("id", ", "));
                $invoices->update(["project_id" => $to->uuid]);
            }
            $expenses = \App\Models\Expense::where("project_id", "=", $from->uuid)->whereNull("deleted_at");
            if($expenses->count()){
                dump("Please remember these expenses ID for revert in future if needed: \n\n" . $expenses->get()->implode("id", ", "));
                $expenses->update(["project_id" => $to->uuid]);
            }
            $payments = \App\Models\Payment::where("project_id", "=", $from->uuid)->whereNull("deleted_at");
            if($payments->count()){
                dump("Please remember these payments ID for revert in future if needed: \n\n" . $payments->get()->implode("id", ", "));
                $payments->update(["project_id" => $to->uuid]);
            }
            $pos = \App\Models\PurchaseOrder::where("project_id", "=", $from->uuid)->whereNull("deleted_at");
            if($pos->count()){
                dump("Please remember these purchase orders ID for revert in future if needed: \n\n" . $pos->get()->implode("id", ", "));
                $pos->update(["project_id" => $to->uuid]);
            }
            $vos = \App\Models\VariationOrder::where("project_id", "=", $from->uuid)->whereNull("deleted_at");
            if($vos->count()){
                dump("Please remember these variation orders ID for revert in future if needed: \n\n" . $vos->get()->implode("id", ", "));
                $vos->update(["project_id" => $to->uuid]);
            }
            \DB::table("contractor_project")
                ->where("project_id", $from->id)
                ->update(["project_id" => $to->id])
            ;
            // clean duplicate contractor_project if any
            $duplicate = \DB::table("contractor_project")
                ->select(["contractor_id", "project_id", \DB::raw("COUNT(id) as total")])
                ->groupBy(["contractor_id", "project_id"])
                ->havingRaw('COUNT(id) > ?', [1])
            ;
            if($duplicate->count()){
                foreach($duplicate->get() as $data){
                    \DB::table("contractor_project")
                        ->where("project_id", $data->project_id)
                        ->where("contractor_id", $data->contractor_id)
                        ->limit($data->total - 1)
                        ->delete()
                    ;
                }
            }
            $from->delete();
            dump("All data under source(from) project already move to target(to) project. And source(from) project is deleted.");
        }else{
            dump("Either source(from) project name or target(to) project name or both is not found.");
        }
    }else{
        dump("Please provide source(from) project name and target(to) project name.");
    }
}
