<?php
$table = (request()->has("table") && request()->input("table"))?request()->input("table"):"clients";
try{
    $compare_lists = [];
    $lists = \DB::query()->from($table)
        ->select(["id", "name", "created_at"])
        ->whereNull("deleted_at")
        ->orderByRaw("CHAR_LENGTH(`name`) DESC, `name` ASC")
        ->get()
    ;
    if(request()->has("date") && ($date = request()->input("date"))){
        $compare_lists = $lists->where("created_at", ">=", $date)->all();
        $lists = $lists->where("created_at", "<=", $date)->all();
    }
    echo toTable(checkSimilarity($lists, $compare_lists));
}catch(\Exception $e){
    dd($e->getMessage());
}

function checkSimilarity($lists, $compare_lists = array()){
    $similar_results = $exclude = [];
    $compare_lists = ((empty($compare_lists))?$lists:$compare_lists);
    foreach($lists as $list){
        if(!isset($exclude[$list->id])){
            $exclude[$list->id] = 1;
            foreach($compare_lists as $compare){
                if(!isset($exclude[$compare->id])){
                    $percent = 0;
                    similar_text(removeSpecificWords($list->name), removeSpecificWords($compare->name), $percent);
                    if($percent >= 70){
                        $exclude[$compare->id] = 1;
                        $similar_results[$list->id . " == " . $list->name][] = $compare->id . " = " . $compare->name . " = " . round($percent, 2) . "%";
                    }
                }
            }
        }
    }
    return $similar_results;
}

function removeSpecificWords($name){
    $matches = [];
    $remove_words = [
        " and ", " & ", "sdn", "bhd", "s/b", "berhad", "corporation", "services", "service", "sales", "sale", "engineering", "industries", "holdings", "holding", "development", "specialist", "hospital", "perunding",
        "conditioner", "conditioning", "works", "management", "trading", "travel", "consult", "tower", "enterprise", "cabin", "associates", "associate", "builders", "builder", "system", "group",
        "kejuruteraan", "co.", "ltd", "properties", "nasional", "tenaga",
        "air", "tech", "cond", "aircond", "electrical", "electric", "construction", "contract", "resources", "technology", "renovation", "supplies", "city", "malaysia", "international"
    ];
    // add to remove any word in bracket
    if(preg_match("/(\(.*\))/", $name, $matches)){
        $remove_words = array_merge($remove_words, array_unique(array_map("strtolower", $matches)), ["()"]);
    }
    return str_ireplace("  ", " ", trim(str_ireplace($remove_words, "", strtolower($name))));
}

function toTable($lists){
    $no = 1;
    $table = "<table border=1><thead><tr><th>No.</th><th>ID</th><th>Name</th><th>Similar</th></tr></thead><tbody>";
    foreach($lists as $key => $list){
        list($id, $name) = explode(" == ", $key);
        $table .= "<tr><td>" . $no++ . "</td><td>$id</td><td>$name</td><td>";
        foreach($list as $l){
            $table .= $l . "<br>";
        }
        $table .= "</td></tr>";
    }
    $table .= "</tbody></table>";
    return $table;
}
