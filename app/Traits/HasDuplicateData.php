<?php

namespace App\Traits;

use App\Events\HasDuplicateDataEvent;

trait HasDuplicateData
{
    /**
     * Boot function from laravel.
     */
    protected static function bootHasDuplicateData()
    {
        static::saved(function ($model){
            $sendEmail = true;
            if(!empty(static::$duplicateData) && $model->isDirty(static::$duplicateData)){
                $check = new static();
                foreach(static::$duplicateData as $field){
                    if($model->$field){
                        $check = $check->where($field, "=", $model->$field);
                    }elseif(static::$wontNotifyIfAnyDataEmpty){
                        $sendEmail = false;
                    }
                }
                if($sendEmail && $check->count() > 1){
                    event(new HasDuplicateDataEvent($model, static::$duplicateData));
                }
            }
        });
    }
}
