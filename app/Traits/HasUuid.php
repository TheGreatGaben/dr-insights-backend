<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait HasUuid
{
    /**
     * Register to insert new user with uuid
     */
    public static function bootHasUuid()
    {
        static::creating(function ($model){
            $uuidFieldName = $model->getUuidFieldName();
            if(empty($model->$uuidFieldName)){
                $model->$uuidFieldName = $model->generateUuid();
            }
        });
    }

    public function getKeyType(){
        return 'string';
    }

    /**
     * @return string
     */
    public function getRouteKeyName(){
        return static::getUuidFieldName();
    }

    /**
     * Get uuid column name in database
     *
     * @return string
     */
    public static function getUuidFieldName(){
        if(!empty(static::$uuidFieldName)){
            return static::$uuidFieldName;
        }
        return 'uuid';
    }

    /**
     * Generate Uuid for model
     *
     * @return string
     * @throws \Exception
     */
    public static function generateUuid(){
        return (string) Str::uuid();
    }

    /**
     * Load model by uuid
     *
     * @param $uuid
     * @return mixed
     */
    public static function findByUuid($uuid){
        return static::where(static::getUuidFieldName(), "=", $uuid)->first();
    }
}
