<?php

namespace App\Traits;

trait Observable
{
    /**
     * autoload observer class you set for the model
     */
    protected static function bootObservable()
    {
        if(isset(static::$observer) && static::$observer){
            static::observe(static::$observer);
        }
    }

    /**
     * quick save the model without observer
     *
     * @param array $options
     * @return mixed
     */
    public function qSave(array $options = [])
    {
        return static::withoutEvents(function () use ($options){
            return $this->fill($options)->save();
        });
    }

    /**
     * temporarily remove observer for quick save
     * this function only needed for laravel version
     * lower than 5.7
     *
     * @param callable $callback
     * @return mixed
     */
    public static function withoutEvents(callable $callback)
    {
        $dispatcher = static::getEventDispatcher();
        static::unsetEventDispatcher();
        try {
            return $callback();
        }finally{
            static::setEventDispatcher($dispatcher);
        }
    }
}
