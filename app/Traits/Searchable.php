<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

trait Searchable
{
    /**
     * @var Request
     */
    protected $searchRequest;

    /**
     * @var string
     */
    protected $searchTable;

    /**
     * @var bool
     */
    protected $startOrSearch = false;

    /**
     * @var array
     */
    protected $availableSymbolSearch = ["<", "<=", ">", ">=", "!=", "<>", "="];

    /**
     * call search from eloquent
     *
     * @param $query
     * @param Request|null $request
     * @param string $tableOrFilters
     * @param bool $enable
     * @return mixed
     * @throws \Exception
     * @see $this->searching()
     */
    public function scopeSearch($query, Request $request = null, $tableOrFilters = "", $enable = true){
        if(is_array($tableOrFilters)){
            $tableOrFilters = ((isset($tableOrFilters[$this->searchTable = $this->getTable()]))?$tableOrFilters[$this->searchTable]:$tableOrFilters);
            return $this->searching($query, $request, $tableOrFilters, $enable);
        }elseif(isset($this->filters[$this->searchTable = (($tableOrFilters)?$tableOrFilters:$this->getTable())])){
            return $this->searching($query, $request, $this->filters[$this->searchTable], $enable);
        }
    }

    /**
     * call search from any place beside eloquent
     * will call filter or datefilter function here
     * remaining will call from callSearching()
     *
     * @param $query
     * @param Request|null $request
     * @param array $filters
     * @param bool $enable
     * @return mixed
     * @throws \Exception
     * @see $this->callSearching()
     */
    public function searching($query, Request $request = null, $filters = [], $enable = true){
        $this->searchRequest = (($request)?$request:request());
        if($enable)
            foreach($filters as $type => $fields){
                if(Str::endsWith(strtolower($type), 'filter') && method_exists($this, strtolower($function = Str::camel($type))))
                    $this->$function($query, $fields, $this->searchRequest->input(strtolower($type)));
                elseif(in_array(strtolower($type), ["or", "has"]) && method_exists($this, ($function = strtolower($type) . "Nested")))
                    $this->$function($query, $fields);
                elseif(in_array($type, $this->availableSymbolSearch))
                    $this->callSearching($query, "symbolSearch", $fields, $type);
                elseif(method_exists($this, ($function = strtolower($type) . "Search")))
                    $this->callSearching($query, $function, $fields);
            }
        return $query;
    }
    /**
     * call specific function for searching
     *
     * @param $query
     * @param $function
     * @param $fields
     * @param string $extra
     * @return mixed
     * @throws \Exception
     */
    private function callSearching($query, $function, $fields, $extra = ""){
        foreach($fields as $field => $tables){
            ((is_string($field) && is_array($tables))?:$field = $tables);
            if(is_array($field)){
                $this->orNested($query, [(($extra)?$extra:substr($function, 0, -6)) => $tables]);
            }elseif(!is_null($requestData = $this->retrieveRequestData($field)) || !empty($requestData)){
                if(is_array($tables) && !empty($tables)){
                    foreach ($tables as $table)
                        $this->$function($query, ((is_array($table))?[$field => $table]:"$table.$field"), $requestData, $extra);
                }else{
                    $this->$function($query, $field, $requestData, $extra);
                }
            }
        }
        return $query;
    }

    /**
     * call search from eloquent by
     * with(), whereHas() or orWhereHas() method
     *
     * @param $query
     * @param $withTable
     * @param Request|null $request
     * @param bool $enable
     * @return mixed
     * @see $this->scopeSearch()
     */
    public function scopeWithSearch($query, $withTable, Request $request = null, $enable = true){
        $withTable = ((is_array($withTable))?$withTable:[$withTable]);
        foreach($withTable as $tableColumns => $tableFilters){
            list($table, $whereHas, $callback) = $this->retrieveWithSearchDetails($request, $tableColumns, $tableFilters, $enable);
            (($whereHas)?$query->$whereHas($table, $callback):$query->with([$table => $callback]));
        }
    }

    /**
     *******************************************
     * Filter
     *******************************************
     */

    /**
     * generate OR search for string columns.
     * eg: (name LIKE '%{string}%' OR email LIKE '%{string}%')
     *
     * @param $query
     * @param $fields
     * @param string $filter
     * @return mixed
     */
    private function filter($query, $fields, $filter = ""){
        if($filter){
            $query->where(function ($query) use ($fields, $filter){
                foreach($fields as $field){
                    $column = $this->retrieveFilterColumn($field);
                    $query->orWhere(\DB::raw("LOWER($column)"), 'like', '%' . strtolower($filter) . '%');
                }
                return $query;
            });
        }
        return $this;
    }

    /**
     * generate OR search for date time columns.
     *
     * @see $this->dateFilter()
     * @param $query
     * @param $fields
     * @param $date
     * @return $this
     */
    private function dateTimeFilter($query, $fields, $date){
        $this->datefilter($query, $fields, $date, true);
        return $this;
    }

    /**
     * generate OR search for date columns. support date range also.
     * eg: (date > '2019-01-01' OR date2 > '2019-01-01')
     *
     * you can search with time also by set the $time to true
     * eg: (date > '2019-01-01 00:00:00' OR date2 > '2019-01-01 00:00:00')
     *
     * if date value from querystring don't have start or end, will become like below:
     * eg: (date = '2019-01-01')
     *
     * @param $query
     * @param $fields
     * @param array $date
     * @param bool $time
     * @return mixed
     */
    private function dateFilter($query, $fields, $date, $time = false){
        if(!empty($date))
            $query->where(function ($query) use ($fields, $date, $time){
                foreach($fields as $field){
                    $column = $this->retrieveFilterColumn($field);
                    if(is_array($date)){
                        $date = $this->retrieveDate($date, $time);
                        if(strtotime($date["start"]) > 0 && strtotime($date["end"]) > 0)
                            $query->orWhereBetween($column, [$date["start"], $date["end"]]);
                        elseif(strtotime($date["start"]) > 0)
                            $query->orWhere($column, '>=', $date["start"]);
                        elseif(strtotime($date["end"]) > 0)
                            $query->orWhere($column, '<=', $date["end"]);
                    }else{
                        $query->orWhere($column, '=', ((new Carbon($date))->format("Y-m-d" . (($time)?" H:i:s":""))));
                    }
                }
                return $query;
            });
        return $this;
    }

    /**
     *******************************************
     * Searching
     *******************************************
     */

    /**
     * generate NULL search for columns.
     *
     * @param $query
     * @param $field
     * @param $data
     * @return $this
     */
    private function nullSearch($query, $field, $data){
        $method = (($this->startOrSearch)?"orW":"w") . "hereNull";
        if($data)
            $query->$method($this->retrieveFilterColumn($field));
        return $this;
    }

    /**
     * generate amount search for columns.
     * like: >, >=, <, <=, <>, !=
     *
     * @param $query
     * @param $field
     * @param $data
     * @param $type
     * @return $this
     */
    private function symbolSearch($query, $field, $data, $type){
        $method = (($this->startOrSearch)?"orW":"w") . "here";
        if(is_array($field)){
            $fields = $this->retrieveOrFilterColumn($field);
            $query->where(function ($query) use ($fields, $data, $type){
                foreach($fields as $field){
                    $column = $this->retrieveFilterColumn($field);
                    $query->orWhere(\DB::raw("LOWER($column)"), $type, strtolower($data));
                }
                return $query;
            });
        }else {
            $query->$method($this->retrieveFilterColumn($field), $type, $data);
        }
        return $this;
    }

    /**
     * generate AND search for string columns.
     *
     * @see $this->string()
     * @param $query
     * @param $field
     * @param $data
     * @return $this
     */
    private function likeSearch($query, $field, $data){
        $this->stringSearch($query, $field, $data, true);
        return $this;
    }

    /**
     * generate AND search for string columns.
     * eg: name LIKE '{string}' AND email LIKE '{string}'
     *
     * you can search columns contain characters by set the $like to true
     * eg: name LIKE '%{string}%' AND email LIKE '%{string}%'
     *
     * @param $query
     * @param $field
     * @param $data
     * @param bool $like
     * @return $this
     */
    private function stringSearch($query, $field, $data, $like = false){
        $method = (($this->startOrSearch)?"orW":"w") . "here";
        if(is_array($field))
            $this->filter($query, $this->retrieveOrFilterColumn($field), $data, $like);
        else
            $query->$method($this->retrieveFilterColumn($field), "LIKE", (($like) ? "%" . $data . "%" : $data));
        return $this;
    }

    /**
     * generate AND search for columns which are selected values
     * eg: status IN ('New', 'Done')
     *
     * @param $query
     * @param $field
     * @param $data
     * @return $this
     */
    private function inSearch($query, $field, $data){
        $method = (($this->startOrSearch)?"orW":"w") . "hereIn";
        $query->$method($this->retrieveFilterColumn($field), ((is_array($data))?$data:[$data]));
        return $this;
    }

    /**
     * generate AND search for date time columns.
     *
     * @see $this->date()
     * @param $query
     * @param $field
     * @param $date
     * @return $this
     */
    private function dateTimeSearch($query, $field, $date){
        $this->dateSearch($query, $field, $date, true);
        return $this;
    }

    /**
     * generate AND search for date columns. support date range also.
     * eg: (date > '2019-01-01' AND date2 > '2019-01-01')
     *
     * you can search with time also by set the $time to true
     * eg: (date > '2019-01-01 00:00:00' AND date2 > '2019-01-01 00:00:00')
     *
     * if date value from querystring don't have start or end, will become like below:
     * eg: (date = '2019-01-01')
     *
     * @param $query
     * @param $field
     * @param $date
     * @param bool $time
     * @return $this
     */
    private function dateSearch($query, $field, $date, $time = false){
        if(is_array($field)){
            $this->dateFilter($query, $this->retrieveOrFilterColumn($field), $date, $time);
        }else{
            $method = (($this->startOrSearch)?"orW":"w") . "here";
            $column = $this->retrieveFilterColumn($field);
            if(is_array($date)){
                $date = $this->retrieveDate($date, $time);
                if(strtotime($date["start"]) > 0 && strtotime($date["end"]) > 0){
                    $method .= "Between";
                    $query->$method($column, [$date["start"], $date["end"]]);
                }elseif(strtotime($date["start"]) > 0){
                    $query->$method($column, '>=', $date["start"]);
                }elseif(strtotime($date["end"]) > 0){
                    $query->$method($column, '<=', $date["end"]);
                }
            }else{
                $query->$method($column, '=', ((new Carbon($date))->format("Y-m-d" . (($time)?" H:i:s":""))));
            }
        }
        return $this;
    }

    /**
     *******************************************
     * Nested Searching Functions
     *******************************************
     */

    /**
     * generate 'exists' sql statement for searching by using sub query
     * so we can search all data that has relation data only
     * it allow another searching array for sub query
     *
     * @param $query
     * @param $hasSearches
     * @return $this
     * @throws \Exception
     */
    private function hasNested($query, $hasSearches){
        $originalTable = $this->searchTable;
        $originalSearchMethod = $this->startOrSearch;
        foreach($hasSearches as $hasSearch){
            $subQuery = "";
            if($hasSearch["query"] instanceof \Illuminate\Database\Eloquent\Builder)
                $subQuery = $hasSearch["query"]->getQuery();
            elseif($hasSearch["query"] instanceof \Illuminate\Database\Query\Builder)
                $subQuery = $hasSearch["query"];
            elseif(is_string($hasSearch["query"]))
                $subQuery = \DB::table($hasSearch["query"]);
            if($subQuery){
                $this->searchTable = $subQuery->from;
                $this->startOrSearch = false;
                $query->addWhereExistsQuery($this->searching($subQuery
                    ->select(\DB::raw(1))
                    ->whereRaw($hasSearch["join"][0] . " = " . $hasSearch["join"][1])
                , $this->searchRequest, $hasSearch["search"]), (($originalSearchMethod)?"or":"and"));
            }
        }
        $this->searchTable = $originalTable;
        $this->startOrSearch = $originalSearchMethod;
        return $this;
    }

    /**
     * set to use or search
     *
     * @param $query
     * @param $filters
     * @return $this
     * @throws \Exception
     */
    private function orNested($query, $filters){
        if($this->startOrSearch)
            throw new \Exception("Or Search loop.");
        $this->startOrSearch = true;
        $query->where(function($query) use ($filters){
            $this->searching($query, $this->searchRequest, $filters);
        });
        $this->startOrSearch = false;
        return $this;
    }

    /**
     *******************************************
     * Others
     *******************************************
     */

    /**
     * generate date array with key 'start' or 'end' or both
     *
     * @param $date
     * @param bool $time
     * @return array
     */
    private function retrieveDate($date, $time = false){
        $data = ["start" => "", "end" => ""];
        if(isset($date["start"]) &&  strtotime($date["start"]) > 0)
            $data["start"] = ((new Carbon($date["start"]))->format("Y-m-d" . (($time)?" H:i:s":"")));
        if(isset($date["end"]) && strtotime($date["end"]) > 0)
            $data["end"] = ((new Carbon($date["end"]))->format("Y-m-d" . (($time)?" H:i:s":"")));
        return $data;
    }

    /**
     * generate column string to do searching
     *
     * @param $field
     * @return string
     */
    private function retrieveFilterColumn($field){
        $column = "$this->searchTable.$field";
        if(count($filterDetails = explode(";", $field)) > 1)
            $column = $filterDetails[1];
        else
            if(count($filterDetails = explode(".", $field)) > 1){
                list($table, $column) = $filterDetails;
                return "$table.$column";
            }
        return $column;
    }

    /**
     * generate column string to do searching for or search
     * E.g: ['string' => ["name" => ['user', 'admin']]]
     * above example will generate query as below:
     * (user.name = '' AND admin.name = '')
     *
     * @param $fields
     * @return array
     */
    private function retrieveOrFilterColumn($fields){
        $columns = [];
        foreach($fields as $field => $tables)
            foreach($tables as $table)
                $columns[] = "$table.$field";
        return $columns;
    }

    /**
     * retrieve data from querystring.
     *
     * @param $field
     * @return array|string
     */
    private function retrieveRequestData($field){
        $column = $field;
        if(count($filterDetails = explode(";", $field)) > 1)
            $column = $filterDetails[0];
        return $this->searchRequest->input($column);
    }

    /**
     * extra details and settings for with search
     *
     * @param Request|null $request
     * @param $tableColumns
     * @param $tableFilters
     * @param bool $enable
     * @param string $whereHas
     * @return array
     */
    private function retrieveWithSearchDetails(Request $request = null, $tableColumns, $tableFilters, $enable = true){
        if(is_array($tableFilters)){
            $tableColumns = explode(";", $tableColumns);
        }else{
            $tableColumns = explode(";", $tableFilters);
            $tableFilters = "";
        }
        // first value must be the table name
        $table = $tableColumns[0];
        // second value must be the columns that needed to select and return
        // will return all if empty or got '*'
        $columns = ((isset($tableColumns[1]))?explode(",", $tableColumns[1]):[]);
        // third value will decide call method name.
        // if empty or invalid will default call with()
        // valid method include: whereHas() or orWhereHas()
        $whereHas = ((isset($tableColumns[2]) && in_array(strtolower($tableColumns[2]), ["wherehas", "orwherehas"]))?$tableColumns[2]:"");
        // forth value will decide return trashed data or not
        $withTrashed = ((isset($tableColumns[3]) && in_array(strtolower($tableColumns[3]), ["deleted"]))?true:false);
        // last will generate the callback function before return
        return [$table, $whereHas, function($query) use ($request, $tableFilters, $columns, $withTrashed, $enable){
            $tableFilters = ((is_array($tableFilters))?$tableFilters:[]);
            return $query->search($request, $tableFilters, $enable)
                ->when(is_array($columns) && !empty($columns), function($query) use ($columns){
                    return $query->addSelect(array_map(function($column) use ($query){
                        return $query->getModel()->getTable() . "." . trim($column);
                    }, $columns));
                })
                ->when($withTrashed, function($query){
                    return $query->withTrashed();
                })
            ;
        }];
    }
}
