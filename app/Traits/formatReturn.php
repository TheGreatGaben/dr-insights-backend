<?php

namespace App\Traits;

trait formatReturn
{
    public function excludeID($fields)
    {
        unset($fields->id);
        return $fields;
    }
}