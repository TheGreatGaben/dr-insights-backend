<?php

namespace App\Traits;

use Ramsey\Uuid\Uuid;

trait uuids
{

    /**
     * Boot function from laravel.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            #$model->uuid = Uuid::generate()->string;
            $model->uuid = Uuid::uuid4()->toString();
        });
    }
}