<?php

return [
    "invoice" => [
        "include_advance_received" => env('PROJECT_INVOICE_ADVANCE_RECEIVED', true),
    ],
    "expenses" => [
        "include_overpay_amount" => env('PROJECT_EXPENSES_OVERPAY_RECEIVED', true),
    ],
];
