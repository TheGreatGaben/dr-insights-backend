<?php

use Faker\Generator as Faker;
use Carbon\Carbon;
use Illuminate\Support\Str;

$factory->define(App\Models\Expense::class, function (Faker $faker) {
    $randomAmount = $faker->randomFloat(2, 10000, 90000);
    return [
        'invoice_date' => Carbon::now()->addDays($faker->numberBetween(1, 90)),
        'ref_no' => 'Inv: ' . Str::random(10),
        'invoice_amt' =>  $randomAmount,
    ];
});
