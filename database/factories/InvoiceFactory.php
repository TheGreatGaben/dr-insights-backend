<?php

use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Models\Invoice::class, function (Faker $faker) {
    $randomAmount = $faker->randomFloat(2, 10000, 90000);
    return [
        'project_id' => $faker->uuid,
        'invoice_no' => Str::random(10),
        'invoice_date' => Carbon::now()->addDays($faker->numberBetween(1, 90)),
        'cert_no' => 'PC#1',
        'nett_certified' => $randomAmount,
        'term' => 30,
        'retention' => 0,
        'retention_released' => 0,
        'work_done_certified' => $randomAmount,
        'prev_certified' => 0,
        'debit_note' => 0,
        'vo' => 0,
    ];
});
