<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Payment::class, function (Faker $faker) {
    return [
        'payment_date' => now()->addDays($faker->numberBetween(1, 90)),
        'payment_amt' => $faker->randomFloat(2, 5000, 30000),
    ];
});
