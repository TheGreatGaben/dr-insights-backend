<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Models\Project::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'start_date' => Carbon::now()->subDays($faker->numberBetween(15, 540)),
        'end_date' => Carbon::now()->addDays($faker->numberBetween(-450, 90)),
        'contract_sum' => $faker->randomFloat(2, 100000, 5000000),
        'variation_order' => $faker->randomFloat(2, 50000, 500000),
        'budget' => $faker->randomFloat(2, 50000, 125000),
    ];
});
