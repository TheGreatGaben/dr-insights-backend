<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->index();
            $table->timestamps();
            $table->softDeletes();
            $table->string('name')->nullable();
            $table->string('client')->nullable();
            $table->string('end_customer')->nullable();
            $table->string('schedule')->nullable();
            $table->string('project_manager')->nullable();
            $table->string('status')->default('New')->nullable();
            $table->date('project_completion')->nullable();
            $table->decimal('contract_sum', 20, 2)->default(0.00);
            $table->decimal('budget', 20, 2)->default(0.00);
            $table->decimal('retention_percent', 5, 2)->default(0.00);
            $table->decimal('retention', 20, 2)->default(0.00);
            $table->decimal('variation_order', 20, 2)->default(0.00);
            $table->decimal('simulated_cashflow', 20, 2)->default(0.00);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
