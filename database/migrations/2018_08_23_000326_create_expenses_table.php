<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->index();
            $table->timestamps();
            $table->softDeletes();
            $table->string('project_id')->index();
            $table->date('invoice_date')->nullable();
            $table->date('date_received')->nullable();
            $table->string('contractor_id')->nullable()->index();
            $table->string('contractor')->nullable()->index();
            $table->string('payable_type')->nullable();
            $table->integer('payable_id')->unsigned()->nullable();
            $table->string('ref_no')->nullable();
            $table->string('term')->nullable();
            $table->date('due_date')->nullable()->index();
            $table->string('description')->nullable();
            $table->decimal('invoice_amt', 20, 2)->default(0.00)->nullable();
            $table->decimal('gst', 20, 2)->default(0.00)->nullable();
            $table->decimal('sst', 20, 2)->default(0.00)->nullable();
            $table->boolean('exclude_tax')->default(false);
            $table->decimal('dc_note', 20, 2)->default(0.00)->nullable();
            $table->decimal('payment_amt', 20, 2)->default(0.00)->nullable();
            $table->decimal('balance', 20, 2)->default(0.00)->nullable();
            $table->string('remark')->nullable();
            $table->string('invoice_id')->index()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
