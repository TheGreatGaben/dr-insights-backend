<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->index();
            $table->timestamps();
            $table->softDeletes();
            $table->string('project_id')->index();
            $table->string('invoice_no')->nullable();
            $table->date('invoice_date')->nullable();
            $table->integer('term')->default(60)->nullable();
            $table->date('due_date')->nullable();
            $table->string('cert_no')->nullable();
            $table->string('description')->nullable();
            $table->decimal('work_done_certified', 20, 2)->default(0.00)->nullable();
            $table->decimal('vo', 20, 2)->default(0.00)->nullable();
            $table->decimal('retention_released', 20, 2)->default(0.00)->nullable();
            $table->decimal('retention', 20, 2)->default(0.00)->nullable();
            $table->decimal('prev_certified', 20, 2)->default(0.00)->nullable();
            $table->decimal('nett_certified', 20, 2)->default(0.00)->nullable();
            $table->decimal('gst', 20, 2)->default(0.00)->nullable();
            $table->decimal('sst', 20, 2)->default(0.00)->nullable();
            $table->boolean('exclude_tax')->default(false);
            $table->decimal('debit_note', 20, 2)->default(0.00)->nullable();
            $table->decimal('payment_amt', 20, 2)->default(0.00)->nullable();
            $table->decimal('balance', 20, 2)->default(0.00)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
