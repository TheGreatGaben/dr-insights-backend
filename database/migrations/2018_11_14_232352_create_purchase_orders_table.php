<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->index();
            $table->timestamps();
            $table->softDeletes();
            $table->string('project_id')->index();
            $table->string('contractor_id')->nullable()->index();
            $table->date('issued_date')->nullable();
            $table->string('po_no')->nullable();
            $table->string('party')->default('Contractor')->nullable(); /* Customer/Contractor */
            $table->string('description')->nullable();
            $table->decimal('total_value', 20, 2)->default(0.00)->nullable();
            $table->decimal('gst', 20, 2)->default(0.00)->nullable();
            $table->decimal('sst', 20, 2)->default(0.00)->nullable();
            $table->boolean('exclude_tax')->default(false);
            $table->decimal('dc_note', 20, 2)->default(0.00)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
