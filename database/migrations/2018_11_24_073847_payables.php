<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Payables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payables', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('expenses_id', 36);
            $table->string('parent_id', 36);
            $table->string('parent_type', 60);
            $table->index(['parent_id', 'expenses_id']);
            $table->index(['parent_type', 'parent_id']);
            $table->unique(['parent_type', 'parent_id', 'expenses_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payables');
    }
}
