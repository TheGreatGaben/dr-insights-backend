<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaymentsBatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->string('batch_no')->nullable()->index();
            $table->text('batch_remark')->nullable();
        });

        DB::statement('ALTER TABLE `payments` ADD INDEX `batch_payment_idx` (`deleted_at`, `batch_no`)');

        DB::statement('ALTER TABLE `payments` ADD FULLTEXT INDEX `batch_payment_idx2` (`batch_no`, `batch_remark`)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `payments` DROP INDEX `batch_payment_idx`');
        DB::statement('ALTER TABLE `payments` DROP INDEX `batch_payment_idx2`');

        Schema::table('payments',function(Blueprint $table){
            $table->dropColumn('batch_no');
            $table->dropColumn('batch_remark');
        });
    }
}
