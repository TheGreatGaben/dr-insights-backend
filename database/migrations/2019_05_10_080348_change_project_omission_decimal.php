<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeProjectOmissionDecimal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("ALTER TABLE `projects` CHANGE `omission_value` `omission_value` DECIMAL(20, 2) NOT NULL DEFAULT '0.00'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::statement("ALTER TABLE `projects` CHANGE `omission_value` `omission_value` DECIMAL(8, 2) NOT NULL DEFAULT '0.00'");
    }
}
