<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StoreTotalInProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->decimal('total_invoiced', 20, 2)->default(0)->after('client_id');
            $table->decimal('total_received', 20, 2)->default(0)->after('total_invoiced');
            $table->decimal('total_expenses', 20, 2)->default(0)->after('total_received');
            $table->decimal('total_paid', 20, 2)->default(0)->after('total_expenses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('total_invoiced');
            $table->dropColumn('total_received');
            $table->dropColumn('total_expenses');
            $table->dropColumn('total_paid');
        });
    }
}
