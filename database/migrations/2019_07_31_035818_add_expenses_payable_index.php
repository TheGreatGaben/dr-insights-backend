<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpensesPayableIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE `expenses` ADD INDEX(`payable_type`)");
        \DB::statement("ALTER TABLE `expenses` ADD INDEX(`payable_id`)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("ALTER TABLE `expenses` DROP INDEX(`payable_type`)");
        \DB::statement("ALTER TABLE `expenses` DROP INDEX(`payable_id`)");
    }
}
