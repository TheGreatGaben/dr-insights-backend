<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWriteOffColumnsToExpenses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenses', function (Blueprint $table) {
            $table->date('write_off_date')->nullable();
            $table->string('write_off_reason', 255)->nullable();
            $table->unsignedInteger('write_off_user')->nullable();
            $table->decimal('write_off_amt', 20, 2)->default(0.00)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenses', function (Blueprint $table) {
            $table->dropColumn(['write_off_date', 'write_off_reason', 'write_off_user', 'write_off_amt']);
        });
    }
}
