<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvoiceExpensesAccCodeId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function($table) {
            $table->uuid('acc_code_id')->nullable()->after('project_id');
        });
        Schema::table('expenses', function($table) {
            $table->uuid('acc_code_id')->nullable()->after('project_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function($table) {
            $table->dropColumn('acc_code_id');
        });
        Schema::table('expenses', function($table) {
            $table->dropColumn('acc_code_id');
        });
    }
}
