<?php

use \App\Models\Organisation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Organisations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('organisations')){
            Schema::create('organisations', function (Blueprint $table) {
                $table->increments('id');
                $table->uuid('uuid')->index();
                $table->string('name');
                $table->timestamps();
                $table->softDeletes();
            });
            $now = \Carbon\Carbon::now();
            Organisation::create([
                "name" => "Default"
            ]);
        }
        if(!Schema::hasColumn('invoices', 'organisation_id')){
            Schema::table('invoices', function ($table) {
                $table->uuid('organisation_id')->nullable()->after('project_id');
            });
        }
        if(!Schema::hasColumn('expenses', 'organisation_id')){
            Schema::table('expenses', function ($table) {
                $table->uuid('organisation_id')->nullable()->after('project_id');
            });
        }
        $org = Organisation::find(1);
        DB::table('invoices')->update(array('organisation_id' => $org->uuid));
        DB::table('expenses')->update(array('organisation_id' => $org->uuid));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organisations');
        if(Schema::hasColumn('invoices', 'organisation_id')){
            Schema::table('invoices', function($table) {
                $table->dropColumn('organisation_id');
            });
        }

        if(Schema::hasColumn('expenses', 'organisation_id')){
            Schema::table('expenses', function ($table) {
                $table->dropColumn('organisation_id');
            });
        }
    }
}
