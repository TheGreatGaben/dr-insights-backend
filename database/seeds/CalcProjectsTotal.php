<?php

use App\Models\Project;
use App\Models\Invoice;
use App\Models\Expense;
use Illuminate\Database\Seeder;

class CalcProjectsTotal extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // recalculate invoices payment amount and balance
        $sql = "UPDATE `invoices` SET `payment_amt` = (SELECT COALESCE(SUM(`payment_amt`), 0) FROM `payments` WHERE `payments`.`invoice_id` = `invoices`.`uuid` AND `payments`.`deleted_at` IS NULL), " .
            "`balance` = `nett_certified` + `gst` + `sst` - (SELECT COALESCE(SUM(`payment_amt`), 0) FROM `payments` WHERE `payments`.`invoice_id` = `invoices`.`uuid` AND `payments`.`deleted_at` IS NULL) " .
            "WHERE `invoices`.`deleted_at` IS NULL";
        // DB::statement($sql);

        // recalculate expenses payment amount and balance
        $sql = "UPDATE `expenses` SET `payment_amt` = (SELECT COALESCE(SUM(`payment_amt`), 0) FROM `payments` WHERE `payments`.`expenses_id` = `expenses`.`uuid` AND `payments`.`deleted_at` IS NULL), " .
            "`balance` = `invoice_amt` + `dc_note` + `gst` + `sst` - (SELECT COALESCE(SUM(`payment_amt`), 0) FROM `payments` WHERE `payments`.`expenses_id` = `expenses`.`uuid` AND `payments`.`deleted_at` IS NULL) " .
            "WHERE `expenses`.`deleted_at` IS NULL";
        // DB::statement($sql);

        $projects = Project::withTrashed()->get();
        foreach($projects as $project){
            // if project has been deleted, will deleted all related data like their invoices/expenses
            if(strtotime($project->deleted_at) > 0){
                $project->payments()->delete();
                $project->invoices()->delete();
                $project->purchaseOrders()->delete();
                $project->variationOrders()->delete();
                $project->expenses()->delete();
            }else{
                $total_invoiced = $total_received = $total_expenses = $total_paid = 0;
                $invoices = $project->invoices()->withTrashed()->get();
                foreach($invoices as $invoice){
                    if(strtotime($invoice->deleted_at) > 0){
                        $invoice->delete();
                    }else{
                        $total_invoiced += $invoice->nett_certified;
                        $total_received += ($invoice->payment_amt - $invoice->gst - $invoice->sst);
                    }
                }
                $project->total_invoiced = $total_invoiced;
                $project->total_received = $total_received;
                $expenses = $project->expenses()->withTrashed()->get();
                foreach($expenses as $expense){
                    if(strtotime($expense->deleted_at) > 0){
                        $expense->delete();
                    }else{
                        $total_expenses += ($expense->invoice_amt + $expense->dc_note);
                        $total_paid += ($expense->payment_amt - $expense->gst - $expense->sst);
                    }
                }
                $project->total_expenses = $total_expenses;
                $project->total_paid = $total_paid;
                $project->save();
            }
        }
    }
}
