<?php

use App\Models\Project;
use App\Models\Client;
use Illuminate\Database\Seeder;
class CreateClientsFromProjects extends Seeder
{
    /**
     * Create clients from existing projects.
     *
     * @return void
     */
    public function run()
    {
        $projects = Project::all();
        foreach ($projects as $project) {
            if ($project->deleted_at != null) {
                continue;
            }
            $clientName = $project->client;
            $client = Client::select('uuid')->where('name', $clientName)->first();

            if ($client == null) {
                $client = Client::create([
                    'name' => $clientName
                ]);
            }

            // Set client foreign key for project and its invoices
            $project->client_id = $client->uuid;
            $project->save();
            $invoices = $project->invoices()->get();
            foreach ($invoices as $invoice) {
                $invoice->client_id = $client->uuid;
                $invoice->save();
            }
        }
    }
}
