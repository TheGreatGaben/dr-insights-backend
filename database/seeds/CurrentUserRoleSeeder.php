<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class CurrentUserRoleSeeder extends Seeder
{
    /**
     * hardcode user which store diff name in db
     * @var array
     */
    protected $users = [
        "KL Chong" => [["id" => 9]],
        "Azmi Chin Sau Foh" => [["id" => 15]],
        "Mohd Firdaus" => [["id" => 18]],
    ];

    /**
     * Just for view
     * @var array
     */
    protected $users_not_found = [
        "Kau Calvin", "Liew", "Joseph Gan", "Ho Yap Ho", "Gan Zhijian", "Lai Teik Yik", "Ching Heng Wei", "Soo"
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_not_found = [];
        $projects = DB::table('projects')->get(['id', 'project_manager', 'deleted_at'])->toArray();
        foreach($projects as $project){
            $project_managers = explode("/", $project->project_manager);
            foreach($project_managers as $pm){
                $pm_name = trim(str_replace("Mr ", "", $pm));
                $user = User::select("id")->where("name", "LIKE", "%" . $pm_name . "%")->get()->toArray();
                if(count($user) <= 0){
                    if(isset($this->users[$pm_name])){
                        $user = $this->users[$pm_name];
                    }else{
                        if(!isset($user_not_found[$pm_name])){
                            $user_not_found[$pm_name] = 1;
                        }
                    }
                }
                if(count($user) == 1){
                    $user = User::find($user[0]["id"]);
                    $user->syncRoles(["Project Manager"]);
                    if($project->deleted_at == null){
                        $user->givePermissionTo("project.view." . $project->id);
                    }
                }
            }
        }
        print_r(array_keys($user_not_found));
    }
}
