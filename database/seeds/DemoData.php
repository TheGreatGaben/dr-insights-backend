<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Client;
use App\Models\Contractor;
use App\Models\Expense;
use App\Models\Invoice;
use App\Models\Organisation;
use App\Models\Payment;
use App\Models\Project;
use App\Models\User;
use App\Models\FinanceAccountCode;
use App\Queries\v2\FinanceAccountCodeQuery;

class DemoData extends Seeder
{
    private $testUserData = ["Gabriel", "gabriel@example.com"];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $codes = '4-0000';
        $finance_account_code = FinanceAccountCode::where("code", "=", $codes)->select(["id"])->first();
        $invoiceAccountCodes = (new FinanceAccountCodeQuery())->query()->descendantsAndSelf($finance_account_code->id);

        $codes = ['5-0000', '6-0000'];
        $expenseAccountCodes = collect([]);
        foreach($codes as $code) {
            $finance_account_code = FinanceAccountCode::where("code", "=", $code)->select(["id"])->first();
            if($finance_account_code) {
                $accountCodes = (new FinanceAccountCodeQuery())->query()->descendantsAndSelf($finance_account_code->id);
                $expenseAccountCodes = $expenseAccountCodes->merge($accountCodes);
            }
        }

        $faker = Faker::create();
        $testUser = User::where("email", "=", $this->testUserData[1])->first();

        $clientList = factory(Client::class, 10)->create();
        $contractorList = factory(Contractor::class, 10)->create();
        $pmList = factory(User::class, 5)->create()->each(function ($pm) {
            $pm->assignRole('Project Manager');
        });

        $organisation = Organisation::find(1); // default organisation

        for ($i = 0; $i < 30; $i++) {
            $randomClient = $clientList->random();
            $randomPM = $pmList->random();
            $project = factory(Project::class, 1)->create([
                'client' => $randomClient->name,
                'client_id' => $randomClient->uuid,
            ])->first();
            $randomPM->givePermissionTo("project.view." . $project->id);
            $testUser->givePermissionTo("project.view." . $project->id);

	        $projectDays = $project->end_date->diffInDays($project->start_date);

            // Create project invoices and their payments
            for ($j = 1; $j < $faker->numberBetween(6, 16); $j++) {
		        $invoiceDate = $project->start_date->addDays($faker->numberBetween(1, $projectDays));
                $invoice = factory(Invoice::class, 1)->create([
                    'project_id' => $project->uuid,
                    'cert_no' => 'PC#' . $j,
		            'invoice_date' => $invoiceDate,
                    'acc_code_id' => $invoiceAccountCodes->random()->uuid,
                    'organisation_id' =>  $organisation->uuid,
                ])->first();

                $paymentDate = $invoice->invoice_date->addDays($faker->numberBetween(1, $invoice->term));
                $paymentAmt = factory(Payment::class, $faker->numberBetween(5, 15))->create([
                    'project_id' => $project->uuid,
                    'invoice_id' => $invoice->uuid,
                    'payment_date' => $paymentDate,
                ])->sum('payment_amt');

                $invoice->update(['payment_amt' => $paymentAmt]);
            }

            // Create project expenses and their payments
            for ($k = 1; $k < $faker->numberBetween(6, 16); $k++) {
                $randomContractor = $contractorList->random();

		        $invoiceDate = $project->start_date->addDays($faker->numberBetween(1, $projectDays));
                $expense = factory(Expense::class, 1)->create([
                    'project_id' => $project->uuid,
                    'contractor_id' => $randomContractor->uuid,
                    'contractor_type' => $randomContractor->type,
                    'invoice_date' => $invoiceDate,
                    'date_received' => $invoiceDate,
                    'term' => 30,
                    'due_date' => $invoiceDate->addDays(30),
                    'acc_code_id' => $expenseAccountCodes->random()->uuid,
                    'organisation_id' =>  $organisation->uuid,
                ])->first();

                $paymentDate = $expense->invoice_date->addDays($faker->numberBetween(1, $expense->term));
                $paymentAmt = factory(Payment::class, $faker->numberBetween(5, 15))->create([
                    'project_id' => $project->uuid,
                    'expenses_id' => $expense->uuid,
                    'cash_flow' => 'OUT',
                    'payment_date' => $paymentDate,
                ])->sum('payment_amt');

                $expense->update(['payment_amt' => $paymentAmt]);
            }
        }
    }
}
