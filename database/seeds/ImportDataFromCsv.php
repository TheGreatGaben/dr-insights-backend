<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ImportDataFromCsv extends Seeder
{
    public $fileWithTable = [
        "CUST" => "Client",
        "SUPPLIERS" => "Contractor",
//        "EMPLOY" => "User",
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->fileWithTable as $file => $modelName){
            $modelName = "\\App\\Models\\" . $modelName;
            $model = new $modelName();
            $inserts = $inserted = $results = [];
            if(($csvFile = fopen(database_path("seeds/csv/" . $file . ".TXT"), "r")) !== false){
                $this->loadFile($csvFile, $results);
                foreach($results as $result){
                    $name = str_replace("  ", " ", $result["Co./Last Name"]);
                    if(!\DB::query()->from($model->getTable())->where("name", "=", $name)->exists() && !isset($inserted[$name])){
                        $data = [
                            "uuid" => (string) Str::uuid(),
                            "name" => $name,
                            "created_at" => \Carbon\Carbon::now(),
                            "updated_at" => \Carbon\Carbon::now(),
                        ];
                        list($data["address_1"], $data["city"], $data["state"], $data["postcode"], $data["tel_no"], $data["fax_no"]) = $this->getAddressPhoneFax($result);
                        $inserted[$name] = 1;
                        $inserts[] = $data;
                    }
                }
                $modelName::insert($inserts);
            }
        }
    }

    /**
     * to load all rows of data from file
     *
     * @param $csvFile
     * @param $results
     */
    public function loadFile($csvFile, &$results){
        $no = 0;
        $columns = [];
        $new = $prev = $column = "";
        while(($allData = fgetcsv($csvFile, 0, "\t")) !== false){
            if(empty($columns)){
                $columns = $allData;
            }else{
                foreach($allData as $index => $data){
                    if(isset($columns[$index]) && count($array = explode("-", $columns[$index])) >= 2){
                        list($new, $column) = $array;
                        if(trim($new))
                            $prev = trim($new);
                        $results[$no][$prev][trim($column)] = $data;
                    }elseif(!isset($columns[$index])){
                         // dd($columns, $index, $allData[1]);
                    }else{
                        $results[$no][trim($columns[$index])] = $data;
                    }
                }
                $no++;
            }
        }
        fclose($csvFile);
    }

    /**
     * to get address from 1-5 if not empty
     *
     * @param $details
     * @return array
     */
    public function getAddressPhoneFax($details){
        $address = $city = $state = $postcode = $tel_no = $fax_no = "";
        for($i = 1 ; $i <= 5 ; $i++){
            if(array_filter($details["Addr " . $i])){
                $addressArray = $details["Addr " . $i];
                $address = $addressArray["Line 1"] . " " . $addressArray["Line 2"] . " " . $addressArray["Line 3"] . " " . $addressArray["Line 4"];
                $city = $addressArray["City"];
                $state = $addressArray["State"];
                $postcode = $addressArray["Postcode"];
                $fax_no = $addressArray["Fax #"];
                for($j = 1 ; $j <= 3 ; $j++){
                    if($addressArray["Phone # " . $j]){
                        $tel_no = $addressArray["Phone # " . $j];
                        break;
                    }
                }
                break;
            }
        }
        return [$address, $city, $state, $postcode, $tel_no, $fax_no];
    }
}
