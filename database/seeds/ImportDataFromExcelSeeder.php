<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;
use App\Models\Project;
use App\Models\Contractor;
use App\Models\Expense;
use App\Models\Payment;

class ImportDataFromExcelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path("seeds/files/") . "Miscellaneous Project.xlsx";
        if(file_exists($file)){
            $xlsxData = Excel::toArray(new stdClass(), $file);
            for($i = 9 ; $i < count($xlsxData[0]) ; $i++){
                if(!empty(array_filter($data = $xlsxData[0][$i]))){
                    if(!$data[9])
                        continue;
                    $receive_date = (($data[12])?Carbon::createFromFormat('d-m-y', $data[11]):null);
                    $term = (((int)$data[14])?$data[14]:60);
                    $due_date = (($receive_date)?$receive_date->copy()->addDays($term):null);
                    $expenses = [
                        "project_id" => $project_id = $this->getProjectUuid($data[9]), // project name
                        "date_received" => $receive_date,
                        "invoice_date" => $invoice_date = (($data[10])?Carbon::createFromFormat('d-m-y', $data[10]):$receive_date),
                        "contractor_id" => $this->getContractorUuid($data[12]),
                        "ref_no" => $data[13],
                        "term" => $term,
                        "due_date" => $due_date,
                        "description" => $data[16],
                        "invoice_amt" => $data[17],
                    ];
                    if(strtotime($invoice_date) >= strtotime('2015-04-01') && strtotime($invoice_date) < strtotime('2018-09-01')){
                        $expenses['gst'] = $data[18];
                    }elseif(strtotime($invoice_date) >= strtotime('2018-09-01')){
                        $expenses['sst'] = $data[18];
                    }
                    if(!$this->checkExpenseExist($expenses)){
                        $expense = Expense::create($expenses);
                        // add payment if any for expenses
                        $cheques = explode("/", $data[20]);
                        $payment_dates = explode("/", substr($data[22], 3));
                        if($expense && !empty($cheques) && !empty($payment_dates) && count($cheques) == count($payment_dates)){
                            $paid_amount = $data[19];
                            for($j = 0 ; $j < count($cheques) ; $j++){
                                Payment::create([
                                    "project_id" => $project_id,
                                    "expenses_id" => $expense->uuid,
                                    "cash_flow" => "OUT",
                                    "cheque_no" => $cheques[$j],
                                    "payment_date" => Carbon::createFromFormat('d-m-y', $payment_dates[$j]),
                                    "payment_amt" => $paid_amount,
                                ]);
                                $paid_amount = 0;
                            }
                        }
                        dd("checking");
                    }
                }
            }
        }
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getProjectUuid($name){
        $project = Project::select("uuid")->where("name", "=", $name);
        if($project->exists()){
            $project = $project->first();
        }else{
            $project = Project::create([
                "name" => $name,
                "client_id" => "",
            ]);
        }
        return $project->uuid;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getContractorUuid($name){
        $contractor = Contractor::select("uuid")->where("name", "=", $name);
        if($contractor->exists()){
            $contractor = $contractor->first();
        }else{
            $contractor = Contractor::create([
                "name" => $name,
            ]);
        }
        return $contractor->uuid;
    }

    /**
     * @param $data
     * @return bool
     */
    public function checkExpenseExist($data){
        return Expense::where("project_id", "=", $data["project_id"])
            ->where("contractor_id", "=", $data["contractor_id"])
            ->where("ref_no", "=", $data["ref_no"])
            ->where("invoice_amt", $data["invoice_amt"])
            ->exists();
    }
}
