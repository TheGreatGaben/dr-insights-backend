<?php

use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    public $users = [
        ["Eng", "esp@example.com"],
        ["Lih", "nenglih@example.com"],
        ["YiHou", "yihou@example.com"],
        ["John", "john@example.com"],
        ["Gabriel", "gabriel@example.com"],
    ];

    public $roles = [
        "KAMI",
        "Admin",
        "Business Owner",
        "Project Manager",
    ];

    public $permissions = [
        'role',
        'role.add',
        'role.edit',
        'role.delete',
        'permission',
        'permission.add',
        'permission.edit',
        'permission.delete',
        'member',
        'member.add',
        'member.edit',
        'member.delete',
        'client',
        'client.add',
        'client.edit',
        'client.delete',
        'contractor',
        'contractor.add',
        'contractor.edit',
        'contractor.delete',
        'finance_account_code',
        'finance_account_code.add',
        'finance_account_code.edit',
        'finance_account_code.delete',
        'project',
        'project.add',
        'project.edit',
        'project.delete',
        'project.invoice',
        'project.invoice.add',
        'project.invoice.edit',
        'project.invoice.delete',
        'project.expense',
        'project.expense.add',
        'project.expense.edit',
        'project.expense.delete',
        'project.payment',
        'project.payment.add',
        'project.payment.edit',
        'project.payment.delete',
        'project.po',
        'project.po.add',
        'project.po.edit',
        'project.po.delete',
        'project.vo',
        'project.vo.add',
        'project.vo.edit',
        'project.vo.delete',
        'report',
        'report.client',
        'report.contractor',
        'report.po',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->permissions as $permission) {
            if(!Permission::where("name", "=", $permission)->count()){
                Permission::create(['name' => $permission, 'guard_name' => 'api']);
            }
        }
        foreach($this->roles as $role){
            if(!($new_role = Role::where("name", "=", $role)->first())){
                $new_role = Role::create(['name' => $role, 'guard_name' => 'api']);
            }
            if($role == "KAMI") {
                $new_role->givePermissionTo($this->permissions);
            }elseif($role == "Admin"){
                $new_role->givePermissionTo($this->filter_permissions(["role", "permission"]));
            }elseif($role == "Business Owner"){
                $new_role->givePermissionTo(["project", "client", "contractor", "project.invoice", "project.expense", "project.po", "report", "report.client", "report.contractor", "report.po"]);
            }
        }
        foreach(Project::all() as $index => $project){
            if(!Permission::where("name", "=", "project.view." . $project->id)->count()) {
                Permission::create(['name' => "project.view." . $project->id, 'guard_name' => 'api']);
            }
        }

        foreach($this->users as $userData){
            list($name, $email) = $userData;
            if(!($user = User::where("email", "=", $email)->first())){
                $user = User::create(["name" => $name, "email" => $email, "password" => "qwe123"]);
            }
            $user->syncRoles(["KAMI"]);
        }
    }

    public function filter_permissions($filters = []){
        $filtered = [];
        $patterns = implode("|", $filters);
        array_walk($this->permissions, function($value, $key) use (&$filtered, $patterns){
            if(!preg_match("/^(" . $patterns . ")/", $value)){
                $filtered[$key] = $value;
            }
        });
        return $filtered;
    }
}
