<?php

use Illuminate\Database\Seeder;

class UpdateAllDataDate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // update invoices
        $sql = "UPDATE `invoices` SET `invoice_date` = DATE_ADD(`invoice_date`, INTERVAL 1 MONTH), `due_date` = DATE_ADD(`due_date`, INTERVAL 1 MONTH) WHERE `deleted_at` IS NULL";
        \DB::statement($sql);

        // update expenses
        $sql = "UPDATE `expenses` SET `invoice_date` = DATE_ADD(`invoice_date`, INTERVAL 1 MONTH), `due_date` = DATE_ADD(`due_date`, INTERVAL 1 MONTH), `date_received` = DATE_ADD(`date_received`, INTERVAL 1 MONTH) WHERE `deleted_at` IS NULL";
        \DB::statement($sql);

        // update payments
        $sql = "UPDATE `payments` SET `payment_date` = DATE_ADD(`payment_date`, INTERVAL 1 MONTH) WHERE `deleted_at` IS NULL";
        \DB::statement($sql);
    }
}
