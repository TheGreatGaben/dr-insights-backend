<?php

use App\Models\Client;
use App\Models\Project;
use Illuminate\Database\Seeder;

class UpdateDemoDataFirst extends Seeder
{
    private $projects_name_clients = [
        "Mid Valey"	=> "Berjaya Group",
        "IOI Mall" => "IOI Group",
        "Amcorp Mall" => "Amcorp Group",
        "Pavilion" => "Urusharta Cemerlang (KL)",
        "Paradigm Mall" => "WCT",
        "One World Trade Center" => "Tishman Construction",
        "Taipei 101" => "KTRT Joint Venture",
        "Petronas Twin Tower" => "KLCC Holdings",
        "KL Tower" => "Kumpulan Senireka",
        "KL Sentral" => "MRCB",
        "KLIA" => "Khazanah Nasional",
        "Lottee World Tower" => "Lotte Engineering & Construction",
        "Nu Sentral" => "Pelaburan Hartanah Berhad (PHB)",
        "Guangzhou International Finance Center" => "Guangzhou Municipal Construction Group",
        "Resort Hotel" => "Genting Group",
        "First World Hotel" => "Genting Group",
        "Pertamina Energy Tower" => "SOM",
        "Jin Mao Tower" => "China Jin Mao Group",
        "Sungai Wang" => "Sungei Wang Plaza",
        "Plaza Low Yat" => "Low Yat Group",
        "Time Square" => "Berjaya Group",
        "Bank of China Tower" => "Sherman Kung & Associates Architects",
        "Al Hamra Tower" => "SOM",
        "Yokohama Landmark Tower" => "Shimizu Corporation",
        "Ryugyong Tower" => "Baikdoosan Architects & Engineers",
        "Sydney Opera House" => "Ove Arup & Partners",
        "Empire State Building" => "Shreve, Lamb & Harmon",
        "Lotus Temple" => "Flint & Neill",
        "Maxim Hotel" => "Genting Group",
        "Jewel Changi Airport" => "RSP Architects Planners & Engineers",
        "Fahrenheit 88 Shopping Mall" => "Makna Mujur Sdn Bhd",
        "Tropicana Avenue" => "Tropicana Corporation",
        "IOI city mall" => "IOI Group",
        "Theme Park Hotel" => "Genting Group",
        "Crockfords" => "Genting Group",
        "PNB 118" => "Leslie E. Robertson Associates",
        "The Exchange 106" => "China State Construction Engineering Corporation",
        "World Capital Centre" => "Al-Aman Group",
        "Grand Hyatt Colombo" => "MAGA Engineering",
        "Ostankino Tower" => "PTPC",
        "Macau Tower" => "Craig Craig Moller",
        "Tokyo Skytree" => "Obayashi Corporation",
        "Canton Tower" => "Arup",
        "Beijing National Stadium" => "ArupSport",
        "Burj Khalifa" => "SOM",
        "Willis Tower" => "SOM",
        "Tokyo Disneyland" => "The oriental Land Company",
        "Disneyland Park" => "The Walt Disney Company",
        "Ginza Six" => "Ginza Six Retail Management Co. Ltd.",
        "Velocity Mall" => "Sunway Group",
        "Sunway Pyramid" => "Sunway Group",
        "Sunway Lagoon" => "Sunway Group",
        "Sunway Resort Hotel & Spa" => "Sunway Group",
        "Sky Avenue" => "Genting Group",
        "3 Damansara Shopping Mall" => "CapitaLand",
        "Fuji Television Network, Inc." => "Fuji Media Holdings, Inc.",
        "Hong Kong Stadium" => "Leisure and Cultural Services Department",
        "Beijing National Stadium" => "China Architectural Design & Research Group",
        "National Museum of China" => "Ministry of Culture of the People's Republic of China",
        "Mitsukoshi, Ltd." => "Mitsui Takatoshi",
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // update invoices
        $sql = "UPDATE `invoices` SET `work_done_certified` = `work_done_certified` * 20, `vo` = `vo` * 20, `retention_released` = `retention_released` * 20, `retention` = `retention` * 20, `prev_certified` = `prev_certified` * 20, `nett_certified` = `nett_certified` * 20, `gst` = `gst` * 20, `sst` = `sst` * 20, `debit_note` = `debit_note` * 20, `payment_amt` = `payment_amt` * 20, `balance` = `balance` * 20";
        \DB::statement($sql);

        // update expenses
        $sql = "UPDATE `expenses` SET `invoice_amt` = `invoice_amt` * 20, `gst` = `gst` * 20, `sst` = `sst` * 20, `dc_note` = `dc_note` * 20, `payment_amt` = `payment_amt` * 20, `balance` = `balance` * 20";
        \DB::statement($sql);

        // update payments
        $sql = "UPDATE `payments` SET `payment_amt` = `payment_amt` * 20";
        \DB::statement($sql);

        // update purchase order
        $sql = "UPDATE `purchase_orders` SET `total_value` = `total_value` * 20, `gst` = `gst` * 20, `sst` = `sst` * 20, `dc_note` = `dc_note` * 20, `budget` = `budget` * 20";
        \DB::statement($sql);

        // update variation order
        $sql = "UPDATE `variation_orders` SET `total_value` = `total_value` * 20, `gst` = `gst` * 20, `sst` = `sst` * 20, `dc_note` = `dc_note` * 20";
        \DB::statement($sql);

        // update project
        $sql = "UPDATE `projects` SET `contract_sum` = `contract_sum` * 20, `budget` = `budget` * 20, `retention` = `retention` * 20, `variation_order` = `variation_order` * 20, `simulated_cashflow` = `simulated_cashflow` * 20, `omission_value` = `omission_value` * 20, `contingency_amt` = `contingency_amt` * 20";
        \DB::statement($sql);

        $projectNameList = array_keys($this->projects_name_clients);
        shuffle($projectNameList);
        $projects = Project::all();
        foreach($projects as $index => $project) {
            $project->name = $projectNameList[$index];
            $project->client = $this->projects_name_clients[$projectNameList[$index]];
            $client = Client::select("uuid")->where("name", "=", $project->client)->first();
            if(!$client){
                $client = Client::findByUuid($project->client_id);
                $client->name = $project->client;
                $client->save();
            }
            $project->client_id = $client->uuid;
            $project->save();
        }
    }
}
