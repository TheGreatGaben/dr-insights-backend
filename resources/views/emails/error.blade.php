@extends('emails.layout')

@section('content')
Hi,<br><br>
This is just a testing email for error handling. Error details as below:<br><br>

<div>
Status Code : {!! e($error['status_code']) !!}<br>
Error Message : {!! e($error['message']) !!}<br>
Data : {!! json_encode($error['data']) !!}<br>
Trace : <br>
{!! e($trace) !!}
</pre>
</div>
@endsection
