@extends('emails.layout')

@section('content')
Hi,<br><br>
You are receiving this email because we received a password reset request for your account.<br><br>

<a href="{!! e($url) !!}">{!! e($url) !!}</a><br><br>

If you did not request a password reset, no further action is required.
@endsection
