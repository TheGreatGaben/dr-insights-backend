@extends('emails.layout')

@section('content')
Hi,<br><br>
We found duplicate data for model({!! e(get_class($model)) !!}), Please Check<br><br>
<div>
    <span><strong>Model ID</strong>: {!! e($model->id) !!}</span><br>
@foreach ($duplicateData as $field)
    <span><strong>{!! e($field) !!}</strong>: {!! e($model->$field) !!}</span><br>
@endforeach
</div>
@endsection
