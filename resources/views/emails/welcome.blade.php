@extends('emails.layout')

@section('content')
Hi,<br><br>
Welcome to join our system. You account details as below :<br><br>

Email: {!! e($user_email) !!}<br>
Password: {!! e($user_password) !!}<br>
@endsection
