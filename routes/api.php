<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'v2', 'middleware' => 'auth:api', 'prefix' => 'v2'], function () {
    Route::get('dashboard', 'DashboardController@getOverallSummary');
    Route::get('dashboard/overdue', 'DashboardController@getOverdueSummary');
    Route::get('dashboard/projects', 'DashboardController@getProjectSummary');
    Route::get('', 'ProjectController@getSummary');

    Route::resource('organisations','OrganisationController');
    Route::group(['prefix' => 'projects'], function () {
        Route::get('/permissions', 'PermissionController@getAllProjectPermissions');
        Route::resource('', 'ProjectController')->parameters(["" => "project"]);

        Route::get('/{project_id}/invoices/prev_certified', 'InvoiceController@getPrevCertified');
        Route::get('/{project}/invoices', 'InvoiceController@index');
        Route::get('/{project}/invoices/myob', 'InvoiceController@exportMYOB');
        Route::get('/{project}/expenses/myob', 'ExpenseController@exportMYOB');
        Route::get('/{project}/expenses', 'ExpenseController@index');
        Route::get('/{project}/contractors', 'ContractorController@index');
        Route::get('/{project}/cashflow-projection', 'ProjectController@getProjectionCashflow');
    });

    Route::resource('invoices','InvoiceController');
    Route::put('write-off/invoices/{invoice}', 'InvoiceController@writeOff');
    Route::delete('write-off/invoices/{invoice}', 'InvoiceController@undoWriteOff');
    Route::resource('expenses','ExpenseController');
    Route::put('write-off/expenses/{expense}', 'ExpenseController@writeOff');
    Route::delete('write-off/expenses/{expense}', 'ExpenseController@undoWriteOff');

    Route::resource("purchaseOrders", 'PurchaseOrderController');
    Route::resource("variationOrders", 'VariationOrderController');
    Route::resource("payments", 'PaymentController');
    Route::get('clients/myob', 'ClientController@exportMYOB');
    Route::get('clients/invoices/{client?}', 'ClientController@clientsInvoice');
    Route::get('clients/{client}/report', 'ClientController@report');
    Route::resource('clients', 'ClientController');
    Route::get('contractors/myob', 'ContractorController@exportMYOB');
    Route::get('contractors/expenses/{contractor?}', 'ContractorController@contractorsExpenses');
    Route::get('contractors/{contractor}/report', 'ContractorController@report');
    Route::resource('contractors', 'ContractorController');
    Route::resource('payment_records', 'PaymentRecordsController');
    Route::get("finance_account_codes/get_child_by_code", "FinanceAccountCodeController@getChildByCode");
    Route::resource('finance_account_codes','FinanceAccountCodeController');

    Route::get('project-managers/project-summary', 'UserController@getProjectManagersSummary');
    Route::get('end_customers', 'ProjectController@getEndCustomers');
    Route::get('bulk-payment-projects', 'ProjectController@getProjectsForBulkPayment');

    Route::get('user/details', 'UserController@getUserDetails');
    Route::get('users/role/{role}', 'UserController@getUserByRoleId')->where(['role' => '[0-9]+']);
    Route::get('users/role/{role}', 'UserController@getUserByRoleName');
    Route::resource('users','UserController');
    Route::resource('roles','RoleController');
    Route::resource('permissions','PermissionController');
    Route::put('batch-update/users/roles', 'UserController@updateUserRoles');
    Route::get('impersonate-user/take', 'UserController@impersonateUser');
    Route::delete('impersonate-user/leave', 'UserController@leaveImpersonatingUser');

    Route::resource("cache", "CacheController")->only(['index', 'store']);
    Route::put("cache", "CacheController@update");
    Route::delete("cache", "CacheController@delete");
});

// Password change needs user authentication
Route::group(['namespace' => 'Auth', 'prefix' => 'password'], function (){
    // request token to reset password
    Route::post('forgot', 'PasswordResetController@create');
    // verify token and show reset password form
    Route::get('reset/{token}', 'PasswordResetController@find');
    // post to reset password
    Route::post('change', 'PasswordChangeController@change')->middleware('auth:api');
    // for admin to help to reset password
    Route::post('reset', 'PasswordResetController@reset');
});

Route::get('/sandbox/{file}', "v2\AppController@runScript");
Route::get('/test', function(Request $request){
    return (new \App\Queries\v2\ProjectQuery($request))->try();
});
