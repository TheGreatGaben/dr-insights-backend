#!/usr/bin/env bash

#===================================================================================
#
# FILE: env.sh
#
# USAGE: env.sh
#
# DESCRIPTION: add utilities to get env variables
#              <Caveat> have to run command from project root
#
#===================================================================================

function get_env() {
    var_name="$1"
    line=$(grep ${var_name} <.env)

    # remove var name and `=`
    echo ${line#${var_name}=}
}
