#!/usr/bin/env bash

#===================================================================================
#
# FILE: mysql_replace.sh
#
# USAGE: mysql_replace.sh
#
# DESCRIPTION: replace the whole database with production sql
#
#===================================================================================
source env.sh

app_debug=$(get_env "APP_DEBUG")
prod_path=$(get_env "PROD_PATH") # to get db from production

db_name=$(get_env "DB_DATABASE")
db_name=$(get_env "DB_DATABASE")
db_user=$(get_env "DB_USERNAME")
db_password=$(get_env "DB_PASSWORD")

echo ${db_user}

#MYSQL_PWD=${db_password} mysql -u "$db_user" "$db_name" < file.sql
