#!/usr/bin/env bash

#===================================================================================
#
# FILE: mysqldump_db.sh
#
# USAGE: mysqldump_db.sh [$append_filename (optional)]
#
# DESCRIPTION: dump db data into a sql file using `mysqldump`
#              `$append_filename` can be define as the first arg, if not current datetime will be utilise
#
#===================================================================================

function read_env() {
    var_name="$1"
    line=$(grep ${var_name} <.env)

    # remove var name and `=`
    echo ${line#${var_name}=}
}

append_filename=$1
db_name=$(read_env "DB_DATABASE")
db_user=$(read_env "DB_USERNAME")
db_password=$(read_env "DB_PASSWORD")
current_time=${append_filename:=$(date +%Y%m%d_%H%M%S)}
filename="backup_$current_time.sql"

MYSQL_PWD=${db_password} mysqldump -u "$db_user" "$db_name" > ${filename}

echo ${filename}
