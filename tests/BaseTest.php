<?php

namespace Tests;

use App\Models\User;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BaseTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    public function setUp(): void
    {
        parent::setUp();
        $this->artisan('db:seed');
    }

    protected function getFakeUser()
    {
        $user = factory(User::class)->create();
        $user->assignRole('KAMI');
        return $user;
    }
}
