<?php

namespace Tests\Unit;

use App\Models\Client;

use Tests\BaseTest;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClientTest extends BaseTest
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateApi()
    {
        $client = factory(Client::class)->make();
        $payload = $client->attributesToArray();

        $user = $this->getFakeUser();

        $this->actingAs($user, 'api')
             ->json('POST', '/api/v2/clients/', $payload)
             ->assertStatus(201)
        ;
    }

    public function testReadApi()
    {
        $client = factory(Client::class)->create();

        $user = $this->getFakeUser();

        $this->actingAs($user, 'api')
             ->json('GET', '/api/v2/clients/' . $client->uuid)
             ->assertStatus(200)
        ;
    }

    public function testUpdateApi()
    {
        $client = factory(Client::class)->create();
        $payload = $client->attributesToArray();
        $payload['address_1'] = $this->faker->address;
        $payload['postcode'] = substr($this->faker->postcode, 0, 5);

        $user = $this->getFakeUser();

        $this->actingAs($user, 'api')
             ->json('PUT', '/api/v2/clients/' . $client->uuid, $payload)
             ->assertStatus(200)
        ;
    }

    public function testDeleteApi()
    {
        $client = factory(Client::class)->create();

        $user = $this->getFakeUser();

        $this->actingAs($user, 'api')
             ->json('DELETE', '/api/v2/clients/' . $client->uuid)
             ->assertStatus(200)
        ;
    }
}
