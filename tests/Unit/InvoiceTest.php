<?php

namespace Tests\Unit;

use App\Models\Client;
use App\Models\Project;
use App\Models\Invoice;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Tests\BaseTest;
use Illuminate\Foundation\Testing\RefreshDatabase;

class InvoiceTest extends BaseTest
{
    /**
     * @test
     */
    public function invoicePayload()
    {
        $randomAmount = $this->faker->randomFloat(2, 100000, 1000000);
        $this->assertTrue(true);
        return [
            'invoice_no' => Str::random(10),
            'invoice_date' => Carbon::now()->toDateString(),
            'cert_no' => 'PC#1',
            'nett_certified' => $randomAmount,
            'term' => 30,
            'retention' => 0,
            'retention_released' => 0,
            'work_done_certified' => $randomAmount,
            'prev_certified' => 0,
            'debit_note' => 0,
            'exclude_tax' => 0,
            'tax_amt' => 0,
            'tax_type' => 'sst',
            'vo' => 0,
        ];
    }

    /**
     * @depends invoicePayload
     */
    public function testCreateApi($invoice)
    {
        $client = factory(Client::class)->create();
        $project = factory(Project::class)->make();
        $client->projects()->save($project);

        $invoice['project_id'] = $project->uuid;
        $payload = $invoice;

        $user = $this->getFakeUser();

        $this->actingAs($user, 'api')
             ->json('POST', '/api/v2/invoices/', $payload)
             ->assertStatus(201)
        ;
    }

    public function testReadApi()
    {
        $client = factory(Client::class)->create();
        $project = factory(Project::class)->make();
        $client->projects()->save($project);

        $invoice = factory(Invoice::class)->make();
        $project->invoices()->create($invoice->attributesToArray());

        $user = $this->getFakeUser();

        $this->actingAs($user, 'api')
             ->json('GET', '/api/v2/invoices/' . $invoice->uuid)
             ->assertStatus(200)
        ;
    }

    /**
     * @depends invoicePayload
     */
    public function testUpdateApi($invoice)
    {
        $client = factory(Client::class)->create();
        $project = factory(Project::class)->make();
        $client->projects()->save($project);

        $newInvoice = factory(Invoice::class)->make();
        $newInvoice = $project->invoices()->create($newInvoice->attributesToArray());

        $user = $this->getFakeUser();

        $payload = $invoice;
        $payload['project_id'] = $newInvoice->project_id;

        $url = '/api/v2/invoices/' . $newInvoice->uuid;
        $this->actingAs($user, 'api')
             ->json('PUT', $url, $payload)
             ->assertStatus(200)
        ;
    }

    public function testDeleteApi()
    {
        $client = factory(Client::class)->create();
        $project = factory(Project::class)->make();
        $client->projects()->save($project);

        $invoice = factory(Invoice::class)->make();
        $invoice = $project->invoices()->create($invoice->attributesToArray());

        $user = $this->getFakeUser();

        $this->actingAs($user, 'api')
             ->json('DELETE', '/api/v2/invoices/' . $invoice->uuid)
             ->assertStatus(200)
        ;
    }
}
