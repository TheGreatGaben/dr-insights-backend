<?php

namespace Tests\Unit;

use App\Models\User;
use App\Models\Client;
use App\Models\Project;
use App\Models\Invoice;
use App\Models\Payment;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PaymentTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     */
    public function createInvoice() {
        $client = factory(Client::class)->create();
        $project = factory(Project::class)->make();
        $client->projects()->save($project);
        $invoice = factory(Invoice::class)->make();
        $project->invoices()->save($invoice);
        $this->assertTrue(true);
        return $invoice;
    }

    /**
     * @depends createInvoice
     */
    public function testCreateInvoicePayments($invoice) {
        $payments = factory(Payment::class, 5)->make([
            'project_id' => $invoice->project_id,
            'cash_flow' => 'IN',
        ]);
        $invoice->payments()->saveMany($payments);
        $totalPayment = 0;
        foreach ($payments as $payment) {
            $totalPayment += $payment->payment_amt;
        }
        $this->assertEquals($invoice->total_payment, $totalPayment);
        $expectedBalance = round($invoice->total_amount - $totalPayment, 2);
        $this->assertEquals($expectedBalance, $invoice->accurate_balance);
    }

    public function testWriteOffInvoice() {
        $client = factory(Client::class)->create();
        $project = factory(Project::class)->make();
        $client->projects()->save($project);
        $invoice = factory(Invoice::class)->make();
        $project->invoices()->save($invoice);

        $user = factory(User::class)->create();
        $payload = [
            'write_off_reason' => 'Testing invoice write off',
        ];
        $this->actingAs($user, 'api')
             ->json('PUT', '/api/v2/write-off/invoices/' . $invoice->uuid, $payload)
             ->assertStatus(200)
         ;
    }
}
