<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Support\Str;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    protected $user;

    /**
     * @test
     */
    public function testAddUser(){
        $user = User::find(1);
        $payload = [
            'name' => ($name = Str::random(10)),
            'email' => $name . '@nixel.tech',
            'password' => 'test123',
            'password_confirmation' => 'test123',
        ];
        $this->actingAs($user, 'api')
            ->json('POST', route('users.store'), $payload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'email',
                    'all_permissions',
                    'roles',
                ]
            ])
        ;
    }

    /**
     * @test
     */
    public function testEditUser(){
        $user = factory(User::class)->create();
        $payload = [
            'name' => Str::random(10),
            'password' => 'qwe123',
            'password_confirmation' => 'qwe123',
        ];
        $this->actingAs($user, 'api')
            ->json('PUT', route('users.update', $user->id), $payload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'email',
                    'all_permissions',
                    'roles',
                ]
            ])
        ;
    }

    /**
     * @test
     */
    public function testAddUserError(){
        $this->assertFalse(false);
    }
}
